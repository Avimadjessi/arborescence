<?php
if(file_exists('vendor/autoload.php'))
{
	require 'vendor/autoload.php';
} else 
{
	echo "<h1>Please install via composer.json</h1>";
	echo "<p>Install Composer instructions: <a href='https://getcomposer.org/doc/00-intro.md#globally'>https://getcomposer.org/doc/00-intro.md#globally</a></p>";
	echo "<p>Once composer is installed navigate to the working directory in your terminal/command promt and enter 'composer install'</p>";
	exit;
}

if (!is_readable('app/core/config.php')) 
{
	die('No config.php found, configure and rename config.example.php to config.php in app/core.');
}

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
	define('ENVIRONMENT', 'development');
/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but production will hide them.
 */

if (defined('ENVIRONMENT'))
{
	switch (ENVIRONMENT)
	{
		case 'development':
			error_reporting(E_ALL);
			break;

		case 'production':
			error_reporting(0);
			break;

		default:
			exit('The application environment is not set correctly.');
	}
}

//initiate config
new \core\config();

//create alias for Router
use \core\router,
    \helpers\url;

//define routes
Router::any(''								, '\controllers\welcome@index');
Router::any('about'							, '\controllers\welcome@about');
Router::any('faq'							, '\controllers\welcome@faq');
Router::any('login'							, '\controllers\auth@login');
Router::any('logout'						, '\controllers\auth@logout');
Router::any('register'						, '\controllers\auth@register');
Router::any('recover'						, '\controllers\auth@recover');

Router::any('profile'						, '\controllers\account@profile');
Router::any('profile/edit'					, '\controllers\account@edit');
Router::any('profile/edit-image'			, '\controllers\account@editImage');
Router::any('profile/delete'				, '\controllers\account@delete');

Router::any('trees/canvas'					, '\controllers\draw@canvas');
Router::any('trees/canvas/help'				, '\controllers\draw@help');
Router::any('trees/tag/(:any)'				, '\controllers\draw@searchUserTag');
Router::any('trees'							, '\controllers\draw@trees');
Router::any('trees/trees'					, '\controllers\draw@trees');
Router::any('trees/training'				, '\controllers\draw@training');
Router::any('trees/(:any)/delete'			, '\controllers\draw@delete');
Router::any('trees/(:any)/share'			, '\controllers\draw@share');
Router::any('trees/exercise/start'			, '\controllers\draw@startExercise');
Router::any('trees/exercise/verify'			, '\controllers\draw@verifyExercise');
Router::any('trees/exercise/(:any)/canvas'	, '\controllers\draw@canvasExercise');
Router::any('trees/solution/(:any)/canvas'	, '\controllers\draw@loadExercise');
Router::any('trees/(:any)/canvas'			, '\controllers\draw@load');
Router::any('trees/(:any)/(:any)/canvas'	, '\controllers\draw@load');
Router::any('trees/(:any)/tag'				, '\controllers\draw@tag');

Router::any('admin'							, '\controllers\admin@informations');
Router::any('admin/about'					, '\controllers\admin@informations');
Router::any('admin/notice'					, '\controllers\admin@notice');
Router::any('admin/search'					, '\controllers\admin@search');
Router::any('admin/search/result'			, '\controllers\admin@searchResult');
Router::any('admin/exercise/canvas'			, '\controllers\admin@canvas');
Router::any('admin/exercise/(:any)/canvas'	, '\controllers\admin@load');
Router::any('admin/exercise/share'			, '\controllers\admin@share');
Router::any('admin/exercise/(:any)/delete'	, '\controllers\admin@delete');

Router::any('search/notice/all'				, '\controllers\search@noticeGetAll');


//if no route found
Router::error('\core\error@index');

//turn on old style routing
Router::$fallback = false;

//execute matched routes
Router::dispatch();
