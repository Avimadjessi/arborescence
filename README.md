A R B O R E S C E N C E
=========================
# Introduction

**Arborescence** est un outil en ligne qui permet de dessiner des schémas de phrases en quelques clicks. Il accompagne l'utilisateur en anticipant un maximum d'erreurs et fournissant des rétroactions automatiques ciblées. Plusieurs phrases y sont préencodées pour que chacun puisse s'entrainer. 
Enfin tout utilisateur pourra sauvegarder les arbres qu’il a produits pour ensuite y revenir, les partager ou poser des questions théoriques à l'équipe de recherche. 


# SMVC Framework
>**Simple MVC Framework** is a PHP 5.5+ MVC Framework. It's designed to be lightweight and modular, allowing developers to build better and easy to maintain code with PHP. 
[Simple MVC Framework](http://simplemvcframework.com/php-framework)


# Librairies et outils

 - CreateJS (easelJs)
 - JQuery
	 - lettering 
	 - textillate
	 - dataTables
 - Bootstrap
