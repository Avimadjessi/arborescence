<?php
namespace helpers;

/**
* Helper to manage json parse
*
* @todo NOT USED - CHECK AND DELETE
*/
class ArborescenceJson
{
	private $_json;
	private $_tree;
	private $_nodes;
	private $_relations;
	private $_notes;
	private $_drawnObjects;

	/**
	* Constructor
	* @param String $arborescenceJson
	*/
	public function __construct ($arborescenceJson)
	{
		$this->_json 	= $arborescenceJson;
	}

	/**
	* Used to parse the json string
	*/
	public function parse ($json, $structure)
	{
		switch ($structure) {
			case 'node':
				$this->_tree 	= json_decode($json, true);
				$this->_nodes 	= $this->_tree->subTreees;
				break;
			case 'relation':
				# code...
				break;
			case 'drawnObject':
				# code...
				break;
			default:
				# code...
				break;
		}

		var_dump($this->_tree);
		die;
	}


	/*
	---------- Getters & Setters -----------------------------------------------
	*/
	public function getNodes ()
	{
		return $_nodes;
	}
	public function getRelations ()
	{
		return $_relations;
	}
	public function getDrawnObjects ()
	{
		return $_drawnObjects;
	}
	public function getNotes ()
	{
		return $_notes;
	}
}