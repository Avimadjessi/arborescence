<?php
namespace helpers;

/**
* Helper to manage arrays/json parse
*
* @package arborescence.app.helpers
*/
class ArborescenceParser
{
	private $_tree;
	private $_nodes;
	private $_relations;
	private $_notes;
	private $_drawnObjects;

	public function __construct ()
	{
		$this->_tree 			= array();
		$this->_nodes 			= array();
		$this->_relations 		= array();
		$this->_drawnObjects 	= array();
		$this->_notes 			= array();
	}

	/**
	* Used to parse the array and json string
	*
	* Transform a multi-dim array or json string into a simple dimeension array 
	* @param array $array the array to parse or json string
	* @param string $structure the array/json structure to recreate
	* @param boolean $decode true if $array param is a json string
	*/
	public function parse ($array, $structure, $decode=false)
	{
		if ($decode)
		{
			$array = json_decode($array, true)['tree']['subtrees'];
		}

		switch ($structure) 
		{
			case 'tree':
				break;
			case 'node':
				foreach ($array as $node) 
				{
					$this->recursiveParseNode ($node);
				}
				break;
			case 'relation':
				foreach ($array as $relation) 
				{
					if (isset($relation))
					{
						$this->_relations[] = $relation;
					}
				}
				break;
			case 'drawnObject':
				foreach ($array as $drawnObject) 
				{
					if (isset($drawnObject))
					{
						$this->_drawnObjects[] = $drawnObject;
					}
				}
				break;
			default:
				# code...
				break;
		}
	}

	public function nodeEquals ($firstNode, $secondNode)
	{
		return ($firstNode['value']['fonction'] == $secondNode['value']['fonction'])
				&& ($firstNode['value']['structureIntegrative'] == $secondNode['value']['structureIntegrative']);
	}

	/*
	---------- Private Meth ----------------------------------------------------
	*/
	private function recursiveParseNode ($node)
	{
		if (isset($node) && !empty($node))
		{
			$this->_nodes[] 	= $node;

			$this->recursiveParseNode ($node['left']);
			$this->recursiveParseNode ($node['right']);
		}
	}


	/*
	---------- Getters & Setters -----------------------------------------------
	*/
	public function getNodes ()
	{
		return $this->_nodes;
	}
	public function getRelations ()
	{
		return $this->_relations;
	}
	public function getDrawnObjects ()
	{
		return $this->_drawnObjects;
	}
	public function getNotes ()
	{
		return $this->_notes;
	}
}