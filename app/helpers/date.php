<?php 
namespace helpers;

class Date 
{
	public static function verbose ($dateTime, $separator='|', $lang='BE')
	{
		switch ($lang)
		{
			case 'BE':
				return Date::verboseFR($dateTime, $separator);
				break;

			case 'FR':
				return Date::verboseFR($dateTime, $separator);
				break;
			
			default:
				return '/';
		}
	}

	public static function display ($dateTime, $separator='|', $lang='FR')
	{
		switch ($lang)
		{
			case 'BE':
				return Date::displayBE($dateTime, $separator);
				break;

			case 'FR':
				return Date::displayBE($dateTime, $separator);
				break;
			
			default:
				return '/';
		}
	}

	// Display correctely a DATETIME in FRENCH (FR and FR-BE)
	private function displayBE($datetime, $separator)
	{
		$year 		= substr($datetime, 0, 4);
		$month 		= substr($datetime, 5, 2); 
		$day 		= substr($datetime, 8, 2);

		$time 		= substr($datetime, -8, 8);
		return $day."/".$month."/".$year. " " . $separator . " " .$time;
	}

	// Display correctely a DATETIME in FRENCH (FR and FR-BE)
	private function verboseFR($datetime, $separator)
	{
		// $monthList = [
		// 	'01'	=> 'Janvier',
		// 	'02'	=> 'Février',
		// 	'03'	=> 'Mars',
		// 	'04'	=> 'Avril',
		// 	'05'	=> 'Mai',
		// 	'06'	=> 'Juin',
		// 	'07'	=> 'Juillet',
		// 	'08'	=> 'Août',
		// 	'09'	=> 'Septembre',
		// 	'10'	=> 'Octobre',
		// 	'11'	=> 'Novembre',
		// 	'12'	=> 'Décembre',
		// ];
		$monthList = [
			'01'	=> 'Jan',
			'02'	=> 'Fév',
			'03'	=> 'Mar',
			'04'	=> 'Avr',
			'05'	=> 'Mai',
			'06'	=> 'Juin',
			'07'	=> 'Juil',
			'08'	=> 'Août',
			'09'	=> 'Sep',
			'10'	=> 'Oct',
			'11'	=> 'Nov',
			'12'	=> 'Déc',
		];
		$year 		= substr($datetime, 0, 4);
		$month 		= $monthList[substr($datetime, 5, 2)]; 
		$day 		= substr($datetime, 8, 2);

		$time 		= substr($datetime, -8, 8);
		return $day." ".$month." ".$year. " " . $separator . " " .$time;
	}
}