<?php
use \helpers\session,
    \helpers\date;
?>
<header>
	<a href="<?php echo DIR?>">
		<div class="header-logo"></div>
	</a>
</header>
<!-- G L O B A L . M O D A L S -->

<!-- ========== M O D A L // N O T I C E =================================== -->
<div class="modal fade" id="noticeModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title  color-default center">
                    L I S T E . D E S . A N N O N C E S 
                </h4>
            </div>

            <div class="modal-body" id="noticeModalBody">
                <?php 
                if(Session::get('loggedin'))
                {
                    $userNotices    = Session::get('allNotices');
                    if ($userNotices)
                    {
                ?>
                        <div id="user-notice-list">
                            <?php
                            foreach ($userNotices as $key => $notice) 
                            {
                                if ($notice->private == true)
                                {
                                    switch ($notice->type) 
                                    {
                                        case 'TREE_CREATED':
                                            $color = 'success';
                                            break;
                                        case 'TREE_DELETED':
                                            $color = 'success';
                                            break;
                                        case 'TREE_SENT':
                                            $color = 'info';
                                            break;
                                        case 'TREE_RECEIVED':
                                            $color = 'info';
                                            break;
                                        default:
                                            $color = 'success';
                                            break;
                                    }
                                } else
                                    $color = 'warning';
                            ?>
                                <label class="label label-<?php echo $color; ?> notice-header">
                                    <?php echo $notice->title; ?>
                                </label>                                 
                                <b class="notice-title">
                                    <?php echo Date::verbose($notice->created_at) ?></b> <br/> 
                                <span class="notice-message"><?php echo $notice->body; ?></span>
                                <br/><br/>
                            <?php
                            } 
                            ?>
                        </div>
                <?php
                    }
                }
                ?>
            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<!-- ========== M O D A L // T R E E S ===================================== -->
<div class="modal fade" id="treesByTag" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title  color-default center">
                    R E S U L T A T . R E C H E R C H E . A R B R E 
                </h4>
            </div>

            <div class="modal-body" id="treesByTagBody">        
                
            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<!-- ========== M O D A L // U S E R S ===================================== -->
<div class="modal fade" id="usersFromSearch" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title  color-default center">
                    R E S U L T A T . R E C H E R C H E . U T I L I S A T E U R 
                </h4>
            </div>

            <div class="modal-body" id="usersFromSearchBody">
                
            </div>

            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>