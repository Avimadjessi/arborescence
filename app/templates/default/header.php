<!DOCTYPE html>
<html lang="<?php echo LANGUAGE_CODE; ?>">
	<head>

		<!-- Site meta -->
		<meta charset="utf-8">
		<title><?php echo $data['title'].' - '.SITETITLE; ?></title>

		<!-- ---------- C . S . S ------------------------------------------ -->
		<?php
			helpers\assets::css(array(
				helpers\url::template_path() . 'css/bootstrap.css',
				helpers\url::template_path() . 'css/dataTables.bootstrap.css',
				helpers\url::template_path() . 'css/fileinput.css',
				helpers\url::template_path() . 'css/font-awesome.min.css',
				helpers\url::template_path() . 'css/style.css',
				helpers\url::template_path() . 'css/animations.css',
				helpers\url::template_path() . 'css/animate.css',
				helpers\url::template_path() . 'css/color.css'
			));
		?>
		<!-- ---------- F A V I C O N -------------------------------------- -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo helpers\url::template_path()?>icon/arborescence/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo helpers\url::template_path()?>icon/arborescence/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo helpers\url::template_path()?>icon/arborescence/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo helpers\url::template_path()?>icon/arborescence/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo helpers\url::template_path()?>icon/arborescence/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo helpers\url::template_path()?>icon/arborescence/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo helpers\url::template_path()?>icon/arborescence/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo helpers\url::template_path()?>icon/arborescence/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo helpers\url::template_path()?>icon/arborescence/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo helpers\url::template_path()?>icon/arborescence/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo helpers\url::template_path()?>icon/arborescence/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo helpers\url::template_path()?>icon/arborescence/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo helpers\url::template_path()?>icon/arborescence/favicon-16x16.png">
		<link rel="manifest" href="<?php echo helpers\url::template_path()?>icon/arborescence/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo helpers\url::template_path()?>icon/arborescence/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		
		<!-- ---------- J . S ---------------------------------------------- -->
		<?php 
			helpers\assets::js(array(
				helpers\url::template_path() . 'js/vendor/jquery-2.1.1.min.js',
				helpers\url::template_path() . 'js/vendor/jquery-ui.js',
				helpers\url::template_path() . 'js/vendor/jquery.lettering.js',
				helpers\url::template_path() . 'js/vendor/jquery.textillate.js',
				helpers\url::template_path() . 'js/vendor/jquery.dataTables.js',
				helpers\url::template_path() . 'js/vendor/easeljs-0.8.0.min.js',
				helpers\url::template_path() . 'js/vendor/bootstrap.js',
				helpers\url::template_path() . 'js/vendor/file/fileinput.min.js',
				helpers\url::template_path() . 'js/vendor/file/fileinput_locale_fr.js',

				helpers\url::template_path() . 'js/script.js',

				helpers\url::template_path() . 'js/arborescence.js',
				helpers\url::template_path() . 'js/arborescence-tree.js',
				helpers\url::template_path() . 'js/arborescence-shapes.js',
				helpers\url::template_path() . 'js/arborescence-drawing.js',
				helpers\url::template_path() . 'js/arborescence-node.js',
				helpers\url::template_path() . 'js/arborescence-relation.js',
				helpers\url::template_path() . 'js/arborescence-drawnObject.js',
				helpers\url::template_path() . 'js/arborescence-note.js'
			));
		?>
		
	</head>
<body>
	<div id="body">
