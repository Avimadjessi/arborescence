DROP TABLE IF EXISTS HVE_NOTICE;
DROP TABLE IF EXISTS HVE_TREEHASTAG;
DROP TABLE IF EXISTS HVE_USERHASTAG;
DROP TABLE IF EXISTS HVE_USERHASEXERCISE;
DROP TABLE IF EXISTS HVE_EXERCISESOLUTION;
DROP TABLE IF EXISTS HVE_EXERCISE;
DROP TABLE IF EXISTS HVE_TAG;
DROP TABLE IF EXISTS HVE_TREE;
DROP TABLE IF EXISTS HVE_USER;

DROP TABLE IF EXISTS HVE_USER_ARCHIVE;
DROP TABLE IF EXISTS HVE_TREE_ARCHIVE;

-- ----- User ------------------------------------------------------------------
CREATE TABLE HVE_USER (
    id                  INT(20)         AUTO_INCREMENT,
    email               VARCHAR(50)     NOT NULL,
    fullname            VARCHAR(50)     NOT NULL,
    password            VARCHAR(60)     NOT NULL,
    occupation          VARCHAR(60)     ,
    level               TINYINT(4)      DEFAULT 3,
    active              BOOLEAN         DEFAULT 1,
    lastLoggedin        TIMESTAMP       NOT NULL DEFAULT 0,
    created_at          TIMESTAMP       NOT NULL DEFAULT 0,
    updated_at          TIMESTAMP       NOT NULL DEFAULT NOW() ON UPDATE NOW(),

    PRIMARY KEY (id),
    UNIQUE (email)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE HVE_USER_ARCHIVE (
    id                  INT(20)         AUTO_INCREMENT,
    oldId               INT(20)         NOT NULL,
    email               VARCHAR(50)     NOT NULL,
    fullname            VARCHAR(50)     NOT NULL,
    password            VARCHAR(60)     NOT NULL,
    occupation          VARCHAR(60)     ,
    level               TINYINT(4)      DEFAULT 3,
    active              BOOLEAN         DEFAULT 1,
    lastLoggedin        TIMESTAMP       NOT NULL DEFAULT 0,
    created_at          TIMESTAMP       NOT NULL DEFAULT 0,
    updated_at          TIMESTAMP       NOT NULL DEFAULT NOW() ON UPDATE NOW(),

    PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----- Tree ------------------------------------------------------------------
CREATE TABLE HVE_TREE (
    id                  INT(20)         AUTO_INCREMENT,
    jsonNodes           TEXT            NOT NULL,
    jsonRelations       TEXT            NOT NULL,
    jsonDrawnObjects    TEXT            NOT NULL,
    jsonNotes           TEXT            NOT NULL,
    user                INT(20)         NOT NULL,
    lastUser            INT(20)         NOT NULL,
    name                VARCHAR(100)    NOT NULL,
    message             VARCHAR(256)    ,
    exercise            INT(20)         ,
    slug                VARCHAR(200)    NOT NULL,
    version             DECIMAL(4,2)    NOT NULL,
    
    created_at          TIMESTAMP       NOT NULL DEFAULT 0,
    updated_at          TIMESTAMP       NOT NULL DEFAULT NOW() ON UPDATE NOW(),

    PRIMARY KEY (id),
    UNIQUE (name, user),
    FOREIGN KEY (user) REFERENCES HVE_USER (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE HVE_TREE_ARCHIVE (
    id                  INT(20)         AUTO_INCREMENT,
    oldId               INT(20)         NOT NULL,
    jsonNodes           TEXT            NOT NULL,
    jsonRelations       TEXT            NOT NULL,
    jsonDrawnObjects    TEXT            NOT NULL,
    jsonNotes           TEXT            NOT NULL,
    user                INT(20)         NOT NULL,
    lastUser            INT(20)         NOT NULL,
    name                VARCHAR(100)    NOT NULL,
    message             VARCHAR(256)    ,
    exercise            INT(20)         ,
    slug                VARCHAR(200)    NOT NULL,
    version             DECIMAL(4,2)    NOT NULL,
    
    created_at          TIMESTAMP       NOT NULL DEFAULT 0,
    updated_at          TIMESTAMP       NOT NULL DEFAULT NOW() ON UPDATE NOW(),

    PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----- Exercise --------------------------------------------------------------
CREATE TABLE HVE_EXERCISE (
    id                  INT(20)         AUTO_INCREMENT,
    user                INT(20)         NOT NULL,
    name                VARCHAR(100)    NOT NULL,
    slug                VARCHAR(200)    NOT NULL,
    description         TEXT            NOT NULL,
    
    jsonNodes           TEXT            NOT NULL,
    jsonRelations       TEXT            NOT NULL,
    jsonDrawnObjects    TEXT            NOT NULL,
    jsonNotes           TEXT            NOT NULL,
    
    created_at          TIMESTAMP       NOT NULL DEFAULT 0,
    updated_at          TIMESTAMP       NOT NULL DEFAULT NOW() ON UPDATE NOW(),

    PRIMARY KEY (id),
    UNIQUE (name, user),
    FOREIGN KEY (user) REFERENCES HVE_USER (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE HVE_USERHASEXERCISE (
    id                  INT(20)         AUTO_INCREMENT,
    exercise            INT(20)         NOT NULL,
    user                INT(20)         NOT NULL,
    
    created_at          TIMESTAMP       NOT NULL DEFAULT 0,
    updated_at          TIMESTAMP       NOT NULL DEFAULT NOW() ON UPDATE NOW(),

    PRIMARY KEY (id),
    FOREIGN KEY (exercise) REFERENCES HVE_EXERCISE (id),
    FOREIGN KEY (user) REFERENCES HVE_USER (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE HVE_EXERCISESOLUTION (
    id                  INT(20)         AUTO_INCREMENT,
    exercise            INT(20)         NOT NULL,
    tree                INT(20)         NOT NULL,
    
    created_at          TIMESTAMP       NOT NULL DEFAULT 0,
    updated_at          TIMESTAMP       NOT NULL DEFAULT NOW() ON UPDATE NOW(),

    PRIMARY KEY (id),
    FOREIGN KEY (exercise) REFERENCES HVE_EXERCISE (id),
    FOREIGN KEY (tree) REFERENCES HVE_TREE (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----- Tag -------------------------------------------------------------------
CREATE TABLE HVE_TAG (
    id                  INT(20)         AUTO_INCREMENT,
    body                VARCHAR(40)     NOT NULL,
    slug                VARCHAR(80)     NOT NULL,
    
    created_at          TIMESTAMP       NOT NULL DEFAULT 0,
    updated_at          TIMESTAMP       NOT NULL DEFAULT NOW() ON UPDATE NOW(),

    PRIMARY KEY (id),
    UNIQUE (body)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE HVE_TREEHASTAG (
    id                  INT(20)         AUTO_INCREMENT,
    tree                INT(20)         NOT NULL,
    tag                 INT(20)         NOT NULL,
    
    created_at          TIMESTAMP       NOT NULL DEFAULT 0,
    updated_at          TIMESTAMP       NOT NULL DEFAULT NOW() ON UPDATE NOW(),

    PRIMARY KEY (id),
    FOREIGN KEY (tree)  REFERENCES HVE_TREE (id),
    FOREIGN KEY (tag)   REFERENCES HVE_TAG (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE HVE_USERHASTAG (
    id                  INT(20)         AUTO_INCREMENT,
    user                INT(20)         NOT NULL,
    tag                 INT(20)         NOT NULL,
    
    created_at          TIMESTAMP       NOT NULL DEFAULT 0,
    updated_at          TIMESTAMP       NOT NULL DEFAULT NOW() ON UPDATE NOW(),

    PRIMARY KEY (id),
    FOREIGN KEY (user)  REFERENCES HVE_USER (id),
    FOREIGN KEY (tag)   REFERENCES HVE_TAG (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----- Notice ----------------------------------------------------------------
CREATE TABLE HVE_NOTICE (
    id                  INT(20)         AUTO_INCREMENT,
    user                INT(20)         NOT NULL,
    title               VARCHAR(64)     NOT NULL,
    body                VARCHAR(256)    NOT NULL,
    private             BOOLEAN         DEFAULT 1,
    type                VARCHAR(32)     DEFAULT "TREE_CREATED",
    
    created_at          TIMESTAMP       NOT NULL DEFAULT 0,
    updated_at          TIMESTAMP       NOT NULL DEFAULT NOW() ON UPDATE NOW(),

    PRIMARY KEY (id),
    FOREIGN KEY (user) REFERENCES HVE_USER (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----- Default Users ---------------------------------------------------------
INSERT INTO HVE_USER (id, email, fullname, password, occupation, level, active, lastLoggedin, created_at, updated_at) 
    VALUES
        (1, 'carlos.avim@gmail.com', 'Carlos Avimadjessi', '$2y$12$chfjCXGB1mFhokVzvP1WCOcLkrNCMiDJ5BuKu4mnAkS6ZO82jl50m', 'Analyste Programmeur', 9, 1, '2015-09-25 20:57:57', '2014-12-23 11:00:00', '2015-09-25 20:57:57'),
        (2, 'arborescence@gramm-r.ulb.ac.be', 'Jane Doe', '$2y$12$W1.uWddKx1TG4iW9NaBhoeIbezz3f2k9YARJ6BfC764ORHqnseh3W', 'Inconnu', 1, 1, '2015-09-23 10:41:05', '2014-12-23 11:00:00', '2015-09-23 10:41:05'),
        (3, 'lmeinert@gmail.com', 'Lionel Meinertzhagen', '$2y$12$W1.uWddKx1TG4iW9NaBhoeIbezz3f2k9YARJ6BfC764ORHqnseh3W', 'Chercheur en linguistique française', 9, 1, '0000-00-00 00:00:00', '2014-12-23 11:00:00', '2015-09-23 09:02:38');