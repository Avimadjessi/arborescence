<?php
use \helpers\session,
	\helpers\date;
?>
<i id="menuToggle" onclick="javascript:menuToggle();" class="btn fa fa-bars"
	onmouseover="javascript:menuToggleOver();" onmouseout="javascript:menuToggleOut()">
</i>
<div id="sidemenu" class="active">
	<div id="sidemenu-content">
		<section class="menuheader">
			<?php 
			if (Session::get('loggedin'))
			{
			?>
				<img id="user-profile-image" 
					src="<?php echo helpers\url::template_path() . 'images/profile/_profile_' . $data['user']->email . '.png' ?>">
			<?php
			}
			?>
			<h3 id="menu-title"><?php echo $data['sub-title']?></h3>
		</section>
		<div class="menubody">
			<section>
				<h5 id="miHome" class="sidemenu-h5">
					<a href="<?php echo DIR?>" class="sidemenu-menu">
						<i class="fa fa-home"></i>Accueil
					</a>
				</h5>
			</section>
			<?php 
			if (Session::get('loggedin'))
			{
			?>
				<section>
					<h5 id="miAccount" class="sidemenu-h5">
						<a href="javascript:collapseOrFoldMenu('accountItems');" class="sidemenu-menu">
							Mon compte
						</a>
					</h5>
					<ul id="accountItems">
						<li id="miProfile" class="sidemenu-li">
							<a href="<?php echo DIR?>profile" class="sidemenu-item">Profil</a>
						</li>
						<?php 
						if ($data['user']->level != 1)
						{
						?>
							<li id="miEditProfile" class="sidemenu-li">
								<a href="<?php echo DIR?>profile/edit" class="sidemenu-item">Modifier mon profil</a>
							</li>
							<li id="miEditProfileImage" class="sidemenu-li">
								<a href="<?php echo DIR?>profile/edit-image" class="sidemenu-item">Modifier mon image de profil</a>
							</li>
							<li id="miDeleteProfile" class="sidemenu-li">
								<a href="<?php echo DIR?>profile/delete" class="sidemenu-item">Supp. mon compte</a>
							</li>
						<?php
						}
						?>
					</ul>
				</section>

				<?php 
				if ($data['user']->level == 9)
				{
				?>
					<section>
						<h5 id="miAdmin" class="sidemenu-h5">
							<a href="javascript:collapseOrFoldMenu('adminItems');" class="sidemenu-menu">
								Outils administrateur
							</a>
						</h5>
						<ul id="adminItems">
							<li id="miAdminAbout" class="sidemenu-li">
								<a href="<?php echo DIR?>admin/about" class="sidemenu-item">Informations</a>
							</li>
							<li id="miAdminNotice" class="sidemenu-li">
								<a href="<?php echo DIR?>admin/notice" class="sidemenu-item">Publier une annonce</a>
							</li>
							<li id="miAdminExercise" class="sidemenu-li">
								<a href="<?php echo DIR?>admin/exercise/canvas" class="sidemenu-item">Créer un exercice</a>
							</li>
							<li id="miAdminSearch" class="sidemenu-li">
								<a href="<?php echo DIR?>admin/search" class="sidemenu-item">Recherche</a>
							</li>
						</ul>
					</section>
				<?php
				} 
				?>

				<section>
					<h5 class="sidemenu-h5">
						<a href="javascript:collapseOrFoldMenu('treesItems');" class="sidemenu-menu">
							Mes arbres
						</a>
					</h5>
					<ul id="treesItems">
						<li id="miTrees" class="sidemenu-li">
							<a href="<?php echo DIR?>trees" class="sidemenu-item">Liste des arbres</a>
						</li>
						<li id="miCanvas" class="sidemenu-li">
							<a href="<?php echo DIR?>trees/canvas" class="sidemenu-item">Créer un arbre</a>
						</li>
					</ul>
				</section>

				<section>
					<h5 id="miHelp" class="sidemenu-h5">
						<a href="<?php echo DIR?>trees/canvas/help" class="sidemenu-menu">Aide au dessin</a>
					</h5>
				</section>
			<?php
			} else
			{
			?>
				<section>
					<h5 id="miBaseAccount" class="sidemenu-h5">
						<a href="javascript:collapseOrFoldMenu('baseAccountItems');" class="sidemenu-menu">
							Connexion
						</a>
					</h5>
					<ul id="baseAccountItems">
						<li id="miLogin" class="sidemenu-li">
							<a href="<?php echo DIR?>login" class="sidemenu-item">Se connecter</a>
						</li>
						<li id="miRegister" class="sidemenu-li">
							<a href="<?php echo DIR?>register" class="sidemenu-item">Créer un compte</a>
						</li>
						<li id="miRecover" class="sidemenu-li">
							<a href="<?php echo DIR?>recover" class="sidemenu-item">Récupérer un compte</a>
						</li>
					</ul>
				</section>
			<?php
			}
			?>

			<section>
				<h5 id="miFaq" class="sidemenu-h5">
					<a href="<?php echo DIR?>faq" class="sidemenu-menu">FAQ</a>
				</h5>
			</section>
			<section>
				<h5 id="miAbout" class="sidemenu-h5">
					<a href="<?php echo DIR?>about" class="sidemenu-menu">À propos</a>
				</h5>
			</section>
			<?php 
			if (Session::get('loggedin'))
			{
			?>
				<section>
					<h5 id="miLogout" class="sidemenu-h5">
						<a href="<?php echo DIR?>logout" class="sidemenu-item">
							<i class="fa fa-sign-out"></i>Déconnexion 
						</a>
					</h5>
				</section>
			<?php
			}
			?>
		</div>
		<br/>
	
		<!-- ========== T A G S . L I S T =========================== ========== -->
		<?php 
		if(Session::get('loggedin'))
		{
			$userTags 	= Session::get('userTags');
			if ($userTags)
			{
		?>
				<div id="user-tag-list" class="tag-list">
					<label>Mots clés</label><hr class="small"/>
			
					<?php
						foreach ($userTags as $key => $tag) 
						{
					?>
							<a class="tag" href="<?php echo DIR.'trees/tag/'.$tag->slug?>"><?php echo $tag->body; ?></a>
					<?php
						} 
					?>
			    </div>
		<?php
			}
		}
		?>

		<!-- ========== N O T E S . L I S T ========================= ========== -->
		<?php 
		if(Session::get('loggedin'))
		{
			$userNotices 	= Session::get('userNotices');
			if ($userNotices)
			{
		?>
				<div id="user-notice-list">
					<br/>
					<label>Notifications</label> 
					<a href="javascript:displayAllNotices();" class="float-right">Tout afficher</a>
					<hr class="small"/>
					<?php
					foreach ($userNotices as $key => $notice) 
					{
						if ($notice->private == true)
						{
							switch ($notice->type) 
							{
								case 'TREE_CREATED':
									$color = 'success';
									break;
								case 'TREE_DELETED':
									$color = 'success';
									break;
								case 'TREE_SENT':
									$color = 'info';
									break;
								case 'TREE_RECEIVED':
									$color = 'info';
									break;
								default:
									$color = 'success';
									break;
							}
						} else
							$color = 'warning';
					?>
						<label class="label label-<?php echo $color; ?> notice-header">
							<?php echo $notice->title; ?>
						</label> 
						<b class="notice-title"><?php echo Date::verbose($notice->created_at) ?></b> 
						<br/> 
						<span class="notice-message"><?php echo $notice->body; ?></span>
						<br/>
						<br/>
					<?php
					} 
					?>
			    </div>
		<?php
			}
		}
		?>
		<hr/>
		<span class="font-ninety">
			Gramm-R &copy; Arborescence 2014-2015
			<!-- <br/> -->
			<!-- Powered by <a href="http://simplemvcframework.com">SimpleMvcFramework</a> -->
		</span>
		<hr/>
		
	</div>
	
</div>
