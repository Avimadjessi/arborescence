var arborescence = arborescence || {};

arborescence.Drawing = (function ()
{

    // ===== C O N S T R U C T O R =============================================
    function TreeDrawing()
    {
        this.canvas             = arborescence.canvas;
        this.canvas.width       = arborescence.WIDTH;
        this.canvas.height      = arborescence.HEIGHT;
        $("#arbre").width(arborescence.DEFAULT_WIDTH);
        $("#arbre").height(arborescence.DEFAULT_HEIGHT);

        // EaselJS Stage
        this.stage              = new createjs.Stage(this.canvas);
        this.stage.enableMouseOver();
        this.stage.on("stagemousedown", this.handleCanvasMouseDown, this);
        createjs.Touch.enable(this.stage);

        // Camera
        this.camera = new createjs.Container();
        this.stage.addChild(this.camera);

        // 
        this.drawGridFlag                   = true;
        this.snapPointsFlag                 = true;
        this.isToolActive                   = false;
        this.isRelationAttenteToolActive    = false;
        this.isDeleteToolActive             = false;
        this.isNoteToolActive               = false;
        this.snapPoints                     = [];
        
        this.dashedArc                      = null;

        // Game clock
        createjs.Ticker.setFPS(40);
    };
    createjs.Graphics.prototype.dashedArrow = function(x1, y1, x2, y2, dashLen) 
    {
        this.moveTo(x1, y1);

        var dX = x2 - x1;
        var dY = y2 - y1;
        var dashes = Math.floor(Math.sqrt(dX * dX + dY * dY) / dashLen);
        var dashX = dX / dashes;
        var dashY = dY / dashes;

        var q = 0;
        while (q++ < dashes) 
        {
            x1 += dashX;
            y1 += dashY;
            this[q % 2 === 0 ? 'moveTo' : 'lineTo'](x1, y1);
        }
        this[q % 2 === 0 ? 'moveTo' : 'lineTo'](x2, y2); 
    };

    var prototype = TreeDrawing.prototype;

    prototype.resetDrawing                  = function ()
    {
        createjs.Ticker.removeAllEventListeners();
        createjs.Ticker.on('tick', this.handleTick, this);
        this.camera.removeAllChildren();
        this.camera.x = 0;
        
        this.selectedNode = null;
    };
    prototype.initDrawing                   = function (treeToDraw)
    {
        this.resetDrawing();

        this.tree = treeToDraw || new arborescence.Tree();

        // ===== E R R O R S // ================================================
        

        this.draw();
    };
    
    // ====================================================================== //
    // ========== // D . R . A . W // ======================================= //
    // ====================================================================== //
    prototype.draw                          = function ()
    {
        // this.checkAndResizeCanvas();

        // C L E A R // C A N V A S ============================================
        this.camera.removeAllChildren();

        // === D R A W // G R I D ==============================================
        if (this.drawGridFlag)
            this.drawGrid();
        
        // === D R A W // T I T L E ============================================
        this.drawTitle();
        
        // === D R A W // N O D E S  ===========================================
        this.drawNodes();

        // ===== D R A W // R E L A T I O N S ==================================
        this.drawRelations();  
        
        // ===== D R A W // D R A W N . O B J E C T S ==========================
        this.drawDrawnObjects();

        // ===== D R A W // N O T E S ==========================================
        this.drawNotes();

        this.drawTags ();
    };
    // D r a w . G r i d -------------------------------------------------------
    prototype.drawGrid                      = function ()
    {
        var gapX = 64;
        var gapY = 64;
        var width = arborescence.WIDTH;
        var height = arborescence.HEIGHT;
  
        var limitW = height / gapY;
        var limitH = width / gapX;

        for (var i = 0; i < limitW; i++)
        {
            for (var z = 1; z < 8; z++)
            {
                var xSmStart = 0;
                var ySmStart = (i * gapY) + z * 8;

                var xSmEnd = width;
                var ySmEnd = (i * gapY) + z * 8;
                var smBgLine = arborescence.TreeShapes.line({
                    color: "#002b36",
                    x1: xSmStart,
                    y1: ySmStart,
                    x2: xSmEnd,
                    y2: ySmEnd
                });
                smBgLine.alpha = 0.05;
                this.camera.addChild(smBgLine);
            }

            var xStart = 0;
            var yStart = i * gapY;
            var xEnd = width;
            var yEnd = i * gapY;
            var bgLine = arborescence.TreeShapes.line({
                color: "#002b36",
                x1: xStart,
                y1: yStart,
                x2: xEnd,
                y2: yStart
            });
            bgLine.alpha = 0.1;
            this.camera.addChild(bgLine);
        }
        for (var j = 0; j < limitH; j++)
        {
            for (var y = 1; y < 8; y++)
            {
                var xSmStart = (j * gapY) + y * 8;
                var ySmStart = 0;

                var xSmEnd = (j * gapY) + y * 8;
                var ySmEnd = height;
                var smBgLine = arborescence.TreeShapes.line({
                    color: "#002b36",
                    x1: xSmStart,
                    y1: ySmStart,
                    x2: xSmEnd,
                    y2: ySmEnd
                });
                smBgLine.alpha = 0.05;
                this.camera.addChild(smBgLine);
            }

            var xStart = j * gapY;
            var yStart = 0;
            var xEnd = j * gapY;
            var yEnd = height;
            var bgLine = arborescence.TreeShapes.line({
                color: "#002b36",
                x1: xStart,
                y1: yStart,
                x2: xEnd,
                y2: yEnd
            });
            bgLine.alpha = 0.1;
            this.camera.addChild(bgLine);
        }

        // ===== S N A P . P O I N T S =========================================
        if (this.snapPointsFlag)
        {
            for (var i=0; i<limitW+1; i++) 
            {
                var p = new createjs.Shape();
                p.graphics.f("#D0DC47").dc(0,0,1);
                p.set({x:0, y:i*64});
                p.alpha     = 1;
                this.camera.addChild(p);
                this.snapPoints.push(p);
            }
            for (var j=0; j<limitH+1; j++) 
            {
                var p = new createjs.Shape();
                p.graphics.f("#D0DC47").dc(0,0,1);
                p.set({x:j*64, y:0});
                p.alpha     = 1;
                this.camera.addChild(p);
                this.snapPoints.push(p);
            }
            for (var i=0; i<limitW+1; i++) 
            {
                for (var j=0; j<limitH+1; j++) 
                {
                    var p = new createjs.Shape();
                    p.graphics.f("#D0DC47").dc(0,0,1);
                    p.set({x:j*64, y:i*64});
                    p.alpha     = 1;
                    this.camera.addChild(p);
                    this.snapPoints.push(p);
                }
            }
            for (var i=1; i<limitW*2+1; i++) 
            {
                for (var j=1; j<limitH*2+1; j++) 
                {
                    var p = new createjs.Shape();
                    p.graphics.f("#D0DC47").dc(0,0,1);
                    p.set({x:j*32, y:i*32});
                    p.alpha     = 1;
                    this.camera.addChild(p);
                    this.snapPoints.push(p);
                }
            }
        }
    };
    // D r a w . T i t l e -----------------------------------------------------
    prototype.drawTitle                     = function ()
    {
        if (this.tree.title.value)
        {
            var title = new createjs.Text(this.tree.title.value,
                    this.tree.title.bold + " " + this.tree.title.italic + " "
                    + this.tree.title.size + "px "+arborescence.FONT, "#000");
            title.x = (arborescence.WIDTH / 2) - (title.getBounds().width / 2);
            title.y = 8;
            this.camera.addChild(title);
        }
    };
    // D r a w . N o d e s -----------------------------------------------------
    prototype.drawNodes                     = function ()
    {
        for (j = 0; j < this.tree.nodesToDraw.length; j++)
        {
            var node = this.tree.nodesToDraw[j];
            if(node && !node.properties.isDeleted)
            {
                // H I T  // A R E A
                var shape = arborescence.TreeShapes.rectangle({
                    'width': arborescence.NODEWIDTH,
                    'height': arborescence.NODEHEIGHT
                });

                // N O D E // T E X T
                node.drawing.color   = (node.properties.isVisible) ? ((node.properties.isValueVisible) ? arborescence.Colors.BLACK : arborescence.Colors.SUCCESS) : arborescence.Colors.DANGER;

                var nodeDrawing = arborescence.TreeShapes.node(node);

                shape.alpha     = 0.01;
                shape.x         = node.drawing.position.x - arborescence.NODEWIDTH/2 - 4;
                shape.y         = node.drawing.position.y - 4;
                shape.name      = "node";
                shape.cursor    = "pointer";

                this.drawNodeSideLines(node);

                var data = 
                {
                    node: node,
                    hitArea: shape
                };

                this.camera.addChild(shape);
                this.camera.addChild(nodeDrawing);

                // === N O D E // M O U S E // E V E N T S =====================
                shape.on("click", this.handleNodeClick, 
                    this, false, data);
                shape.on("pressmove", this.handleNodePressmove, 
                    this, false, data);
                shape.on("mousedown", this.handleMouseDown, this);
            }
        }
    };
    prototype.drawNodeSideLines             = function (node)
    {
        var color = (node.properties.isChildrenVisible) ? arborescence.Colors.BLACK : arborescence.Colors.DANGER;
        if (node.left)
        {
            var x1 = node.drawing.position.x;
            var y1 = node.drawing.position.y + arborescence.NODEHEIGHT + arborescence.FONTSIZE;

            var x2 = node.left.drawing.position.x + arborescence.NODEWIDTH/2;
            var y2 = node.left.drawing.position.y + arborescence.FONTSIZE;

            var leftLine = arborescence.TreeShapes.line({
                x1: x1,
                y1: y1,
                x2: x2,
                y2: y2
            });
            this.camera.addChild(leftLine);
        }

        if (node.right)
        {
            var x3 = node.drawing.position.x;
            var y3 = node.drawing.position.y + arborescence.NODEHEIGHT + arborescence.FONTSIZE;

            var x4 = node.right.drawing.position.x - arborescence.NODEWIDTH/2;
            var y4 = node.right.drawing.position.y + arborescence.FONTSIZE;

            var rightLine = arborescence.TreeShapes.line({
                x1: x3,
                y1: y3,
                x2: x4,
                y2: y4
            });
            rightLine.color = node.drawing.color;

            this.camera.addChild(rightLine);
        }
    };
    // D r a w . R e l a t i o n s ---------------------------------------------
    prototype.drawRelations                 = function ()
    {
        for (var k = 0; k < this.tree.relationsToDraw.length; k++)
        {
            var relation = this.tree.relationsToDraw[k];
            if( relation && !relation.properties.isDeleted)
            {
                var relationDrawing = arborescence.TreeShapes.relation(
                    relation);
                var data = 
                {
                    relation: relation
                };
                // === R E L A T I O N // M O U S E // E V E N T S =============
                relationDrawing.on("click", this.handleRelationClick, this, false, data);
                this.camera.addChild(relationDrawing);
            }
        }
    };
    // D r a w . D r a w n O b j e c t s ---------------------------------------
    prototype.drawDrawnObjects              = function ()
    {
        for (var l = 0; l < this.tree.drawnObjectsToDraw.length; l++)
        {
            var drawnObject = this.tree.drawnObjectsToDraw[l];
            if(!drawnObject.isDeleted)
            {
                var drawnObjectDrawing = arborescence.TreeShapes.drawnObject(drawnObject);
                var data = 
                {
                    drawnObject : drawnObject
                };
                drawnObjectDrawing.on("click", this.handleDrawnObjectClick, this, false, data);
                drawnObjectDrawing.on("pressmove", this.handleDrawnObjectPressmove, this, false, data);
                this.camera.addChild(drawnObjectDrawing);
            }
        }
    };
    // D r a w . N o t e s -----------------------------------------------------
    prototype.drawNotes                      = function ()
    {
        for (var i = 0; i < this.tree.notes.length; i++) 
        {
            var note    = this.tree.notes[i];
            // N O T E // T E X T
            var noteDrawing = arborescence.TreeShapes.note(note);

            // H I T  // A R E A
            var shape = arborescence.TreeShapes.postIt({
                'width'             : noteDrawing.getBounds().width + 12,
                'height'            : noteDrawing.getBounds().height + 16,
                'strokeThickness'   : 0.1,
                'fillColor'         : note.drawing.background,
                'strokeColor'       : arborescence.Colors.SUCCESS,
                'alpha'             : 0.8 
            });
            shape.x         = note.drawing.position.x - 6;
            shape.y         = note.drawing.position.y - 8;
            
            var data = {
                note        : note,
                hitArea     : shape
            };

            this.camera.addChild(shape);
            this.camera.addChild(noteDrawing);

            // === N O D E // M O U S E // E V E N T S =====================
            shape.on("click", this.handleNoteClick, 
                this, false, note);
            shape.on("pressmove", this.handleNotePressmove, 
                this, false, data);
        }
    };
    // D r a w . T a g s -------------------------------------------------------
    prototype.drawTags                      = function ()
    {
        $("#tag-list").empty();
        var tagListContent = "";

        for (var i = 0; i < this.tree.treeTags.length; i++) 
        {
            if (!this.tree.treeTags[i].deleted)
            {
                tagListContent += 
                    "<a class='tag' href='javascript:arborescence.drawing.displayTreesByTag(\"" + this.tree.treeTags[i].body + "\")'>" 
                        + this.tree.treeTags[i].body + 
                    "</a><a class='tagDelete' href='javascript:arborescence.drawing.deleteTreeTag(\"" + this.tree.treeTags[i].body + "\")'><i class='fa fa-times'></i></a>";
            }
        };

        $('#tag-list').append( tagListContent );
    };

    // ====================================================================== //
    // ====================================================================== //

    // ========== T I C K E R // F U N C T I O N ===============================
    prototype.handleTick                    = function (event)
    {
        this.updateView();
    };

    // ========= U P D A T E // S C R E E N ====================================
    prototype.updateView                    = function ()
    {
        this.stage.update();
    };


    // ====================================================================== //
    // ========== // E . V . E . N . T . S // =============================== //
    // ====================================================================== //
    // ===== C A N V A S 
    prototype.handleCanvasMouseDown         = function (event)
    {
        if(this.isToolActive)
        {
            if(this.isNoteToolActive) 
            {
                arborescence.key            += 1;
                var note                    = new arborescence.Note();
                note.key                    = arborescence.key;
                note.drawing.position.x     = Math.round(event.stageX);
                note.drawing.position.y     = Math.round(event.stageY);

                this.handleNoteClick(null, note);
            }
        }
    };
    // ===== O T H E R S
    prototype.handleNoteClick               = function (event, note)
    {
        if(this.isDeleteToolActive)
        {
            $('#noteKeyToDelete').val(note.key);

            $('#noteModalDelete').modal('show');
        } else 
        {
            // Create new
            $("#note-x-position").val(note.drawing.position.x);
            $("#note-y-position").val(note.drawing.position.y);
            $("#note-font-size").val(note.drawing.fontSize);
            $("#note-key").val(note.key);                         
            $("#note-width").val(note.drawing.width);            
            $("#note-value").val(note.value);

            $("#noteModal").modal('show');

            // Disable tools
            this.useTools();
        }
    };
    prototype.handleNotePressmove           = function (event, data)
    {
        if(!this.isToolActive)
        {
            // if (this.snapPointsFlag)
            // {
            //     var neighbour,          // what we want to snap to
            //         dist,               // The current distance to our snap partner
            //         snapDistance=20;    // How close to be to snap
            //     for (var i=0, l=this.snapPoints.length; i<l; i++) {
            //         var p = this.snapPoints[i];
                    
            //         // Determine the distance from the mouse position to the point
            //         var diffX = Math.abs(event.stageX - p.x);
            //         var diffY = Math.abs(event.stageY - p.y); 
            //         var d = Math.sqrt(diffX*diffX + diffY*diffY);        
                    
            //         // If the current point is closeEnough and the closest (so far)
            //         // Then choose it to snap to.
            //         var closest = (d<snapDistance && (dist == null || d < dist));
            //         if (closest) {
            //             neighbour = p; 
            //             dist = d;
            //         }
            //         // Continue to check others (don't break) in case something is closer.
            //     }
                
            //     // If there is a close neighbour, snap to it. 
            //     if (neighbour) {
            //         data.hitArea.x = neighbour.x;
            //         data.hitArea.y = neighbour.y;
                    
            //     // Otherwise snap to the mouse
            //     } else {
            //         data.hitArea.x = event.stageX;
            //         data.hitArea.y = event.stageY;
            //     }
            // } else
            // {
                data.hitArea.x = Math.round(event.stageX);
                data.hitArea.y = Math.round(event.stageY);
            // }

            // U P D A T E . N O D E . P O S I T I O N
            this.tree.updateNotePosition(data.note.key, Math.round(data.hitArea.x), Math.round(data.hitArea.y));

            this.tree.createItemList();
            this.draw();
        }
    };
    prototype.handleNodeClick               = function (event, data)
    {
        if(this.isToolActive)
        {
           if(this.isDeleteToolActive)
           {
                $('#nodeKeyToDelete').val(data.node.key);
                $('#nodeModalDelete').modal('show');
           } 
        } else
        {
            // Fill form
            $("#node-attente-fonction").val(data.node.value.fonction);
            $("#relationAtt-key").val(data.node.key);
            $("#node-fonction").val(data.node.value.fonction);
            $("#node-si").val(data.node.value.structureIntegrative);
            $("input[name='terme']").val(data.node.value.terme);
            $("input[name='key']").val(data.node.key);

            // Fill Adv form
            $("#node-x-position").val(data.node.drawing.position.x);
            $("#node-y-position").val(data.node.drawing.position.y);
            $("#node-font-size").val(data.node.drawing.fontSize);
            $("#node-adv-key").val(data.node.key);
        
            if (data.node.properties.isChildrenVisible)
                $("#node-children-visible").prop('checked', true);
            else
                $("#node-visible").prop('checked', false);
            if(data.node.properties.isValueVisible)
                $("#node-value-visible").prop('checked', true);
            else
                $("#node-value-visible").prop('checked', false);


            if (data.node.drawing.type === arborescence.NodeType.ATTENTE)
                $('#nodeAttenteModal').modal('show');
            else
                $('#nodeModal').modal('show');
        }
    };
    prototype.handleRelationClick           = function (event, data)
    {
        if(this.isToolActive)
        {
            if(this.isDeleteToolActive)
           {
                $('#relationKeyToDelete').val(data.relation.key);
                $('#relationModalDelete').modal('show');
           } 
        } else
        {
            if(data.relation.drawing.type === arborescence.RelationType.ATTENTE 
                || data.relation.drawing.type === arborescence.RelationType.ATTENTESECOND)
            {
                $("#relationAtt-type").val(data.relation.drawing.type);
                $("#relationAtt-key").val(data.relation.key);
                    
                $('#relationAttModal').modal('show');
            }else
            {
                $("#relation-type").val(data.relation.drawing.type);
                $("input[name='relationKey']").val(data.relation.key);

                if (data.relation.properties.isVisible)
                    $("#relation-visible").prop('checked', true);
                else
                    $("#relation-visible").prop('checked', false);

                if(data.relation.properties.isValueVisible)
                    $("#relation-value-visible").prop('checked', true);
                else
                    $("#relation-value-visible").prop('checked', false);

                $('#relationModal').modal('show');
            }

            
        }
    };
    prototype.handleNodePressmove           = function (event, data)
    {
        if(!this.isToolActive)
        {
            if (this.snapPointsFlag)
            {
                var neighbour,          // what we want to snap to
                    dist,               // The current distance to our snap partner
                    snapDistance=20;    // How close to be to snap
                for (var i=0, l=this.snapPoints.length; i<l; i++) {
                    var p = this.snapPoints[i];
                    
                    // Determine the distance from the mouse position to the point
                    var diffX = Math.abs(event.stageX - p.x);
                    var diffY = Math.abs(event.stageY - p.y); 
                    var d = Math.sqrt(diffX*diffX + diffY*diffY);        
                    
                    // If the current point is closeEnough and the closest (so far)
                    // Then choose it to snap to.
                    var closest = (d<snapDistance && (dist == null || d < dist));
                    if (closest) {
                        neighbour = p; 
                        dist = d;
                    }
                    // Continue to check others (don't break) in case something is closer.
                }
                
                // If there is a close neighbour, snap to it. 
                if (neighbour) {
                    data.hitArea.x = neighbour.x;
                    data.hitArea.y = neighbour.y;
                    
                // Otherwise snap to the mouse
                } else {
                    data.hitArea.x = event.stageX;
                    data.hitArea.y = event.stageY;
                }
            } else
            {
                data.hitArea.x = event.stageX;
                data.hitArea.y = event.stageY;
            }

            // U P D A T E . N O D E . P O S I T I O N
            this.tree.updateNodePosition(data.node.key, data.hitArea.x, data.hitArea.y);

            this.tree.createItemList();
            this.draw();
        }
    };
    prototype.handleMouseDown               = function (event)
    {
        if(this.isToolActive)
        {
            if(this.isRelationAttenteToolActive)
            {
                this.dashedArc = new createjs.Shape().set(
                    {
                        x:event.stageX,
                        y:event.stageY,
                        mouseEnabled:false,
                        graphics: new createjs.Graphics().s("#00f").dc(0,0,10)
                    }
                );
                this.stage.addChild(this.dashedArc);
                this.stage.on("pressmove", this.drawArc, this, false);
                this.stage.on("pressup", this.endDrawArc, this, false);
            }
        }
    };
    prototype.handleDrawnObjectPressmove    = function (event, data)
    {
        data.drawnObject.drawing.position.xM = event.stageX;
        data.drawnObject.drawing.position.yM = arborescence.HEIGHT - event.stageY;

        this.tree.updateDrawnObjectPosition(data.drawnObject);
        this.draw();
    };
    prototype.handleDrawnObjectClick        = function (event, data)
    {
        if(this.isToolActive)
        {
            if(this.isDeleteToolActive)
           {
                $('#drawnObjectKeyToDelete').val(data.drawnObject.key);
                $('#drawnObjectModalDelete').modal('show');
           } 
       } else 
       {
            // F I L L . F O R M
            $("#drawnObjectType").val(data.drawnObject.drawing.objectType);
            $("input[name='drawnObjectKey']").val(data.drawnObject.key);
            $("#drawnObjectRelationType").val(data.drawnObject.drawing.type);

            $("#drawnObject-xS").val(data.drawnObject.drawing.position.xS);
            $("#drawnObject-yS").val(data.drawnObject.drawing.position.yS);
            $("#drawnObject-xF").val(data.drawnObject.drawing.position.xF);
            $("#drawnObject-yF").val(data.drawnObject.drawing.position.yF);

            
            $("#drawnObjectModal").modal('show');
        }
    };


    // ====================================================================== //
    // ========== // M . O . D . A . L . S // =============================== //
    // ====================================================================== //
    // ========== C A N V A S // R E S I Z E ===================================
    prototype.resizeCanvas                  = function ()
    {
        $('input[name="canvasWidth"]').val(arborescence.DEFAULT_WIDTH);
        $('input[name="canvasHeight"]').val(arborescence.DEFAULT_HEIGHT);

        $("#resizeCanvasModal").modal('show');
    };
    // ========== C H A N G E // F O N T =======================================
    prototype.changeFont                    = function ()
    {
        $('#tree-font-size').val(arborescence.FONTSIZE);
        $("#changeFontModal").modal('show');
    };
    // ========== E D I T // T R E E // T I T L E ==============================
    prototype.editTreeTitle                 = function ()
    {
        $('#treeBold').prop('checked', false);
        $('#treeItalic').prop('checked', false);

        $('#treeTitle').val(this.tree.title.value);
        if (this.tree.title.bold !== '')
            $('#treeBold').prop('checked', true);
        if (this.tree.title.italic !== '')
            $('#treeItalic').prop('checked', true);

        $("#editTreeTitleModal").modal('show');
    };
    // ========== L O A D // T R E E ===========================================
    prototype.showTreeList                  = function ()
    {
        
        $("#treeListModal").modal('show');
    };
    // ========== D E S C R ====================================================
    prototype.editExerciseDesc              = function ()
    {
        $('#exercise-description').val(this.tree.description);

        $("#editExerciseDescModal").modal('show');
    };

    // ====================================================================== //
    // ========== // A . C . T . I . O . N . S // =========================== //
    // ====================================================================== //
    // ========== E X E R C I S E // B E G I N // L O A D ===========================================
    prototype.submitBeginExercise                = function ()
    {
        var name    = $("#exercise-name").val();
        var id      = $("#exercise-id").val();
        if(!name || !id)
            return false;
        
        $("#begin-exercise-"+id).modal('hide');
        this.tree.beginExercise(id, name);
        this.drawTags(); 
    };
    // ========== C A N V A S // R E S I Z E ===================================
    prototype.submitResizeCanvas            = function ()
    {
        var canvasWidth = $('input[name="canvasWidth"]').val();
        var canvasHeight = $('input[name="canvasHeight"]').val();

        arborescence.DEFAULT_WIDTH      = canvasWidth;
        arborescence.DEFAULT_HEIGHT     = canvasHeight;

        if (arborescence.WIDTH < arborescence.DEFAULT_WIDTH)
            arborescence.WIDTH = arborescence.DEFAULT_WIDTH;

        if (arborescence.HEIGHT < arborescence.DEFAULT_HEIGHT)
            arborescence.HEIGHT = arborescence.DEFAULT_HEIGHT;

        $("#arbre").width(arborescence.DEFAULT_WIDTH);
        $("#arbre").height(arborescence.DEFAULT_HEIGHT);

        this.canvas.width   = arborescence.WIDTH;
        this.canvas.height  = arborescence.HEIGHT;

        this.draw();

        $("#resizeCanvasModal").modal("hide");
    };
    prototype.submitResizeCanvasWithDefault = function (width, height)
    {
        this.canvas.width       = width;
        this.canvas.height      = height;

        arborescence.WIDTH = this.canvas.width;
        arborescence.HEIGHT = this.canvas.height;

        if(arborescence.DEFAULT_WIDTH > arborescence.WIDTH)
            $("#arbre").width(arborescence.WIDTH);
        else
            $("#arbre").width(arborescence.DEFAULT_WIDTH);

        if(arborescence.DEFAULT_HEIGHT > arborescence.HEIGHT)
            $("#arbre").height(arborescence.HEIGHT);
        else
            $("#arbre").height(arborescence.DEFAULT_HEIGHT);

        this.draw();
    };
    // ========== C H A N G E // F O N T =======================================
    prototype.submitChangeFont              = function ()
    {
        var font = $('#tree-font').val();
        var fontSize = $('#tree-font-size').val();
        arborescence.FONT = font;
        arborescence.FONTSIZE = Number(fontSize);

        this.tree.updateNodesFont(arborescence.FONTSIZE);
        this.tree.updateNotesFont(arborescence.FONTSIZE);

        $("#changeFontModal").modal('hide');
        this.draw();
    };
    // ========== N O D E // E D I T ===========================================
    prototype.submitEditNode                = function (values)
    {
        var fonction                = $("#node-fonction").val();
        var structureIntegrative    = $("#node-si").val();
        var terme                   = 
            ($("input[name='terme']").val() !== '') ? $("input[name='terme']").val() : null;
        var key                     = $("input[name='key']").val();
        var value                   = values || {
            fonction: fonction,
            structureIntegrative: structureIntegrative, 
            terme: terme
        };

        // ==========// U P D A T E // N O D E /================================
        this.tree.updateNode(key, value);
        $('#nodeModal').modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    prototype.submitEditAdvNode             = function (values)
    {
        var nodeSize                = $("#node-font-size").val();
        var posX                    = $("#node-x-position").val();
        var posY                    = $("#node-y-position").val();
        var key                     = $("#node-adv-key").val();
        var value                   = values || {
            x               : posX,
            y               : posY, 
            fontSize        : nodeSize,
        };

        // ==========// U P D A T E // N O D E /================================
        this.tree.updateAdvNode(key, value);
        $('#nodeModal').modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    prototype.submitExerciseEditAdvNode     = function (values)
    {
        var nodeSize                = $("#node-font-size").val();
        var posX                    = $("#node-x-position").val();
        var posY                    = $("#node-y-position").val();
        var key                     = $("#node-adv-key").val();
        var isChildrenVisible       = $("#node-children-visible").is(':checked');
        var isValueVisible          = $("#node-value-visible").is(':checked');
        var value                   = values || {
            x               : posX,
            y               : posY, 
            fontSize        : nodeSize,
            isChildrenVisible       : isChildrenVisible,
            isValueVisible  : isValueVisible
        };


        // ==========// U P D A T E // N O D E /================================
        var node = this.tree.updateAdvNode(key, value);

        $('#nodeModal').modal('hide');

        this.tree.updateRelationsVisibility();

        this.tree.createItemList();
        this.draw();
    };
    // ========== N O D E // A T T E N T E // E D I T ==========================
    prototype.submitEditNodeAttente         = function ()
    {
        var fonction                = $("#node-attente-fonction").val();
        var structureIntegrative    = $("#node-si").val();
        var terme                   = 
            ($("input[name='terme']").val() !== '') ? $("input[name='terme']").val() : null;
        var values                  = 
        {
            fonction            : fonction,
            structureIntegrative: structureIntegrative, 
            terme               : terme,
            errorFlag           : false
        };
        this.submitEditNode(values);
        $('#nodeAttenteModal').modal('hide');
    };
    // ========== N O T E // E D I T ===========================================
    prototype.submitEditNote               = function () 
    {
        var key             = $("#note-key").val();
        var x               = $("#note-x-position").val();
        var y               = $("#note-y-position").val();
        var value           = $("#note-value").val();
        var width           = $("#note-width").val();
        var fontSize        = $("#note-font-size").val();

        var values          = {
            key             : key,
            value           : value,
            x               : x,
            y               : y,
            fontSize        : fontSize,
            width           : width
        };

        this.tree.updateOrCreateNote (values);
        this.draw();

        $("#noteModal").modal('hide');
    };
    // ========== R E L A T I O N // E D I T ===================================
    prototype.submitEditRelation            = function ()
    {
        var type = $("#relation-type").val();
        var key = $("input[name='relationKey']").val();

        var newValues = {
            type            : Number(type)
        };
        // ========== U P D A T E // R E L A T I O N ===========================
        this.tree.updateRelation(key, newValues);
        $("#relationModal").modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    prototype.submitExerciseEditRelation    = function ()
    {
        var type = $("#relation-type").val();
        var key = $("input[name='relationKey']").val();
        var isVisible               = $("#relation-visible").is(':checked');
        var isValueVisible          = $("#relation-value-visible").is(':checked');

        var newValues = {
            type            : Number(type),
            isVisible       : isVisible,
            isValueVisible  : isValueVisible
        };
        // ========== U P D A T E // R E L A T I O N ===========================
        this.tree.updateRelation(key, newValues);
        $("#relationModal").modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    prototype.submitEditRelationAtt         = function ()
    {
        var type = $("#relationAtt-type").val();
        var key = $("#relationAtt-key").val();
        var newValues = {
            type: Number(type)
        };
        // ========== U P D A T E // R E L A T I O N ===========================
        this.tree.updateRelation(key, newValues);
        $("#relationAttModal").modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    // ========== D R A W N // O B J E C T // E D I T ==========================
    prototype.submitEditDrawnObject         = function ()
    {
        var key     = $("input[name='drawnObjectKey']").val();
        var type    = $("#drawnObjectRelationType").val();

        var xS      = Number($("#drawnObject-xS").val());
        var yS      = Number($("#drawnObject-yS").val());
        var xF      = Number($("#drawnObject-xF").val());
        var yF      = Number($("#drawnObject-yF").val());

        var drawnObject = {
            key             : key,
            drawing         :
            {
                type    : type,
                position : 
                {
                    xS : xS,
                    yS : yS,
                    xF : xF,
                    yF : yF
                } 
            }
        };
        this.tree.editDrawnObject(drawnObject);
        
        $("#drawnObjectModal").modal('hide');
        this.draw();
    };
    // ========== D E L E T E // D R A W N // O B J E C T ======================
    prototype.submitDeleteDrawnObject       = function ()
    {
        var key = $("#drawnObjectKeyToDelete").val();
        this.tree.deleteDrawnObject(key);
        
        $("#drawnObjectModalDelete").modal('hide');
        this.draw();
    };
    // ========== D E L E T E // N O T E =======================================
    prototype.submitDeleteNote              = function ()
    {
        var key = $("#noteKeyToDelete").val();
        this.tree.deleteNote(key);
        
        $("#noteModalDelete").modal('hide');
        this.draw();
    };
    // ==========  D E L E T E // N O D E ======================================
    prototype.submitDeleteSubtree           = function ()
    {
        var key = $("#nodeKeyToDelete").val();

        this.tree.deleteSubtree(key);
        
        $('#nodeModalDelete').modal('hide');
        // $('#nodeAttenteModal').modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    prototype.submitDeleteRelationNode      = function ()
    {
        var relationKey = $("#relationKey").val();
        this.tree.deleteRelationNode(relationKey);

        if (this.deleteRelationNodeErrorFlag)
        {
            $("#err-delete-relationNode").show();
        } else
        {
            $("#err-delete-relationNode").hide();
            $("#relationModal").modal('hide');
            this.tree.createItemList();
            this.draw();
        }
    };
    // ==========  D E L E T E // R E L A T I O N ==============================
    prototype.submitDeleteRelation          = function ()
    {
        var relationKey = $("#relationKeyToDelete").val();
        this.tree.deleteRelation(relationKey);
        
        $("#relationModalDelete").modal('hide');
        this.tree.createItemList();
        this.draw();
    };
    prototype.submitLoadTree                = function ()
    {        
        var name = $("input[name='treeName']").val();
        if(!name || !password)
            return false;
        
        $("#saveOrLoadModal").modal('hide');
        this.tree.JSONToTree(name);
    };
    // ========== T R E E // S A V E ===========================================
    prototype.submitSaveTree                = function ()
    {
        var name = $("input[name='treeName']").val();
        if(!name)
            return false;
        
        $("#saveModal").modal('hide');
        this.tree.saveTree(name);
        $("#reloadModal").modal('show');
        this.drawTags(); 
    };
    prototype.submitSaveLoadedTree          = function ()
    {
        var name = $("input[name='treeName']").val();
        if(!name)
            return false;
        
        $("#saveModal").modal('hide');
        this.tree.saveTree(name);
        this.drawTags(); 
    };
    prototype.submitSaveExercise            = function ()
    {
        var name = $("#exercise-name").val();
        var description = $("#exercise-description").val();
        if(!name)
            return false;
        
        $("#saveModal").modal('hide');
        this.tree.saveExercise(name, description);
        this.drawTags(); 
    };
    // ========== T R E E // R E L O A D =======================================
    prototype.submitReloadTree              = function ()
    {
        $("#reloadModal").modal('hide');
        this.tree.reloadTree();
    };
    // ========== T R E E // S A V E // AS =====================================
    prototype.saveTree                      = function ()
    {
        $("#saveModal").modal('show');
    };
    prototype.saveExercise                  = function ()
    {
        $("#saveModal").modal('show');
    };
    // ========== E D I T // T R E E // T I T L E ==============================
    prototype.submitEditTreeTitle           = function ()
    {
        var bold    = $('#treeBold').is(':checked') ? 'bold' : '';
        var italic  = $('#treeItalic').is(':checked') ? 'italic' : '';
        var title   = 
        {
            value   : $('#treeTitle').val(),
            bold    : bold,
            italic  : italic
        };

        this.tree.updateTitle(title);
        $("#editTreeTitleModal").modal('hide');

        this.draw();
    };
    // ========== E D I T // E X E R C I S E // D E S C ========================
    prototype.submitEditExerciseDesc        = function ()
    {
        var desc    = $('#exercise-description').val();

        this.tree.updateDescription(desc);
        $("#editExerciseDescModal").modal('hide');

        this.draw();
    };
    // ========== A D D // T A G ===============================================
    prototype.submitAddTag                  = function ()
    {
        var tag         = $("#tagBody").val();
        var trimmedTag  = tag.trim();
        // var slug        = $('#tagTreeSlug').val();
        if (trimmedTag)
            this.tree.addTag(trimmedTag);
    };
    // ========== R E M O V E // T A G =========================================
    prototype.removeTag                     = function (tagBody)
    {
        if (tagBody)
        {
            this.tree.removeTag (tagBody);
        }
    };
    prototype.deleteTreeTag                 = function (tagBody)
    {
        if (tagBody)
        {
            this.tree.deleteTreeTag (tagBody);
            this.drawTags();
        }
    };
    // ========== V E R I F Y // E X E R C I S E ===============================
    prototype.verifyExercise                = function ()
    {
        $("#verifyExerciseMessage").addClass('hidden');
        $("#verifyExerciseUnsavedMessage").addClass('hidden');

        if (this.tree.isSaved())
        {
            this.tree.verifyExercise ();
        } else 
        {
            $("#verifyExerciseUnsavedMessage").removeClass('hidden');
            $("#verifyExerciseModal").modal('show');
        }
    };

    
    // ====================================================================== //
    // ========== // O . T . H . E . R . S // =============================== //
    // ====================================================================== //
    // ========== G R I D // H I D E & S E E K =================================
    prototype.switchDrawGridFlag            = function ()
    {
        this.drawGridFlag = this.drawGridFlag ? false : true;
        this.draw();
    };
    // ========== S N A P // H I D E & S E E K =================================
    prototype.switchSnapGridFlag            = function ()
    {
        this.snapPointsFlag = this.snapPointsFlag ? false : true;
        this.draw();
    };
    // ========== E X P O R T // ===============================================
    prototype.generatePNG                   = function ()
    {
        var imagePNG = this.canvas.toDataURL("image/png");
        window.open(imagePNG);
    };
    // ========== R E S I Z E // C A N V A S // ? // ===========================
    prototype.checkAndResizeCanvas = function ()
    {
        var extremPosition = 
        {
            x : 1,
            y : 1
        };
        var incWidth    = false;
        var incHeight   = false;

        for (j = 0; j < this.tree.nodesToDraw.length; j++)
        {
            var node = this.tree.nodesToDraw[j];
            var x = (node.drawing.position.x > extremPosition.x) ? node.drawing.position.x : extremPosition.x;
            var y = (node.drawing.position.y > extremPosition.y) ? node.drawing.position.y : extremPosition.y;

            extremPosition = 
            {
                x : x,
                y : y
            };
            // Check INC canvas size
            if (node.drawing.position.x <= 0 
                || (node.drawing.position.x + arborescence.NODEWIDTH) >= arborescence.WIDTH)
            {
                incWidth           = true;
            }
            if ((node.drawing.position.y + arborescence.NODEHEIGHT) >= arborescence.HEIGHT)
            {
                incHeight          = true;

            }
        }

        // ===== INC 
        if (incWidth)
        {
            arborescence.WIDTH      += arborescence.NODEWIDTH;
            this.canvas.width       = arborescence.WIDTH;
        }
        if (incHeight)
        {
            arborescence.HEIGHT     += arborescence.NODEHEIGHT;
            this.canvas.height      = arborescence.HEIGHT;
        }

        // ===== Check and DESC canvas size
        // if (((extremPosition.x + arborescence.NODEWIDTH) < arborescence.DEFAULT_WIDTH))
        // {
        //     arborescence.WIDTH      = arborescence.DEFAULT_WIDTH;
        //     this.canvas.width       = arborescence.WIDTH;
        // }
        // if (((extremPosition.y + arborescence.NODEHEIGHT) < arborescence.DEFAULT_HEIGHT))
        // {
        //     arborescence.HEIGHT     = arborescence.DEFAULT_HEIGHT;
        //     this.canvas.height      = arborescence.HEIGHT;
        // }
    };
    // ========== A D D // R E L A T I O N // A T T E N T E ====================
    prototype.addNodeAttenteOnNode          = function ()
    {
        var nodeKey = $("input[name='key']").val();
        // ========== U P D A T E // R E L A T I O N ===========================
        this.tree.addNodeAttenteOnNode(nodeKey);
        $("#nodeModal").modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    prototype.addLeftNodeAttenteOnNode      = function ()
    {
        var nodeKey = $("input[name='key']").val();
        // ========== U P D A T E // R E L A T I O N ===========================
        var param = { 
            'nodeKey'   : nodeKey,
            'side'      : 'left'
        };
        this.tree.addNodeAttenteOnNode(param);
        $("#nodeModal").modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    prototype.addRightNodeAttenteOnNode      = function ()
    {
        var nodeKey = $("input[name='key']").val();
        var param = { 
            'nodeKey'   : nodeKey,
            'side'      : 'right'
        };
        // ========== U P D A T E // R E L A T I O N ===========================
        this.tree.addNodeAttenteOnNode(param);
        $("#nodeModal").modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    // ========== A D D // N O D E // N O D E ==================================
    prototype.addNodeOnNode                 = function ()
    {
        var nodeKey = $("#nodeKey").val();
        // ========== U P D A T E // R E L A T I O N ===========================
        this.tree.addNodeOnNode(nodeKey);
        $("#nodeModal").modal('hide');
        $("#nodeAttenteModal").modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    // ========== A D D // N O D E // R E L A T I O N ==========================
    prototype.addNodeOnRelation             = function ()
    {
        var type = $("#relation-type").val();
        var relationkey = $("input[name='relationKey']").val();
        var values = {
            type: type
        };
        // ========== U P D A T E // R E L A T I O N ===========================
        this.tree.addNodeOnRelation(relationkey);
        $("#relationModal").modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    // ========== A D D // N O D E // R E L A T I O N // A T T =================
    prototype.addNodeOnRelationAtt          = function ()
    {
        var type = $("#relationAtt-type").val();
        var relationkey = $("#relationAtt-key").val();
        var values = {
            type: type
        };
        // ========== U P D A T E // R E L A T I O N ===========================
        this.tree.addNodeOnRelationAtt (relationkey);
        $("#relationAttModal").modal('hide');

        this.tree.createItemList();
        this.draw();
    };
    
    // ========== U S E // T O O L S ===========================================
    prototype.useTools                      = function ()
    {
        this.isToolActive = (this.isToolActive) ? false : true;

        this.isRelationAttenteToolActive    = false;
        this.isNoteToolActive               = false;
        this.isDeleteToolActive             = false;

        $("#drawTools").slideToggle();
        $("#tools").toggleClass("unactive");

        if(!$("#relationAttenteTool").hasClass("unactive"))
            $("#relationAttenteTool").addClass("unactive");
        if(!$("#deleteTool").hasClass("unactive"))
            $("#deleteTool").addClass("unactive");
        if(!$("#noteTool").hasClass("unactive"))
            $("#noteTool").addClass("unactive");
    };
    prototype.useToolArc                    = function ()
    {
        if(this.isToolActive)
        {
            // Disable other tools
            this.isDeleteToolActive         = false;
            $("#deleteTool").removeClass("unactive");
            $("#deleteTool").addClass("unactive");
            this.isNoteToolActive           = false;
            $("#noteTool").removeClass("unactive");
            $("#noteTool").addClass("unactive");

            this.isRelationAttenteToolActive = (this.isRelationAttenteToolActive) ? false : true;
            $("#relationAttenteTool").toggleClass("unactive");
            // this.isDeleteToolActive = (this.isDeleteToolActive) ? false : true;
            // $("#deleteTool").toggleClass("unactive");
            // this.isNoteToolActive = (this.isNoteToolActive) ? false : true;
            // $("#noteTool").toggleClass("unactive");
        }
    }; 
    prototype.useToolDelete                 = function ()
    {
        if(this.isToolActive)
        {
            // Disable other tools
            this.isRelationAttenteToolActive= false;
            $("#relationAttenteTool").removeClass("unactive");
            $("#relationAttenteTool").addClass("unactive");
            this.isNoteToolActive= false;
            $("#noteTool").removeClass("unactive");
            $("#noteTool").addClass("unactive");

            this.isDeleteToolActive = (this.isDeleteToolActive) ? false : true;
            $("#deleteTool").toggleClass("unactive");
        }
    };
    prototype.useToolNote                   = function ()
    {
        if(this.isToolActive)
        {
            // Disable other tools
            this.isRelationAttenteToolActive= false;
            $("#relationAttenteTool").removeClass("unactive");
            $("#relationAttenteTool").addClass("unactive");
            this.isDeleteToolActive= false;
            $("#deleteTool").removeClass("unactive");
            $("#deleteTool").addClass("unactive");

            this.isNoteToolActive = (this.isDeleteToolActive) ? false : true;
            $("#noteTool").toggleClass("unactive");
        }
    };
    prototype.drawArc                       = function (event)
    {
        if(this.isToolActive && this.isRelationAttenteToolActive)
        {
            this.dashedArc.graphics.clear();
            this.dashedArc.graphics.setStrokeStyle(1).beginStroke("#F00");
            this.dashedArc.graphics.dashedArrow(arborescence.NODEWIDTH/2 - 12, arborescence.NODEHEIGHT, 
                this.stage.mouseX-this.dashedArc.x, this.stage.mouseY-this.dashedArc.y, 4);
        }
    };
    prototype.endDrawArc                    = function (event)
    {
        if(this.isToolActive && this.isRelationAttenteToolActive)
        {
            var target, targets = 
                    this.stage.getObjectsUnderPoint(this.stage.mouseX, this.stage.mouseY);

            for (var i = 0; i < targets.length; i++)
            {
                if(targets[i].name === "node")
                {
                    target = targets[i];
                    break;
                }
            }
            if(target)
            {
                var xS  = arborescence.NODEWIDTH/2;
                var yS  = arborescence.NODEHEIGHT;
                var xF  = (target.x - this.dashedArc.x) + (arborescence.NODEWIDTH/2);
                var yF  = (target.y - this.dashedArc.y) + (arborescence.NODEHEIGHT);

                var xS  = this.dashedArc.x + arborescence.NODEWIDTH/2;
                var yS  = this.dashedArc.y;
                var xF  = this.dashedArc.x + xF;
                var yF  = this.dashedArc.y + yF;
                var xM  = xF;
                var yM  = (yS > yF) ? (yS - (yS - yF)/2) : (yF - (yF - yS)/2);

                var data = 
                {
                    object  : 
                    {
                        position :
                        {
                            xS : xS,
                            yS : yS,
                            xM : xM,
                            yM : yM,
                            xF : xF,
                            yF : yF
                        },
                        tilt        : arborescence.Tilt.CURVE,
                        objectType  : arborescence.ObjectType.RELATION,
                        align       : arborescence.Align.CENTER,
                        type        : 1
                    }
                };
                this.tree.addDrawnObject(data.object);
                this.draw();
            }

            this.stage.removeChild(this.dashedArc);

            this.stage.removeEventListener("pressmove", this.drawArc, this, false);
            this.stage.removeEventListener("pressup", this.endDrawArc, this, false);
            this.useTools();
        }
    };
    // =========================================================================
    // ========== A D M I N // =================================================
    prototype.adminSearchUsers              = function()
    {
        // Check Fields
        var name        = $("input[name='userUserName']").val();
        var email       = $("input[name='userUserEmail']").val();
        var tags        = $("input[name='userUserTags']").val(); 

        $searchCriteria = 
        {
            name      : name,
            email     : email,
            tags      : tags
        };
        this.tree.searchUsersTool($searchCriteria);
    }
    prototype.adminSearchTrees              = function()
    {
        this.tree.searchTreesTool();
    }


    // =========================================================================
    // ========== O T H E R S // ===============================================
    prototype.displayTreesByTag             = function (tag)
    {
        if (!arborescence.isSaved)
            alert('Veillez sauvegarder votre arbre avant de lancer cette recherche.');

        this.tree.searchTreeByTag(tag);
    };
    prototype.displayAllNotices             = function ()
    {
        this.tree.searchAllNotices();
    };
    prototype.displayDescription            = function ()
    {
        $('#tree-description').slideToggle();
    };

    return TreeDrawing;
})();

// ========== // ===============================================================
