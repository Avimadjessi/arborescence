var tree = tree || {};

tree.Object = (function()
{
    function Object()
    {
        this.initialize();
    };
    
    var prototype = Object.prototype;
    
    prototype.initialize = function()
    {
        this.key                = 0;
        
        this.errorFlag          = false;
        
        this.isDeleted          = false;
        
        this.xS                 = 0;
        this.yS                 = 0;
        this.xF                 = 0;
        this.yF                 = 0;
    };
    
    return Relation;
    
})();
