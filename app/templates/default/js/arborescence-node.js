var arborescence = arborescence || {};

arborescence.Node = (function()
{
    function Node()
    {
        this.initialize();
    }
    
    var prototype = Node.prototype;
    
    prototype.initialize = function()
    {

        this.key            = 0;
        this.level          = 0;

        // ===== N O D E S
        this.left           = null;
        this.right          = null;
        this.value          = null;

        // ===== D R A W I N G // S E T T I N G S
        this.drawing = 
        {
            position     : 
            {
                x   : 0,
                y   : 0
            },
            fontSize    : arborescence.FONTSIZE,
            type        : arborescence.NodeType.NORMAL,
            align       : arborescence.Align.CENTER,
            color       : arborescence.Colors.BLACK
        };

        // ===== O T H E R
        this.properties = 
        {
            isDeleted               : false,
            isVertivalRelationNode  : false,
            // Exercice options
            isChildrenVisible       : true,
            isValueVisible          : true,
            isVisible               : true
        };
    };

    prototype.equals    = function (node)
    {
        return (this.value.fonction == node.value.fonction)
            && (this.value.structureIntegrative == node.value.structureIntegrative);
    };
    
    return Node;
})();
