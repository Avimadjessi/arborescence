var arborescence = arborescence || {};

arborescence.init = function(json)
{
    arborescence.canvas = document.getElementById("arbre-canvas");

    // E N U M S
    arborescence.Colors         = 
    {
        BLACK   : '#000',
        SUCCESS : '#859900',
        DANGER  : '#DC322F'
    };
    arborescence.Align          = 
    {
        CENTER      : 'CENTER',
        LEFT        : 'LEFT',
        RIGHT       : 'RIGHT'
    };
    arborescence.Direction      = 
    {
        NORTH       : 'NORTH',
        EAST        : 'EAST',
        SOUTH       : 'SOUTH',
        WEST        : 'WEST'
    };
    arborescence.Tilt           = 
    {
        HORIZONTAL  : 'HORIZONTAL',
        VERTICAL    : 'VERTICAL',
        CURVE       : 'CURVE'
    };
    arborescence.NodeType       = 
    {
        NORMAL      : 'NORMAL',
        ATTENTE     : 'ATTENTE',
        INLINE      : 'INLINE'
    };
    arborescence.RelationType   =
    {
        LIAISON             : 0,
        DETERMINATION       : 1,
        PREDICATION         : 2,
        ENONCIATION         : 3,
        ATTENTE             : 4,
        ATTENTESECOND       : 5
    };
    arborescence.AnchorType     =
    {
        NODE        : 'NODE',
        RELATION    : 'RELATION'
    };
    arborescence.ObjectType     =
    {
        NODE        : 'NODE',
        RELATION    : 'RELATION',
        RANDOM      : 'RANDOM',
    };

    arborescence.isSaved        = false;
    arborescence.VERSION        = 0.1;
    arborescence.VERSION_TREE   = 0.1;     

    if (arborescence.canvas)
    {
        if(json)
        {
            arborescence.initFromJSON(json);
        } else
        {
            arborescence.DEFAULT_WIDTH  = 832;
            arborescence.DEFAULT_HEIGHT = 384;

            arborescence.WIDTH      = 832;
            arborescence.HEIGHT     = 384;

            arborescence.FONT       = 'Arial';
            arborescence.FONTSIZE   = 10.5;

            arborescence.NODEWIDTH  = 72;
            arborescence.NODEHEIGHT = 42;
            
            arborescence.NA_YGAP    = 14;
            arborescence.NODE_XGAP  = 160;
            arborescence.NODE_YGAP  = 100;

            arborescence.key        = -1;
            arborescence.level      = -1;

            arborescence.tree       = new arborescence.Tree();
            arborescence.drawing    = new arborescence.Drawing();

            arborescence.drawing.initDrawing(arborescence.tree);
        }
    }
};
arborescence.initFromJSON = function(json)
{
    var jsonNodes               = JSON.parse(json.nodes);
    var jsonRelations           = JSON.parse(json.relations);
    var jsonDrawnObjects        = JSON.parse(json.drawnObjects);
    var jsonNotes               = JSON.parse(json.notes);

    arborescence.DEFAULT_WIDTH  = jsonNodes.tree.defaultWidth;
    arborescence.DEFAULT_HEIGHT = jsonNodes.tree.defaultHeight;

    arborescence.WIDTH      = jsonNodes.tree.width;
    arborescence.HEIGHT     = jsonNodes.tree.height;

    arborescence.FONT       = jsonNodes.tree.font;
    arborescence.FONTSIZE   = jsonNodes.tree.fontSize
    
    arborescence.NA_YGAP    = 14;
    arborescence.NODE_XGAP  = jsonNodes.tree.xGap;
    arborescence.NODE_YGAP  = jsonNodes.tree.yGap;

    arborescence.NODEWIDTH  = jsonNodes.tree.nodeWidth;
    arborescence.NODEHEIGHT = jsonNodes.tree.nodeHeight;

    arborescence.VERSION_TREE   = jsonNodes.tree.version;
    arborescence.key        = jsonNodes.tree.key;
    arborescence.level      = jsonNodes.tree.level;
    
    var defaultValues = { nodes : jsonNodes, notes : jsonNotes, relations : jsonRelations, drawnObjects : jsonDrawnObjects, treeTags : json.treeTags, exercise : json.exercise };

    arborescence.tree       = new arborescence.Tree(defaultValues);
    arborescence.drawing    = new arborescence.Drawing();

    arborescence.drawing.initDrawing(arborescence.tree);
    arborescence.drawing.submitResizeCanvasWithDefault (jsonNodes.tree.width, jsonNodes.tree.height);
};
window.onload = function(json)
{};
