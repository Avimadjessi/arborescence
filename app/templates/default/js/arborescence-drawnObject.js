var arborescence = arborescence || {};

arborescence.DrawnObject = (function()
{
    function DrawnObject()
    {
        this.initialize();
    };
    
    var prototype = DrawnObject.prototype;
    
    prototype.initialize = function()
    {
        this.key                = 0;
        
        // ===== E N C H O R S
        this.start              = null;
        this.end                = null;

        // ===== D R A W I N G S // P R P
        this.drawing = 
        {
            position    : 
            {
                xS  : 0,
                yS  : 0,
                xM  : 0,
                yM  : 0,
                xF  : 0,
                yF  : 0
            },
            tilt        : arborescence.Tilt.HORIZONTAL,
            objectType  : arborescence.ObjectType.RELATION,
            align       : arborescence.Align.CENTER,
            type        : 0
        };

        // ===== O T H E R S
        this.properties = 
        {
            isDeleted       : false
        };
    };
    
    return DrawnObject;
    
})();
