$(function () {

	var windowHeight = $(window).height();
	var windowWidth = $(window).width();

	// Menu settings
	$("#sidemenu").height(windowHeight - 120);
	$(".bodyWrapper").addClass("active-menu");

	// Animate Text
	$("#home_title").textillate(
		{
			loop: true,
			minDisplayTime: 4000,

			// in animation settings.
		  	in: {
		    	effect: 'rollIn',
		    	delayScale: 1.5,
		    	delay: 50,
		        sync: false,
		    	shuffle: false,
		    	reverse: false,
		    	callback: function () {}
		  	},
			// out animation settings.
		  	out: {
		  		effect: 'rollOut',
		  		delayScale: 1.5,
		    	delay: 50,
		  		sync: false,
		    	shuffle: false,
		  		reverse: true,
		    	callback: function () {}
		  	},
		  	// callback that executes once textillate has finished 
  			callback: function () {},
		  	type: 'char'
		}
	);
});

var lastModalId = null;
// ===== // ========== D I S P L A Y . F I E L D S =============================
function scrollToAnchor(aid){
    var tag = $(aid +"").offset().top;
    $('html,body').animate({scrollTop: tag}, 500);
};
function showModal(modalId)
{
	lastModalId = modalId;
	$("#"+modalId).modal('show');
};

// ===== // ========== M E N U =================================================
function menuToggle ()
{
	if ($("#sidemenu").is(":visible"))
	{
		$("#sidemenu").hide("slide", { direction: "left" }, 1000);
	} else
	{
		$("#sidemenu").show("slide", { direction: "left" }, 1000);
	}

	$("#sidemenu").toggleClass("active");
	$(".bodyWrapper").toggleClass("active-menu");
}
function menuToggleOver ()
{
	// $("#menuToggle").removeClass();
	// $("#menuToggle").addClass("btn fa fa-circle-o");

	if ($("#sidemenu").is(":visible"))
	{
		$("#menuToggle").removeClass();
		$("#menuToggle").addClass("btn fa fa-chevron-left");
	} else
	{
		$("#menuToggle").removeClass();
		$("#menuToggle").addClass("btn fa fa-chevron-right");
	}
}
function menuToggleOut ()
{
	$("#menuToggle").removeClass();
	$("#menuToggle").addClass("btn fa fa-bars");
}
function disableAllMenuItem ()
{
	$("#miProfile").removeClass('active');
	$("#miLogin").removeClass('active');
	$("#miRegister").removeClass('active');
	$("#miRecover").removeClass('active');
	$("#miEditProfile").removeClass('active');
	$("#miEditProfileImage").removeClass('active');

	$("#miTrees").removeClass('active');
	$("#miTraining").removeClass('active');
	$("#miHome").removeClass('active');
	$("#miHelp").removeClass('active');
	$("#miCanvas").removeClass('active');
	$("#miDeleteProfile").removeClass('active');
	$("#miAbout").removeClass('active');
	$("#miFaq").removeClass('active');

	// A D M I N
	$("#miAdmin").removeClass('active');
	$("#miAdminAbout").removeClass('active');
	$("#miAdminNotice").removeClass('active');
	$("#miAdminSearch").removeClass('active');
	$("#miAdminExercise").removeClass('active');

	// M E N U . I T E M S
	$('#accountItems').slideUp();
	$('#adminItems').slideUp();
	$('#treesItems').slideUp();
	$('#baseAccountItems').slideUp();
};
function activateMenuItem (menuItem, menu)
{
	switch(menuItem)
	{
		// A D M I N
		case 'admin':
			disableAllMenuItem();
			$("#miAdminAbout").addClass('active');
			break;
		case 'adminAbout':
			disableAllMenuItem();
			$("#miAdminAbout").addClass('active');
			break;
		case 'adminNotice':
			disableAllMenuItem();
			$("#miAdminNotice").addClass('active');
			break;
		case 'adminSearch':
			disableAllMenuItem();
			$("#miAdminSearch").addClass('active');
			break;
		case 'adminExercise':
			disableAllMenuItem();
			$("#miAdminExercise").addClass('active');
			break;
			
		// A C C O U N T
		case 'profile':
			disableAllMenuItem();
			$("#miProfile").addClass('active');
			break;
		case 'editProfile':
			disableAllMenuItem();
			$("#miEditProfile").addClass('active');
			break;
		case 'editProfileImage':
			disableAllMenuItem();
			$("#miEditProfileImage").addClass('active');
			break;
		case 'deleteProfile':
			disableAllMenuItem();
			$("#miDeleteProfile").addClass('active');
			break;

		// T R E E 
		case 'trees':
			disableAllMenuItem();
			$("#miTrees").addClass('active');
			break;
		case 'canvas':
			disableAllMenuItem();
			$("#miCanvas").addClass('active');
			break;

		// C O N N E X I O N
		case 'login':
			disableAllMenuItem();
			$("#miLogin").addClass('active');
			break;
		case 'register':
			disableAllMenuItem();
			$("#miRegister").addClass('active');
			break;
		case 'recover':
			disableAllMenuItem();
			$("#miRecover").addClass('active');
			break;

		// O T H E R
		case 'about':
			disableAllMenuItem();
			$("#miAbout").addClass('active');
			break;
		case 'faq':
			disableAllMenuItem();
			$("#miFaq").addClass('active');
			break;
		case 'help':
			disableAllMenuItem();
			$("#miHelp").addClass('active');
			break;

		default:
			disableAllMenuItem();
			$("#miHome").addClass('active');
	}

	if (menu)
		collapseOrFoldMenu (menu);

};
function collapseOrFoldMenu (menu)
{
	$("#"+menu).slideToggle(200);
};

// ===== // ========== S E A R C H =============================================
function adminSearchUser ()
{
	// Check Fields
    var name        = $("input[name='userUsername']").val();
    var email       = $("input[name='userUserEmail']").val();
    var tag         = $("input[name='userUserTag']").val();

    $.ajax({
        type        : 'POST',
        url         : "/arborescence/admin/search",
        contentType : "application/x-www-form-urlencoded",
        data        : 
        {
            submit          	: 'searchUser',
            username            : name,
            useremail           : email,
            usertag            : tag         
        },
        success: function(response)
        {
            $("#usersFromSearchBody").empty();
            $("#usersFromSearchBody").html(response);
            $('#usersFromSearch').modal('show');
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            alert('Failed//'+errorThrown+'//'+textStatus);
        }
    });
};
function adminSearchTree ()
{
	// Check Fields
    var name        = $("input[name='treeTreeName']").val();
    var email       = $("input[name='treeUserEmail']").val();
    var tag         = $("input[name='treeTreeTag']").val();

    $.ajax({
        type        : 'POST',
        url         : "/arborescence/admin/search",
        contentType : "application/x-www-form-urlencoded",
        data        : 
        {
            submit          	: 'searchTree',
            treename            : name,
            useremail           : email,
            usertag             : tag        
        },
        success: function(response)
        {
            $("#treesByTagBody").empty();
            $("#treesByTagBody").html(response);
            $('#treesByTag').modal('show');
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            alert('Failed//'+errorThrown+'//'+textStatus);
        }
    });
};

// ===== // ========== S H A R E . T R E E =====================================
function shareTree (id)
{
	var email 	= $('#share-email-'+id).val();
	var slug 	= $('#share-slug-'+id).val();
	var message = $('#share-message-'+id).val();

	var url 	= "trees/" + slug + "/share";
	$.ajax({
        type        : 'POST',
        url         : url,
        contentType : "application/x-www-form-urlencoded",
        data: 
        {
        	slug			: slug,
            email           : email,
            message 		: message
        },
        success: function(response)
        { 

        	if (response === 'success')
        	{
    			$("#"+lastModalId).modal('hide');

        	} else if (response === 'error-user')
        	{
    			$("#share-message-danger-user").removeClass("disabled");
    			$("#share-message-name").addClass("disabled");
    			$("#share-message-sucess").addClass("disabled");
    			$("#share-message-danger-tree").addClass("disabled");
        	} else if (response === 'error-tree')
        	{
    			$("#share-message-danger-tree").removeClass("disabled");
    			$("#share-message-name").addClass("disabled");
    			$("#share-message-sucess").addClass("disabled");
    			$("#share-message-danger-user").addClass("disabled");
        	} else if (response == 'unknow')
        	{
        		$("#"+lastModalId).modal('hide');
        	}
    	},
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("/ERROR/ " + errorThrown);
        }
    });
};

// ===== // ========== S H A R E . E X E R C I S E =============================
function shareExercise (exerciseId)
{
	var userList 	= [];
	var slug 	= $('#share-exercise-slug-'+exerciseId).val();

	$('input[name="user-select"]').each(function()
	{
		if ($(this).is(':checked'))
		{
		 	userList.push($(this).val());
		}
	});

	var url 	= "admin/exercise/share";
	$.ajax({
        type        : 'POST',
        url         : url,
        contentType : "application/x-www-form-urlencoded",
        data: 
        {
        	exercise	: exerciseId,
            users       : userList
        },
        success: function(response)
        { 
        	console.log(response);
        	$("#share-exercise-message").html(response);
    	},
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("/ERROR/ " + errorThrown);
        }
    });
};

function displayAllNotices ()
{
	$('#noticeModal').modal('show');
}

// ===== // ====================================================================
// Disable sorting from table columns with 'no_sorting' class 
function disableTableSorting ()
{
	$(".no_sorting").each(function() 
    {
    	$(this).removeClass("sorting_asc");
    	$(this).removeClass("sorting_desc");
    	$(this).removeClass("sorting");
    });
}

// ===== // ========== O T H E R S =============================================

