var arborescence = arborescence || {};

/**
 * Tree Class
 * @type _L6.Tree|Function
 */
arborescence.Tree = (function()
{
    // C O N S T R U C T O R
    /**
     * Tree constructor
     * @param {Array} defaultValues nodes and relations json
     */
    function Tree(defaultValues)
    {
        // ===== // ===//L I S T E S//=== // ===================================
        this.Fonctions = 
        {
            DEFAULT                 : '',
            PHRASE                  : 'Phrase',
            SOUSPHRASE              : 'Sous-Phrase',
            NOYAU                   : 'Noyau',
            PREDICAT                : 'Prédicat',
            PREDICATSECOND          : 'P2',
            DETERMINANT             : 'Dét.',
            DETERMINANTQ            : 'Dét. Q.',
            DETERMINANTCA           : 'Dét. Ca',
            DETERMINANTENONCIATION  : 'Det. Enonc.',
            LIG                     : 'Lig.',
            REL                     : 'rel'
        };
        this.StructuresIntegratives = 
        {
            DEFAULT                 : '******',
            NULL                    : 'ϴ',
            NULLSECOND              : 'Ø',
            GPPREMIER               : 'GP1',
            GPPREMIERPRIME          : "GP1'",
            GPSECOND                : 'GP2',
            DELTA                   : 'Δ',
            GDNOM                   : 'GDN',
            GDNOMPRIME              : "GDN'",
            GDNOMSECOND             : "GDN''",
            GDNOMTIERS              : "GDN'''",
            GDNOMQUATRIEME          : "GDN''''",
            GDPRONOM                : 'GDPron.',
            GDADJECTIF              : 'GDAdj.',
            GDVERBE                 : 'GDV',
            GDADVERBE               : 'GDAdv.',
            GDCONNECTEUR            : 'GDC',
            NOM                     : 'nom',
            VERBE                   : 'verbe',
            ADJECTIF                : 'Adj.',
            ADVERBE                 : 'Adv.',
            PRONOM                  : 'pronom',
            CSUB                    : 'C.sub.',
            CCOORD                  : 'C.Coord.',
            INTERJECTION            : 'interjection'
        };

        if (defaultValues)
        {
            this.initializeWithDefault(defaultValues);
        } else
        {
            this.initialize();
        }
    };

    var prototype = Tree.prototype;

    /**
     * Initialize the tree default values
     */
    prototype.initialize = function()
    {
        this.name       = "";
        this.title      = 
        {
            value   : "",
            bold    : "",
            italic  : "",
            size    : "14"
        };
        this.description = 'Aucune description';

        // ========== // =======================================================
        this.errors             = [];
        this.subTrees           = [];
        this.notes              = [];
        this.nodesToDraw        = [];
        this.relationsToDraw    = [];
        this.drawnObjectsToDraw = [];
        this.treeTags           = [];

        this.initTree();
        // Creates the list of objects to draw
        this.createItemList();

        this.setSaved (false);
    };
    prototype.initializeWithDefault = function(defaultValues)
    {
        this.name   = "";
        this.title  = 
        {
            value   : defaultValues.nodes.tree.title.value,
            bold    : defaultValues.nodes.tree.title.bold,
            italic  : defaultValues.nodes.tree.title.italic,
            size    : defaultValues.nodes.tree.title.size
        };
        this.description    = defaultValues.nodes.tree.description || '<i class="fa fa-times"></i> Aucun énoncé disponible pour cet exercice';
        this.exercise       = defaultValues.exercise;

        // ========== // =======================================================
        this.errors             = [];
        this.nodesToDraw        = [];
        this.relationsToDraw    = [];
        this.subTrees           = [];
        this.notes              = [];
        this.drawnObjectsToDraw = [];
        this.treeTags           = defaultValues.treeTags;

        this.initTreeWithDefault(defaultValues);

        // Creates the list of objects to draw
        this.createItemList();

        this.setSaved (true);
    };

    // ====================================================================== //

    /**
     * Initialize starting nodes: the tree's root node, the root's left and 
     * right nodes
     */
    prototype.initTree = function()
    {
        var root = new arborescence.Node();

        // ===== Position ======================================================
        var gapY = arborescence.NODE_YGAP;
        var gapX = arborescence.NODE_XGAP;

        // === R A C I N E // ==================================================
        arborescence.key        += 1;
        arborescence.level      += 1;
        root.key                = arborescence.key;
        root.level              = arborescence.level;
        root.value              = 
        {
            fonction: this.Fonctions.DEFAULT,
            structureIntegrative: this.StructuresIntegratives.DEFAULT,
            terme: null
        };
        root.drawing.position.x         = arborescence.WIDTH/2;
        root.drawing.position.y         = 36;
        root.drawing.align              = arborescence.Align.CENTER;

        // === R A C I N E . G A U C H E // ====================================
        root.left               = new arborescence.Node();
        arborescence.key        += 1;
        arborescence.level      += 1;
        root.left.key           = arborescence.key;
        root.left.level         = arborescence.level;
        root.left.value         = 
        {
            fonction: this.Fonctions.NOYAU,
            structureIntegrative: this.StructuresIntegratives.DEFAULT,
            terme: null
        };
        root.left.drawing.position.x    = root.drawing.position.x - gapX;
        root.left.drawing.position.y    = root.drawing.position.y + gapY;
        root.left.drawing.align          = arborescence.Align.LEFT;

        // === R A C I N E . D R O I T E // ====================================
        root.right              = new arborescence.Node();
        arborescence.key        += 1;
        root.right.key          = arborescence.key;
        root.right.level        = arborescence.level;
        root.right.isRightNode  = true;
        root.right.value        = 
        {
            fonction: this.Fonctions.DEFAULT,
            structureIntegrative: this.StructuresIntegratives.DEFAULT,
            terme: null
        };
        root.right.drawing.position.x   = root.drawing.position.x + gapX;
        root.right.drawing.position.y   = root.drawing.position.y + gapY;
        root.right.drawing.align        = arborescence.Align.RIGHT;

        this.subTrees.push(root);

        // === G A U C H E && D R O I T E // R E L A T I O N ===================
        var relation = this.createNodeRelation(root.right, root.left);

        this.relationsToDraw.push(relation);
    };
    prototype.initTreeWithDefault = function(defaultValues)
    {
        var lengthSubtrees = Object.keys(defaultValues.nodes.tree.subtrees).length; 
        for(var i = 0; i < lengthSubtrees; i++)
        {       
            var label = "subtree"+i;
            var node = new arborescence.Node();
            this.jsonToNode(node, defaultValues.nodes.tree.subtrees[label]);
            this.subTrees.push(node);
        }
        
        var lengthRelations = Object.keys(defaultValues.relations.relations).length;
        for(var j = 0; j < lengthRelations; j++)
        {       
            var label = "relation"+j;
            var relation = new arborescence.Relation();
            this.jsonToRelation(relation, defaultValues.relations.relations[label]);
            if (this.exercise && !relation.properties.isVisible)
                ; 
            else
                this.relationsToDraw.push(relation);
        }

        var lengthDrawnObjects = Object.keys(defaultValues.drawnObjects.drawnObjects).length;
        for(var k = 0; k < lengthDrawnObjects; k++)
        {
            var label = "drawnObject"+k;
            var drawnObject = new arborescence.DrawnObject();
            this.jsonToDrawn(drawnObject, defaultValues.drawnObjects.drawnObjects[label]);
            this.drawnObjectsToDraw.push(drawnObject);
        }

        var lengthNotes = Object.keys(defaultValues.notes.notes).length;
        for(var l = 0; l < lengthNotes; l++)
        {
            var label = "note"+l;
            var note = new arborescence.Note();
            this.jsonToNote(note, defaultValues.notes.notes[label]);
            this.notes.push(note);
        }
    };

    // ====================================================================== //
    // ========== // L O A D . T R E E // =================================== //
    // ====================================================================== //
    prototype.jsonToNode = function (node, jsonNode)
    {
        if(node)
        {   
            node.key                    = jsonNode.key;
            node.level                  = jsonNode.level;
            node.drawing.position.x     = jsonNode.drawing.position.x;
            node.drawing.position.y     = jsonNode.drawing.position.y;
            node.drawing.fontSize       = jsonNode.drawing.fontSize;
            node.drawing.size           = jsonNode.drawing.fontSize;
            node.drawing.align          = jsonNode.drawing.fontSize;
            node.drawing.color          = jsonNode.drawing.fontSize;

            node.properties.isDeleted               = jsonNode.properties.isDeleted;
            node.properties.isVerticalRelationNode  = jsonNode.properties.isVerticalRelationNode;
            
            node.properties.isChildrenVisible       = jsonNode.properties.isChildrenVisible;
            node.properties.isValueVisible          = jsonNode.properties.isValueVisible;
            node.properties.isVisible               = jsonNode.properties.isVisible;

            node.value      = {
                fonction            : jsonNode.value.fonction,
                structureIntegrative: jsonNode.value.structureIntegrative,
                terme               : jsonNode.value.terme
            };
            if((Object.keys(jsonNode.left).length > 0) 
                    && (Object.keys(jsonNode.right).length > 0))
            {
                node.left   = new arborescence.Node();
                node.right  = new arborescence.Node();
                this.jsonToNode(node.left, jsonNode.left);
                this.jsonToNode(node.right, jsonNode.right);
            }
        }
    };
    prototype.jsonToRelation = function (relation, jsonNode)
    {
        if(relation)
        {
            relation.key                = jsonNode.key;

            relation.drawing.position.xS= jsonNode.drawing.position.xS;
            relation.drawing.position.yS= jsonNode.drawing.position.yS;
            relation.drawing.position.xF= jsonNode.drawing.position.xF;
            relation.drawing.position.yF= jsonNode.drawing.position.yF;

            relation.drawing.tilt       = jsonNode.drawing.tilt;
            relation.drawing.color      = jsonNode.drawing.color;
            relation.drawing.type       = jsonNode.drawing.type;
            relation.drawing.direction  = jsonNode.drawing.direction;

            relation.properties.isSubTreeLink       = jsonNode.properties.isSubTreeLink;
            relation.properties.isAttenteLink       = jsonNode.properties.isAttenteLink;
            relation.properties.isLeftAttenteLink   = jsonNode.properties.isLeftAttenteLink;
            relation.properties.isRightAttenteLink  = jsonNode.properties.isRightAttenteLink;
            relation.properties.isDeleted           = jsonNode.properties.isDeleted;
            relation.properties.isVisible           = jsonNode.properties.isVisible;
            
            if (Object.keys(jsonNode.start).length > 0)
            {
                relation.start = 
                { 
                    type    : arborescence.AnchorType.NODE,
                    value   : new arborescence.Node()
                };

                this.jsonToNode(relation.start.value, jsonNode.start.value);
            }

            if (Object.keys(jsonNode.end).length > 0)
            {
                if (jsonNode.end.type === arborescence.AnchorType.NODE)
                {
                    relation.end = 
                    { 
                        type    : arborescence.AnchorType.NODE,
                        value   : new arborescence.Node()
                    };

                    this.jsonToNode(relation.end.value, jsonNode.end.value);
                } else if (jsonNode.end.type === arborescence.AnchorType.RELATION)
                {
                    relation.end = 
                    {
                        type    : arborescence.AnchorType.RELATION,
                        value   : new arborescence.Relation()  
                    };

                    this.jsonToRelation(relation.end.value, jsonNode.end.value);
                }
            }
        }
    };
    prototype.jsonToDrawn = function (drawnObject, jsonNode)
    {
        if(drawnObject)
        {
            drawnObject.key             = jsonNode.key,

            drawnObject.drawing.position.xS     = jsonNode.drawing.position.xS;
            drawnObject.drawing.position.yS     = jsonNode.drawing.position.yS;
            drawnObject.drawing.position.xM     = jsonNode.drawing.position.xM;
            drawnObject.drawing.position.yM     = jsonNode.drawing.position.yM;
            drawnObject.drawing.position.xF     = jsonNode.drawing.position.xF;
            drawnObject.drawing.position.yF     = jsonNode.drawing.position.yF;

            drawnObject.drawing.tilt            = jsonNode.drawing.tilt;
            drawnObject.drawing.objectType      = jsonNode.drawing.objectType;
            drawnObject.drawing.align           = jsonNode.drawing.align;
            drawnObject.drawing.type            = jsonNode.drawing.type;

            drawnObject.properties.isDeleted    = jsonNode.properties.isDeleted;

            // Start // End //

        }
    };
    prototype.jsonToNote = function (note, jsonNode)
    {
        if(note)
        {
            note.key                        = jsonNode.key,
            note.value                      = jsonNode.value,

            note.drawing.position.x         = jsonNode.drawing.position.x;
            note.drawing.position.y         = jsonNode.drawing.position.y;

            note.drawing.width              = jsonNode.drawing.width;
            note.drawing.fontSize           = jsonNode.drawing.fontSize;
            note.drawing.background         = jsonNode.drawing.background;

            note.properties.isDeleted       = jsonNode.properties.isDeleted;
        }
    };
    // ====================================================================== //

    // ========== // Creates the list of items to draw // =================== //
    prototype.createItemList = function ()
    {
        this.nodesToDraw = [];
        for (var i = 0; i < this.subTrees.length; i++)
        {
            this.recursivePath(this.subTrees[i]);
        }
    };
    prototype.recursivePath = function (node)
    {
        if (node && !node.isDeleted)
        {
            if(this.exercise )
            {
                if(node.properties.isVisible)
                {
                    if (!node.properties.isValueVisible)
                    {
                        nodevalue              = 
                        {
                            fonction: this.Fonctions.DEFAULT,
                            structureIntegrative: this.StructuresIntegratives.DEFAULT,
                            terme: null
                        }
                        node.value = nodevalue;
                        node.properties.isValueVisible = true;
                    }
                    if (!node.properties.isChildrenVisible)
                    {
                        node.left   = null;
                        node.right  = null;
                        node.properties.isChildrenVisible = true;
                    }

                    this.nodesToDraw.push(node);
                    this.recursivePath(node.left);
                    this.recursivePath(node.right);
                }
            } else
            {
                this.nodesToDraw.push(node);
                this.recursivePath(node.left);
                this.recursivePath(node.right);
            }
        }
    };
    // ====================================================================== //

    // ====================================================================== //
    // ========== // A . C . T . I . O . N //                                 //
    // ====================================================================== //
    // ========== U P D A T E // T I T L E ================================== //
    prototype.updateTitle = function (title)
    {
        this.title.value    = title.value   || '';
        this.title.bold     = title.bold    || '';
        this.title.italic   = title.italic  || '';

        this.setSaved(false);
    };
    // ========== U P D A T E // D E S C ==================================== //
    prototype.updateDescription = function (desc)
    {
        this.description    = desc   || '';

        this.setSaved(false);
    };
    // ====================================================================== //
    // ========== U P D A T E // N O D E ==================================== //
    prototype.updateNode = function (key, value)
    {
        for (var i = 0; i < this.subTrees.length; i++)
        {
            this.recursiveUpdateNode(this.subTrees[i], key, value);
        }

        this.setSaved(false);
    };
    prototype.updateAdvNode = function (key, value)
    {
        for (var i = 0; i < this.subTrees.length; i++)
        {
            this.recursiveUpdateAdvNode(this.subTrees[i], key, value); 
        }

        this.setSaved(false);
    };
    prototype.recursiveUpdateNode = function (node, key, value)
    {
        if (node)
        {
            if (node.key === Number(key))
            {
                this.updateNodeValues(node, value);
                return true;
            }

            this.recursiveUpdateNode(node.left, key, value);
            this.recursiveUpdateNode(node.right, key, value);
        }
    };
    prototype.recursiveUpdateAdvNode = function (node, key, value)
    {
        if (node)
        {
            if (node.key === Number(key))
            {
                node.drawing.position.x     = Number(value.x);
                node.drawing.position.y     = Number(value.y);

                node.drawing.fontSize       = Number(value.fontSize);
                if (value.isValueVisible != null)
                {
                    node.properties.isChildrenVisible   = value.isChildrenVisible;
                    node.properties.isValueVisible = value.isValueVisible;

                    this.setVisibility(node.left, node.properties.isChildrenVisible);
                    this.setVisibility(node.right, node.properties.isChildrenVisible);
                }

                return node;
            }

            this.recursiveUpdateAdvNode(node.left, key, value);
            this.recursiveUpdateAdvNode(node.right, key, value);
        }

        return null;
    };
    prototype.updateNodeValues = function (node, value)
    {
        node.value.fonction             = value.fonction;
        node.value.structureIntegrative = value.structureIntegrative;
        node.value.terme                = 
            node.value.terme === '' ? null : value.terme;

        // ===== C H E C K // 1ERE LETTRE
        // "G" => développement de l'arbre
        if ((node.value.structureIntegrative[0] === 'G'))
        {
            // Ne pas effacer un sous arbre existant
            if ((!node.left) && (!node.right))
            {
                arborescence.level      = 
                    (node.level + 1) > 4 ? 4 : (node.level + 1);

                arborescence.key        += 1;
                node.left               = new arborescence.Node();
                node.left.key           = arborescence.key;
                node.left.level         = arborescence.level;
                node.left.drawing.align = arborescence.Align.LEFT;
                node.left.value         = 
                {
                    fonction: this.Fonctions.NOYAU,
                    structureIntegrative: this.StructuresIntegratives.DEFAULT,
                    terme: null
                };

                arborescence.key        += 1;
                node.right              = new arborescence.Node();
                node.right.key          = arborescence.key;
                node.right.level        = arborescence.level;
                node.right.drawing.align= arborescence.Align.RIGHT;


                node.right.value = {
                    fonction: this.Fonctions.DEFAULT,
                    structureIntegrative: this.StructuresIntegratives.DEFAULT,
                    terme: null
                };

                // ===== P O S I T I O N =======================================
                var gapY = arborescence.NODE_YGAP;
                var gapX = arborescence.NODE_XGAP - (arborescence.level * 28);
                node.left.drawing.position.x    = node.drawing.position.x - gapX;
                node.left.drawing.position.y    = node.drawing.position.y + gapY;

                node.right.drawing.position.x   = node.drawing.position.x + gapX;
                node.right.drawing.position.y   = node.drawing.position.y + gapY;

                // === G A U C H E && D R O I T E // R E L A T I O N ===========
                var relation                    = this.createNodeRelation(node.right, node.left);
                this.relationsToDraw.push(relation);

            }
            this.checkImplications(node);
            this.checkRelations(node);
        }
    };

    // ========== U P D A T E // D R A W N . O B J E C T // P O S I T I O N ====
    prototype.updateDrawnObjectPosition = function (values)
    {
        for(var i = 0; i < this.drawnObjectsToDraw.length; i++)
        {
            var drawnObject = this.drawnObjectsToDraw[i];
            if(drawnObject && Number(drawnObject.key) === Number(values.key))
            {
                drawnObject.drawing.position.xM = values.drawing.position.xM;
                drawnObject.drawing.position.yM = arborescence.HEIGHT - values.drawing.position.yM;
            }
        }

        this.setSaved(false);
    };

    // ========== U P D A T E // N O D E // F O N T ============================
    prototype.updateNodesFont = function ()
    {
        for (var i = 0; i < this.subTrees.length; i++)
        {
            this.recursiveUpdateNodesFont(this.subTrees[i]);
        }

        this.setSaved(false);
    };
    prototype.recursiveUpdateNodesFont = function (node)
    {
        if (node)
        {
            node.drawing.fontSize = arborescence.FONTSIZE;

            this.recursiveUpdateNodesFont(node.left);
            this.recursiveUpdateNodesFont(node.right);
        }
    };
    // ========== U P D A T E // N O T E // F O N T ============================
    prototype.updateNotesFont = function ()
    {
        for (var i = 0; i < this.notes.length; i++)
        {
            if (this.notes[i])
            {
                this.notes[i].drawing.fontSize = arborescence.FONTSIZE;
            }
        }

        this.setSaved(false);
    };

    // ========== U P D A T E // R E L A T I O N // V I S I B I L I T Y  =======
    prototype.updateRelationsVisibility         = function ()
    {
        for (var i = 0; i < this.subTrees.length; i++)
        {
            this.setRelationLinkedToNodeVisibility(this.subTrees[i]);
        }

        this.setSaved(false);  
    };
    prototype.setRelationLinkedToNodeVisibility = function (node)
    {
        if (node)
        {
            var nodeKey         = node.key;
            var visibility      = node.properties.isVisible;

            for (var j = 0; j < this.relationsToDraw.length; j++)
            {
                if ((this.relationsToDraw[j].start.value.key === Number(nodeKey))
                    || (this.relationsToDraw[j].end.value.key === Number(nodeKey)))
                {
                    var color = visibility ? arborescence.Colors.BLACK : arborescence.Colors.DANGER;
                    this.relationsToDraw[j].properties.isVisible    = visibility;
                    this.relationsToDraw[j].drawing.color           = color;
                    break; 
                }
            }

            this.setRelationLinkedToNodeVisibility(node.left);
            this.setRelationLinkedToNodeVisibility(node.right);
        }   
    };

    // ========== U P D A T E // N O D E // V I S I B I L I T Y  ===============
    prototype.setChildrenVisibility = function (parent, visibility)
    {
        for (var i = 0; i < this.subTrees.length; i++)
        {
            this.recursiveSetChildrenVisibility(this.subTrees[i], parent, visibility);
        }
    };
    // ========== U P D A T E // N O D E // P O S I T I O N ====================
    prototype.updateNodePosition = function (key, x, y)
    {
        for (var i = 0; i < this.subTrees.length; i++)
        {
            this.recursiveUpdateNodePosition(this.subTrees[i], key, x, y);
        }
        this.setSaved(false);
    };
    prototype.recursiveSetChildrenVisibility = function (node, parent, visibility)
    {
        if (node)
        {
            if (node.key === Number(parent))
            {
                this.setVisibility (node.left, visibility);
                this.setVisibility (node.right, visibility);
            }
        }
    };
    prototype.setVisibility = function (node, visibility)
    {
        if( node)
        {
            node.properties.isVisible = visibility;
            
            this.setVisibility (node.left, visibility);
            this.setVisibility (node.right, visibility);
        }
    };
    prototype.recursiveUpdateNodePosition = function (node, key, x, y)
    {
        if (node)
        {
            if (node.key === Number(key))
            {
                var oldX                    = node.drawing.position.x;
                var oldY                    = node.drawing.position.y;

                var deltaX                  = oldX - x;
                var deltaY                  = oldY - y;

                node.drawing.position.x     = x;
                node.drawing.position.y     = y;

                if (node.left && node.right)
                {
                    this.updateNodePosition(node.left.key,
                            node.left.drawing.position.x - deltaX, 
                            node.left.drawing.position.y - deltaY);
                    this.updateNodePosition(node.right.key,
                            node.right.drawing.position.x - deltaX, 
                            node.right.drawing.position.y - deltaY);
                }

                this.updateRelationPosition(key, x, y);

                return true;
            }

            this.recursiveUpdateNodePosition(node.left, key, x, y);
            this.recursiveUpdateNodePosition(node.right, key, x, y);
        }
    };
    prototype.updateRelationPosition = function (key, x, y)
    {
        for (var i = 0; i < this.relationsToDraw.length; i++)
        {
            var relation = this.relationsToDraw[i];

            if(relation)
            {
                if (relation.start.value.key === key)
                {
                    relation.drawing.position.xS = x;
                    relation.drawing.position.yS = y;
                    // Is something linked to the relation ? Update it
                    this.updateLinkedRelations(relation.key, relation.drawing.position);
                }
                if (relation.end.value.key === key)
                {
                    relation.drawing.position.xF = x;
                    relation.drawing.position.yF = y;
                    // Is something linked to the relation ? Update it
                    this.updateLinkedRelations(relation.key, relation.drawing.position);
                }
            }
        }
    };
    prototype.updateLinkedRelations = function (key, position)
    {
        for (var j = 0; j < this.relationsToDraw.length; j++)
        {
            var relation            = this.relationsToDraw[j];
            var relationWidth       = Math.abs(position.xS - position.xF);
            if (relation.end && relation.end.value.key === Number(key))
            {
                relation.drawing.position.xF = (position.xS > position.xF) ? position.xS - (relationWidth/2) : position.xF - (relationWidth/2);
                relation.drawing.position.yF = position.yS;
                if (relation.drawing.tilt === arborescence.Tilt.HORIZONTAL)
                {
                    var endRelation = this.getRelation(key);
                    if (!endRelation)
                        return false;
                    var relationHeight      = Math.abs(endRelation.drawing.position.yS - endRelation.drawing.position.yF);
                    relation.drawing.position.xF -= arborescence.NODEWIDTH/2
                    relation.drawing.position.yF = endRelation.drawing.position.yS - (relationHeight/2);
                }
                    
            }
        }
    };

    // ========== U P D A T E // N O T E // P O S I T I O N ====================
    prototype.updateNotePosition = function (key, x, y)
    {
        for (var i = 0; i < this.notes.length; i++)
        {
            if (this.notes[i] && this.notes[i].key === key)
            {
                this.notes[i].drawing.position.x    = x;
                this.notes[i].drawing.position.y    = y;
            }
        }
        this.setSaved(false);
    };

    // ========== U P D A T E // R E L A T I O N ===============================
    prototype.updateRelation = function (key, newValues)
    {
        for (var i = 0; i < this.relationsToDraw.length; i++)
        {
            var relation = this.relationsToDraw[i];
            if (relation.key === Number(key))
            {
                relation.drawing.type               = Number(newValues.type);
                if(newValues.isVisible)
                {
                    relation.properties.isVisible       = newValues.isVisible;
                    relation.properties.isValueVisible  = newValues.isValueVisible;
                }
            }
        }
        this.setSaved(false);
    };
    prototype.updateRelationByNode = function (keyS, keyF, relationType)
    {
        for (var i = 0; i < this.relationsToDraw.length; i++)
        {
            var relation = this.relationsToDraw[i];
            if ((relation.start.value.key === Number(keyS)) &&
                    (relation.end.value.key === Number(keyF) ))
            {
                relation.drawing.type = Number(relationType);
            }
        }
        this.setSaved(false);
    };  

    // ========== E X I S T S // N O D E =======================================
    prototype.nodeExists = function (key)
    {
        var exists = false;
        for (var i = 0; i < this.subTrees.length; i++)
        {
            this.recursiveSearchNode(this.subTrees[i], key, exists);
        }
        return exists;
    };
    prototype.recursiveSearchNode = function (node, key, found)
    {
        if (node && !found)
        {
            if (node.key === Number(key))
            {
                found = true;
            }
            this.recursiveSearchNode(node.left, key, found);
            this.recursiveSearchNode(node.right, key, found);
        }
    };
    prototype.nodeIsUsed = function (key)
    {
        for (var i = 0; i < this.nodesToDraw.length; i++)
        {
            if (this.nodesToDraw[i].key === Number(key))
            {
                return true;
            }
        }
        return false;
    };

    // ========== D E L E T E // N O D E =======================================
    prototype.deleteNode = function (nodeKey)
    {
        for (var i = 0; i < this.subTrees.length; i++)
        {
            this.recursiveDelete(this.subTrees[i], nodeKey);
        }
        this.setSaved(false);
    };
    prototype.recursiveDelete = function (node, key)
    {
        if (node)
        {
            if (node.key === Number(key))
            {
                this.deleteNodeChildren(node);
                return true;
            }

            this.recursiveDelete(node.left, key);
            this.recursiveDelete(node.right, key);
        }
    };
    prototype.deleteNodeChildren = function (node)
    {
        if(node)
        {
            if (!node.left && !node.right)
            {
                node.properties.isDeleted = true;
            } else
            {
                arborescence.drawing.deleteLeafErrorFlag = false;
                node.left = null;
                node.right = null;
            }

        }
    };

    // ====================================================================== //
    // ========== // N O T E // ============================================= //
    // ====================================================================== //
    // ======== D E L E T E // N O T E =========================================
    prototype.deleteNote = function (noteKey)
    {
        for(var i = 0; i < this.notes.length; i++)
        {
            if(Number(this.notes[i].key) === Number(noteKey))
            {
                this.notes.splice( i, 1);
                break;
            }
        }
        this.setSaved(false);
    };
    // ========== U P D A T E // N O T E =======================================
    prototype.updateOrCreateNote = function (values)
    {
        var updated         = false;
        // U P D A T E
        for (var i = 0; i < this.notes.length; i++)
        {
            if (this.notes[i])
            {
                if (Number(this.notes[i].key) === Number(values.key))
                {
                    this.notes[i].value                  = values.value;
                    this.notes[i].drawing.position.x     = values.x;
                    this.notes[i].drawing.position.y     = values.y;
                    this.notes[i].drawing.width          = values.width;
                    this.notes[i].drawing.fontSize       = values.fontSize;

                    updated     = true;
                    this.setSaved(false);
                    return;
                }
            }
        }
        if(!updated)
        {
            // C R E A T E
            var note                        = new arborescence.Note();
            note.key                        = values.key;
            note.value                      = values.value;
            note.drawing.position.x         = values.x;
            note.drawing.position.y         = values.y;
            note.drawing.width              = values.width;
            note.drawing.fontSize           = values.fontSize;

            this.notes.push(note);
        }
        this.setSaved(false);
    };
    
    // ====================================================================== //
    // ========== // D R A W N . O B J E C T // ============================= //
    // ====================================================================== //
    // ======== D R A W N // O B J E C T S =====================================
    prototype.addDrawnObject = function (drawnObject)
    {
        var object = new arborescence.DrawnObject();
        arborescence.key            += 1;
        object.key                  = arborescence.key;
        object.drawing.position     = drawnObject.position;
        object.drawing.tilt         = drawnObject.tilt;
        object.drawing.objectType   = drawnObject.objectType;
        object.drawing.align        = drawnObject.align;
        object.drawing.type         = drawnObject.type;
        
        this.drawnObjectsToDraw.push(object);
        this.setSaved(false);
    };
    prototype.deleteDrawnObject = function (drawnObjectKey)
    {
        for(var i = 0; i < this.drawnObjectsToDraw.length; i++)
        {
            if(this.drawnObjectsToDraw[i].key === Number(drawnObjectKey))
            {
                this.drawnObjectsToDraw.splice( i, 1);
                break;
            }
        }
        this.setSaved(false);
    };
    prototype.editDrawnObject = function (newValues)
    {
        for(var i = 0; i < this.drawnObjectsToDraw.length; i++)
        {
            if(this.drawnObjectsToDraw[i].key === Number(newValues.key))
            {
                this.drawnObjectsToDraw[i].drawing.type = newValues.drawing.type;
                this.drawnObjectsToDraw[i].drawing.position.xS = newValues.drawing.position.xS;
                this.drawnObjectsToDraw[i].drawing.position.yS = newValues.drawing.position.yS;
                this.drawnObjectsToDraw[i].drawing.position.xF = newValues.drawing.position.xF;
                this.drawnObjectsToDraw[i].drawing.position.yF = newValues.drawing.position.yF;
                break;
            }
        }
        this.setSaved(false);
    };

    // ====================================================================== //
    // ========== // R E L A T I O N // ===================================== //
    // ====================================================================== //
    // ========== D E L E T E // R E L A T I O N ===============================
    prototype.deleteRelation = function (relationKey)
    {
        this.createItemList();
        for (var i = 0; i < this.relationsToDraw.length; i++)
        {
            var relation = this.relationsToDraw[i];
            if (relation.key === Number(relationKey))
            {
                // if(relation.start.value.properties.isDeleted || !relation.start 
                //     || relation.end.value.properties.isDeleted || !relation.end )
                // {
                    relation.properties.isDeleted = true;
                    this.setSaved(false);
                    return true;
                // }
            }
        }
        alert('Ne peut être supprimé // Ce popup ne devrait jamais s\'afficher!');
        this.setSaved(false);
    };
    prototype.deleteRelationNode = function (relationKey)
    {
        var relationNodeKey = null;
        var relationToDelKey = null;
        for (var i = 0; i < this.relationsToDraw.length; i++)
        {

            var relation = this.relationsToDraw[i];
            if (relation.relationF && (relation.relationF.key === Number(relationKey)))
            {
                relationNodeKey = relation.nodeS.key;
                relationToDelKey = relation.key;
                break;
            }
        }
        if (!relationNodeKey)
        {
            arborescence.drawing.deleteRelationNodeErrorFlag = true;
        } else
        {
            arborescence.drawing.deleteRelationNodeErrorFlag = false;

            for (var k = 0; k < this.relationsToDraw.length; k++)
            {
                var relation = this.relationsToDraw[k];
                if (relation.key === Number(relationToDelKey))
                {
                    this.relationsToDraw.splice(k, 1);
                    break;
                }
            }
            for (var j = 0; j < this.subTrees.length; j++)
            {
                var subTreeRoot = this.subTrees[j];
                if (subTreeRoot.key === Number(relationNodeKey))
                {
                    this.subTrees.splice(j, 1);
                    break;
                }
            }

            // ===== C L E A N E R 
            this.cleanRelations();
        }
        this.setSaved(false);
    };
    prototype.deleteRelationByNode = function (nodeKey)
    {
        this.createItemList();
        for (var i = 0; i < this.relationsToDraw.length; i++)
        {
            var relation = this.relationsToDraw[i];
            if (relation.start.value.key === Number(nodeKey) 
                    || relation.end.value.key === Number(nodeKey))
            {
                relation.properties.isDeleted = true;
            }
        }
        this.setSaved(false);
    };

    // ====================================================================== //
    // ========== // N O D E // ============================================= //
    // ====================================================================== //
    // ========== A D D // N O D E // N O D E ==================================
    prototype.addNodeOnNode = function (nodeKey)
    {
        nodeKey = Number(nodeKey);
        this.searchNodeAndAdd(nodeKey);
        this.setSaved(false);
    };
    prototype.addNodeAttenteOnNode = function (param)
    {
        nodeKey = Number(param.nodeKey);
        param.nodeKey = nodeKey;
        this.searchNodeAndAddAttente(param);
        this.setSaved(false);
    };
    prototype.searchNodeAndAdd = function (nodeKey)
    {
        for (var j = 0; j < this.subTrees.length; j++)
        {
            this.recursiveAddNodeOnNode(this.subTrees[j], nodeKey);
        }
    };
    prototype.searchNodeAndAddAttente = function (param)
    {
        for (var j = 0; j < this.subTrees.length; j++)
        {
            param.node = this.subTrees[j];
            this.recursiveAddNodeAttenteOnNode(param);
        }
    };
    prototype.recursiveAddNodeOnNode = function (node, nodeKey)
    {
        if (node)
        {
            if (node.key === Number(nodeKey))
            {
                this.linkNode(node);
                return true;
            }
            this.recursiveAddNodeOnNode(node.left, nodeKey);
            this.recursiveAddNodeOnNode(node.right, nodeKey);
        }
    };
    prototype.recursiveAddNodeAttenteOnNode = function (param)
    {
        if (param.node)
        {
            if (param.node.key === Number(param.nodeKey))
            {
                this.linkNodeAttente(param.node, param.side);
                return true;
            }
            var left    = param.node.left;
            var right   = param.node.right;
            param.node = left;
            this.recursiveAddNodeAttenteOnNode(param);
            param.node = right;
            this.recursiveAddNodeAttenteOnNode(param);
        }
    };
    prototype.linkNode = function (node)
    {
        if (!node)
        {
            return false;
        }

        var newNode                 = new arborescence.Node();
        var newRelation             = new arborescence.Relation();

        var properties              = 
        {
            isSubTreeLink   : true
        };

        if(node.drawing.align === arborescence.Align.RIGHT)
        {
            var positionNode        = 
            {
                x   : node.drawing.position.x + 128,
                y   : node.drawing.position.y + 32
            };     
            var drawingNode         =
            {
                position        : positionNode,
                align           : arborescence.Align.RIGHT
            };
            var propertiesNode      = 
            {
                isVerticalRelationNode : false
            };
            newNode                 = this.createSubTree(drawingNode, propertiesNode);

            var positionRelation    =
            {
                xS  : newNode.drawing.position.x,
                yS  : newNode.drawing.position.y,
                xF  : node.drawing.position.x,
                yF  : node.drawing.position.y
            };
            var drawingRelation     =
            {
                position        : positionRelation,
                tilt            : arborescence.Tilt.HORIZONTAL,
                direction       : arborescence.Direction.WEST
            };
            newRelation             = this.createNodeRelation(
                                        newNode, node, 
                                        drawingRelation, properties);
        }else if(node.drawing.align === arborescence.Align.LEFT
                    || node.drawing.align === arborescence.Align.CENTER)
        {
            var positionNode        = 
            {
                x   : node.drawing.position.x - 128,
                y   : node.drawing.position.y + 32
            };     
            var drawingNode         =
            {
                position        : positionNode,
                align           : arborescence.Align.LEFT
            };
            var propertiesNode      = 
            {
                isVerticalRelationNode : false
            };
            newNode                 = this.createSubTree(drawingNode, propertiesNode);

            var positionRelation    =
            {
                xS  : newNode.drawing.position.x,
                yS  : newNode.drawing.position.y,
                xF  : node.drawing.position.x,
                yF  : node.drawing.position.y
            };
            var drawingRelation     =
            {
                position        : positionRelation,
                tilt            : arborescence.Tilt.HORIZONTAL,
                direction       : arborescence.Direction.EAST
            };
            newRelation             = this.createNodeRelation(
                                        newNode, node, 
                                        drawingRelation, properties);
        }

        newRelation.drawing.type    = arborescence.RelationType.DETERMINATION;

        this.subTrees.push(newNode);
        this.relationsToDraw.push(newRelation);
    };
    prototype.linkNodeAttente = function (node, side)
    {
        if (!node)
        {
            return false;
        }

        var newNode                 = new arborescence.Node();
        var newRelation             = new arborescence.Relation();

        var properties              = 
        {
            isSubTreeLink       : true,
            isAttenteLink       : true
        };
        if (side === "left")
        {

            if (node.properties.isVerticalRelationNode)
            {
                var positionNode        = 
                {
                    x   : node.drawing.position.x - arborescence.NODEWIDTH/3,
                    y   : node.drawing.position.y - 72
                };     
                var drawingNode         =
                {
                    position        : positionNode,
                    align           : arborescence.Align.LEFT,
                    type            : arborescence.NodeType.ATTENTE
                };
                var propertiesNode      = 
                {
                    isVerticalRelationNode : true
                };
                newNode                 = this.createSubTree(drawingNode, propertiesNode);

                var positionRelation    =
                {
                    xS  : newNode.drawing.position.x,
                    yS  : newNode.drawing.position.y,
                    xF  : node.drawing.position.x,
                    yF  : node.drawing.position.y
                };
                var drawingRelation     =
                {
                    position        : positionRelation,
                    direction       : arborescence.Direction.NORTH,
                    tilt            : arborescence.Tilt.VERTICAL
                };
    
                newRelation             = this.createNodeRelation(
                                            newNode, node, 
                                            drawingRelation, properties
                                            );
            } else 
            {
                var positionNode        = 
                {
                    x   : node.drawing.position.x - 124,
                    y   : node.drawing.position.y
                };     
                var drawingNode         =
                {
                    position        : positionNode,
                    align           : arborescence.Align.LEFT,
                    type            : arborescence.NodeType.ATTENTE
                };
                var propertiesNode      = 
                {
                    isVerticalRelationNode : false
                };
                newNode                 = this.createSubTree(drawingNode, propertiesNode);

                var positionRelation    =
                {
                    xS  : newNode.drawing.position.x,
                    yS  : newNode.drawing.position.y,
                    xF  : node.drawing.position.x,
                    yF  : node.drawing.position.y
                };
                var drawingRelation     =
                {
                    position        : positionRelation,
                    direction       : arborescence.Direction.WEST
                };
                newRelation             = this.createNodeRelation(
                                            newNode, node, 
                                            drawingRelation, properties
                                            );
                newRelation.drawing.tilt    = arborescence.Tilt.HORIZONTAL;
            }

            newRelation.properties.isLeftAttenteLink    = true;
            newRelation.properties.isRightAttenteLink   = false;
            
        } else if (side === "right")
        {
            if (node.properties.isVerticalRelationNode)
            {
                var positionNode        = 
                {
                    x   : node.drawing.position.x + arborescence.NODEWIDTH/3,
                    y   : node.drawing.position.y - 72
                };     
                var drawingNode         =
                {
                    position        : positionNode,
                    align           : arborescence.Align.RIGHT,
                    type            : arborescence.NodeType.ATTENTE
                };
                var propertiesNode      = 
                {
                    isVerticalRelationNode : true
                };
                newNode                 = this.createSubTree(drawingNode, propertiesNode);

                var positionRelation    =
                {
                    xS  : newNode.drawing.position.x,
                    yS  : newNode.drawing.position.y,
                    xF  : node.drawing.position.x,
                    yF  : node.drawing.position.y
                };
                var drawingRelation     =
                {
                    position        : positionRelation,
                    direction       : arborescence.Direction.NORTH,
                    tilt            : arborescence.Tilt.VERTICAL
                };
                newRelation             = this.createNodeRelation(
                                            newNode, node, 
                                            drawingRelation, properties
                                            );
            } else 
            {
                var positionNode        = 
                {
                    x   : node.drawing.position.x + 124,
                    y   : node.drawing.position.y
                };     
                var drawingNode         =
                {
                    position        : positionNode,
                    align           : arborescence.Align.RIGHT,
                    type            : arborescence.NodeType.ATTENTE
                };
                var propertiesNode      = 
                {
                    isVerticalRelationNode : false
                };
                newNode                 = this.createSubTree(drawingNode, propertiesNode);

                var positionRelation    =
                {
                    xS  : newNode.drawing.position.x,
                    yS  : newNode.drawing.position.y,
                    xF  : node.drawing.position.x,
                    yF  : node.drawing.position.y
                };
                var drawingRelation     =
                {
                    position        : positionRelation,
                    direction       : arborescence.Direction.EAST
                };
                newRelation             = this.createNodeRelation(
                                            newNode, node, 
                                            drawingRelation, properties
                                            );
                newRelation.drawing.tilt    = arborescence.Tilt.HORIZONTAL;
            }
            newRelation.properties.isLeftAttenteLink    = false;
            newRelation.properties.isRightAttenteLink   = true;
        }

        newRelation.drawing.type    = arborescence.RelationType.ATTENTE;

        this.subTrees.push(newNode);
        this.relationsToDraw.push(newRelation);
    };
    // ========== A D D // N O D E // R E L A T I O N ==========================
    prototype.addNodeOnRelation                     = function (relationKey)
    {
        relationKey             = Number(relationKey);
        var relation            = this.getRelation(relationKey);
        if (!relation)
        {
            return false;
        }

        var newNode             = new arborescence.Node();
        var newRelation         = new arborescence.Relation();
        var relationHeight      = Math.abs(relation.drawing.position.yS - relation.drawing.position.yF);
        var relationWidth       = Math.abs(relation.drawing.position.xS - relation.drawing.position.xF);

        var properties          = 
        {
            isSubTreeLink           : true
        };

        if (relation.drawing.tilt === arborescence.Tilt.HORIZONTAL)
        {
            // Create VERTICAL relation + node
            var positionNode    = 
            {
                x   : relation.drawing.position.xS - (relationWidth/2),
                y   : relation.drawing.position.yS + 76
            };
            var drawingNode     =
            {
                position        : positionNode,
                align           : arborescence.Align.CENTER,
            };
            var propertiesNode  = 
            {
                isVerticalRelationNode  : true
            };
            newNode             = this.createSubTree(drawingNode, propertiesNode);
            var positionRelation=
            {
                xS  : newNode.drawing.position.x,
                yS  : newNode.drawing.position.y,
                xF  : newNode.drawing.position.x,
                yF  : relation.drawing.position.yS
            };
            var drawingRelation =
            {
                position        : positionRelation,
                tilt            : arborescence.Tilt.VERTICAL,
                direction       : arborescence.Direction.NORTH
            };
            newRelation         = this.createRelationRelation(newNode, 
                                    relation, 
                                    drawingRelation, 
                                    properties);

        }else if (relation.drawing.tilt === arborescence.Tilt.VERTICAL)
        {
            // Create HORIZONTAL relation + node

            var positionNode    = 
            {
                x   : relation.drawing.position.xS + 76,
                y   : relation.drawing.position.yS - (relationHeight/2)
            };     
            var drawingNode     =
            {
                position        : positionNode,
                align           : arborescence.Align.CENTER
            };
            var propertiesNode      = 
            {
                isVerticalRelationNode : false
            };
            newNode             = this.createSubTree(drawingNode, propertiesNode);
            var positionRelation=
            {
                xS  : newNode.drawing.position.x,
                yS  : newNode.drawing.position.y,
                xF  : relation.drawing.position.xS - arborescence.NODEWIDTH/2,
                yF  : newNode.drawing.position.y
            };
            var drawingRelation =
            {
                position        : positionRelation,
                tilt            : arborescence.Tilt.HORIZONTAL,
                direction       : arborescence.Direction.WEST
            };
            newRelation         = this.createRelationRelation(newNode, 
                                    relation, 
                                    drawingRelation, 
                                    properties);
        }

        this.subTrees.push(newNode);
        this.relationsToDraw.push(newRelation);
        this.setSaved(false);
    };
    prototype.addNodeOnRelationAtt                  = function (relationKey)
    {
        relationKey             = Number(relationKey);
        var relation            = this.getRelation(relationKey);
        if (!relation)
        {
            return false;
        }

        var newNode             = new arborescence.Node();
        var newRelation         = new arborescence.Relation();
        var relationHeight      = Math.abs(relation.drawing.position.yS - relation.drawing.position.yF);
        var relationWidth       = Math.abs(relation.drawing.position.xS - relation.drawing.position.xF);

        var properties          = 
        {
            isSubTreeLink           : true
        };

        if (relation.drawing.tilt === arborescence.Tilt.HORIZONTAL)
        {
            // Create VERTICAL relation + node
            var x = (relation.drawing.position.xS > relation.drawing.position.xF) ? relation.drawing.position.xS - (relationWidth/2) : relation.drawing.position.xF - (relationWidth/2);

            var positionNode    = 
            {
                x   : x,
                y   : relation.drawing.position.yS + 76
            };
            var drawingNode     =
            {
                position        : positionNode,
                align           : arborescence.Align.CENTER,
            };
            var propertiesNode  = 
            {
                isVerticalRelationNode  : true
            };
            newNode             = this.createSubTree(drawingNode, propertiesNode);

            var positionRelation=
            {
                xS  : newNode.drawing.position.x,
                yS  : newNode.drawing.position.y,
                xF  : newNode.drawing.position.x,
                yF  : relation.drawing.position.yS + arborescence.NODEHEIGHT/2
            };
            var drawingRelation =
            {
                position        : positionRelation,
                tilt            : arborescence.Tilt.VERTICAL,
                direction       : arborescence.Direction.NORTH
            };
            newRelation         = this.createRelationRelation(newNode, 
                                    relation, 
                                    drawingRelation, 
                                    properties);

        }else if (relation.drawing.tilt === arborescence.Tilt.VERTICAL)
        {
            // Create HORIZONTAL relation + node

            var positionNode    = 
            {
                x   : relation.drawing.position.xS + 76,
                y   : relation.drawing.position.yS - (relationHeight/2)
            };     
            var drawingNode     =
            {
                position        : positionNode,
                align           : arborescence.Align.CENTER
            };
            var propertiesNode      = 
            {
                isVerticalRelationNode : false
            };
            newNode             = this.createSubTree(drawingNode, propertiesNode);
            var positionRelation=
            {
                xS  : newNode.drawing.position.x,
                yS  : newNode.drawing.position.y,
                xF  : relation.drawing.position.xS - arborescence.NODEWIDTH/2,
                yF  : newNode.drawing.position.y
            };
            var drawingRelation =
            {
                position        : positionRelation,
                tilt            : arborescence.Tilt.HORIZONTAL,
                direction       : arborescence.Direction.WEST
            };
            newRelation         = this.createRelationRelation(newNode, 
                                    relation, 
                                    drawingRelation, 
                                    properties);
        }

        this.subTrees.push(newNode);
        this.relationsToDraw.push(newRelation);
        this.setSaved(false);
    };
    prototype.deleteSubtree                         = function (nodeKey)
    {
        for (var i = 0; i < this.subTrees.length; i++)
        {
            this.recursiveDeleteSubtree(this.subTrees[i], nodeKey);
        }
        this.cleanNodesAndRelations();
        this.setSaved(false);
    };
    prototype.recursiveDeleteSubtree                = function (node, key)
    {
        if (node)
        {
            if (node.key === Number(key))
            {
                this.deleteNodeChildren(node);
                // this.deleteUnusedNode(node);
                return true;
            }

            this.recursiveDelete(node.left, key);
            this.recursiveDelete(node.right, key);
        }
    };
    prototype.deleteNodeChildren                    = function (node)
    {
        if(node)
        {
            if (!node.left && !node.right)
            {
                node.properties.isDeleted = true;
            } else
            {
                var saveLeftKey     = node.left.key;
                var saveRightKey    = node.right.key;

                node.left           = null;
                node.right          = null;

                this.deleteRelationByNode(saveLeftKey);
                this.deleteRelationByNode(saveRightKey);
            }

        }
    };
    prototype.deleteUnusedNode                      = function (node)
    {
        var found = false;
        for (var i = 0; i < this.subTrees.length; i++)
        {
            this.recursiveCheckUnusedNode(this.subTrees[i], nodeKey, found);
            if (found)
                return true;
        }
        if(!found)
        {
            node.properties.isDeleted = true;
        }
    };
    prototype.recursiveCheckUnusedNode              = function (node, key, found)
    {
        if(node && node.left && node.right)
        {
            if (node.left.key === key 
                    || node.right.key === key)
            {
                found = true;
            }
        }
    }

    // ====================================================================== //
    // ========== // S A V E // ============================================= //
    // ====================================================================== //
    prototype.saveTree                              = function(name)
    {
        var jsonNodes       = this.createNodesJSON();
        var jsonRelations   = this.createRelationJSON();
        var jsonDrawnObjects= this.createDrawnJSON();
        var jsonNotes       = this.createNotesJSON();

        $.ajax({
            type        : 'POST',
            url         : "canvas",
            contentType : "application/x-www-form-urlencoded",
            data        : 
            {
                save            : true,
                name            : name,
                version         : arborescence.VERSION,
                jsonNodes       : JSON.stringify(jsonNodes),
                jsonRelations   : JSON.stringify(jsonRelations),
                jsonDrawnObjects: JSON.stringify(jsonDrawnObjects),
                jsonNotes       : JSON.stringify(jsonNotes),
                treeTags        : this.treeTags
            },
            success: function(response)
            {
                console.log("Tree saved.");
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                console.log('Save failed//'+errorThrown+'//'+textStatus);
            }
        });

        this.name = name;
        this.setSaved(true);
    };
    prototype.saveExercise                          = function(name, description)
    {
        this.parseAndSanitizeTree ();

        var jsonNodes       = this.createNodesJSON();
        var jsonRelations   = this.createRelationJSON();
        var jsonDrawnObjects= this.createDrawnJSON();
        var jsonNotes       = this.createNotesJSON();

        $.ajax({
            type        : 'POST',
            url         : "canvas",
            contentType : "application/x-www-form-urlencoded",
            data        : 
            {
                save            : true,
                name            : name,
                description     : description,
                jsonNodes       : JSON.stringify(jsonNodes),
                jsonRelations   : JSON.stringify(jsonRelations),
                jsonDrawnObjects: JSON.stringify(jsonDrawnObjects),
                jsonNotes       : JSON.stringify(jsonNotes),
                treeTags        : this.treeTags
            },
            success: function(response)
            {
                this.name = name;
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                console.log('Save failed//'+errorThrown+'//'+textStatus);
            }
        });

        this.setSaved(true);
    };
    prototype.beginExercise                          = function(id, name)
    {
        console.log("Begin Exercise // "+id+" // "+name);
        $.ajax({
            type        : 'POST',
            url         : "canvas",
            contentType : "application/x-www-form-urlencoded",
            data        : 
            {
                exerciseId      : id,
                solutionName    : name,
                beginExercise   : true,
                description     : description
            },
            success: function(response)
            {
                this.name = name;
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                console.log('Save failed//'+errorThrown+'//'+textStatus);
            }
        });

        this.setSaved(true);
    };
    prototype.createNodesJSON                       = function ()
    {
        var jsonNodes =
        {
            tree:
            {
                version     : arborescence.VERSION,
                key         : arborescence.key,
                level       : arborescence.level,
                description : this.description,
                title       :
                {
                    value   : this.title.value,
                    bold    : this.title.bold,
                    italic  : this.title.italic,
                    size    : this.title.size
                },
                subtrees    :
                {},
                defaultWidth    : arborescence.DEFAULT_WIDTH,
                defaultHeight   : arborescence.DEFAULT_HEIGHT,
                width           : arborescence.WIDTH,
                height          : arborescence.HEIGHT,
                font            : arborescence.FONT,
                fontSize        : arborescence.FONTSIZE,
                nodeWidth       : arborescence.NODEWIDTH,
                nodeHeight      : arborescence.NODEHEIGHT,
                xGap            : arborescence.NODE_XGAP,
                yGap            : arborescence.NODE_YGAP
            }
        };
        for (var i = 0; i < this.subTrees.length; i++)
        {
            var subtree = this.subTrees[i];
            var subtreeLabel = "subtree" + i;

            this.addNodeToJSON(subtree, jsonNodes.tree.subtrees, subtreeLabel);
        }
        
        return jsonNodes;
    };
    prototype.createRelationJSON                    = function ()
    {
        var jsonRelations = 
        {
            relations:
            {}
        };
        for(var j = 0; j < this.relationsToDraw.length; j++){
            var label = "relation"+j;
            this.addRelationToJSON(this.relationsToDraw[j], label, jsonRelations.relations);
        }
        return jsonRelations;
    };
    prototype.createNotesJSON                       = function()
    {
        var jsonNotes =
        {
            notes:
            {}
        };
        
        for(var l = 0; l < this.notes.length; l++)
        {
            var label = "note"+l;
            this.addNoteToJSON(this.notes[l], label, jsonNotes.notes);
        }

        return jsonNotes;
    };
    prototype.createDrawnJSON                       = function()
    {
        var jsonDrawn =
        {
            drawnObjects:
            {}
        };
        
        for(var k = 0; k < this.drawnObjectsToDraw.length; k++)
        {
            var label = "drawnObject"+k;
            this.addDrawnObjectToJSON(this.drawnObjectsToDraw[k], label, jsonDrawn.drawnObjects);
        }

        return jsonDrawn;
    };
    prototype.addNodeToJSON                         = function(node, jsonNode, label)
    {
        if (node)
        {
            jsonNode[label] = {
                key         : node.key,
                value       :
                {
                    fonction            : node.value.fonction,
                    structureIntegrative: node.value.structureIntegrative,
                    terme               : node.value.terme
                },
                left        : {},
                right       : {},
                level       : node.level,
                drawing     : 
                {
                    position :
                    {
                        x : node.drawing.position.x,
                        y : node.drawing.position.y
                    },
                    fontSize: node.drawing.fontSize,
                    type    : node.drawing.type,
                    align   : node.drawing.align,
                    color   : node.drawing.color
                },
                properties  :
                {
                    isDeleted               : node.properties.isDeleted,
                    isVerticalRelationNode  : node.properties.isVerticalRelationNode,

                    isValueVisible          : node.properties.isValueVisible,
                    isChildrenVisible       : node.properties.isChildrenVisible,
                    isVisible               : node.properties.isVisible
                }
            };
            this.addNodeToJSON(node.left, jsonNode[label], "left");
            this.addNodeToJSON(node.right, jsonNode[label], "right");
        }
    };
    prototype.addRelationToJSON                     = function(relation, label, jsonNode) 
    {
        jsonNode[label] = 
        {
            key             : relation.key,
            start           : {},
            end             : {},
            drawing         :
            {
                position :
                {
                    xS      : relation.drawing.position.xS,
                    yS      : relation.drawing.position.yS,
                    xF      : relation.drawing.position.xF,
                    yF      : relation.drawing.position.yF
                },
                color       : relation.drawing.color,
                tilt        : relation.drawing.tilt,
                type        : relation.drawing.type,
                direction   : relation.drawing.direction
            },
            properties      :
            {
                isDeleted           : relation.properties.isDeleted,
                isSubTreeLink       : relation.properties.isSubTreeLink,
                isAttenteLink       : relation.properties.isAttenteLink,
                isLeftAttenteLink   : relation.properties.isLeftAttenteLink,
                isRightAttenteLink  : relation.properties.isRightAttenteLink,
                isVisible           : relation.properties.isVisible
            }
        };
        if (relation.start)
        {
            jsonNode[label].start = {
                type        : arborescence.AnchorType.NODE,
                value       :
                {
                    key         : relation.start.value.key,
                    value       :
                    {
                        fonction            : relation.start.value.fonction,
                        structureIntegrative: relation.start.value.structureIntegrative,
                        terme               : relation.start.value.terme
                    },
                    left        : {},
                    right       : {},
                    level       : relation.start.value.level,
                    drawing     : 
                    {
                        position :
                        {
                            x : relation.start.value.drawing.position.x,
                            y : relation.start.value.drawing.position.y
                        },
                        type    : relation.start.value.drawing.type,
                        align   : relation.start.value.drawing.align
                    },
                    properties  :
                    {
                        isDeleted               : relation.start.value.properties.isDeleted,
                        isVerticalRelationNode  : relation.start.value.properties.isVerticalRelationNode
                    }
                }
            };
        }
        if (relation.end)
        {
            if (relation.end.type === arborescence.AnchorType.NODE)
            {
                jsonNode[label].end = 
                {
                    type        : arborescence.AnchorType.NODE,
                    value       :
                    {
                        key         : relation.end.value.key,
                        value       :
                        {
                            fonction            : relation.end.value.fonction,
                            structureIntegrative: relation.end.value.structureIntegrative,
                            terme               : relation.end.value.terme
                        },
                        left        : {},
                        right       : {},
                        level       : relation.end.value.level,
                        drawing     : 
                        {
                            position    :
                            {
                                x   : relation.end.value.drawing.position.x,
                                y   : relation.end.value.drawing.position.y
                            },
                            type        : relation.end.value.drawing.type,
                            align       : relation.end.value.drawing.align
                        },
                        properties  :
                        {
                            isDeleted               : relation.end.value.properties.isDeleted,
                            isVerticalRelationNode  : relation.end.value.properties.isVerticalRelationNode
                        }
                    }
                };
            } else if (relation.end.type === arborescence.AnchorType.RELATION)
            {
                jsonNode[label].end = 
                {
                    type        : arborescence.AnchorType.RELATION,
                    value       :
                    {
                        key             : relation.end.value.key,
                        start           : {},
                        end             : {},
                        drawing         :
                        {
                            position :
                            {
                                xS      : relation.end.value.drawing.position.xS,
                                yS      : relation.end.value.drawing.position.yS,
                                xF      : relation.end.value.drawing.position.xF,
                                yF      : relation.end.value.drawing.position.yF
                            },
                            color       : relation.end.value.drawing.color,
                            tilt        : relation.end.value.drawing.tilt,
                            type        : relation.end.value.drawing.type,
                            direction   : relation.end.value.drawing.direction
                        },
                        properties      :
                        {
                            isDeleted           : relation.end.value.properties.isDeleted,
                            isSubTreeLink       : relation.end.value.properties.isSubTreeLink,
                            isAttenteLink       : relation.end.value.properties.isAttenteLink,
                            isLeftAttenteLink   : relation.end.value.properties.isLeftAttenteLink,
                            isRightAttenteLink  : relation.end.value.properties.isRightAttenteLink,
                            isVisible           : relation.end.value.properties.isVisible
                        }
                    }
                }
            }
        }
    };
    prototype.addNoteToJSON                         = function(note, label, jsonNode)
    {
        jsonNode[label] = 
        {
            key             : note.key,
            value           : note.value,
            drawing         :
            {
                position :
                {
                    x      : note.drawing.position.x,
                    y      : note.drawing.position.y,
                },
                width       : note.drawing.width,
                fontSize    : note.drawing.fontSize,
                background  : note.drawing.background
            },
            properties  :
            {
                isDeleted   : note.properties.isDeleted
            }
        };
    };
    prototype.addDrawnObjectToJSON                  = function(drawnObject, label, jsonNode)
    {
        jsonNode[label] = 
        {
            key             : drawnObject.key,
            start           : {},
            end             : {},
            drawing         :
            {
                position :
                {
                    xS      : drawnObject.drawing.position.xS,
                    yS      : drawnObject.drawing.position.yS,
                    xM      : drawnObject.drawing.position.xM,
                    yM      : drawnObject.drawing.position.yM,
                    xF      : drawnObject.drawing.position.xF,
                    yF      : drawnObject.drawing.position.yF
                },
                tilt        : drawnObject.drawing.tilt,
                objectType  : drawnObject.drawing.objectType,
                type        : drawnObject.drawing.type,
                align       : drawnObject.drawing.direction
            },
            properties  :
            {
                isDeleted   : drawnObject.properties.isDeleted
            }
        };
    };
    // ========== R E L O A D ==================================================
    prototype.reloadTree                              = function()
    {
        if (!this.name || this.name.trim() == '')
            return false;

        $.ajax({
            type        : 'POST',
            url         : "canvas",
            contentType : "application/x-www-form-urlencoded",
            data        : 
            {
                reload          : true,
                name            : this.name
            },
            success: function(response)
            {
                console.log("Done. Reloading");
                window.location.href = response;
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                console.log('Load failed//'+errorThrown+'//'+textStatus);
            }
        });

        this.setSaved(true);
    };

    // ========= // P A R S E . T R E E ========================================
    prototype.parseAndSanitizeTree                  = function ()
    {
        for (var i = 0; i < this.subTrees.length; i++)
        {
            var subtree = this.subTrees[i];
            this.sanitizeNode(subtree);
        }
    };
    prototype.sanitizeNode                          = function (node)
    {
        if (!node.properties.isChildrenVisible)
        {
            this.setVisibility(node.left, node.properties.isChildrenVisible);
            this.setVisibility(node.right, node.properties.isChildrenVisible);
        }
    };


    // ====================================================================== //
    // ========== // T A G // =============================================== //
    // ====================================================================== //
    prototype.addTag                                = function (tag)
    {
        var notFound        = true;

        for (var i = 0; i < this.treeTags.length; i++) 
        {
            if (this.treeTags[i].body.toLowerCase() === tag.toLowerCase())
            {
                notFound    = false;
            }
        }
        if (notFound)
        {
            this.treeTags.push(
                { id : null, body : tag, deleted : false}
            );
            $("#tagBody").val('');
            var tagListContent = "";
            tagListContent +=   "<a class='tag disabled'>" + 
                                    tag + 
                                "</a><a class='tagDelete' href='javascript:arborescence.drawing.removeTag(\""+ tag.toLowerCase() +"\")'><i class='fa fa-times'></i></a>";
            $('#tag-list').append( tagListContent );
        }

        this.setSaved(false);
    };
    prototype.deleteTreeTag                         = function (tagBody)
    {
        for (var i = 0; i < this.treeTags.length; i++) 
        {
            if (this.treeTags[i].body.toLowerCase() === tagBody.toLowerCase())
            {
                this.treeTags[i].deleted    = true;
            }
        }
        this.setSaved(false);
    };
    prototype.removeTag                             = function (tag)
    {
        for (var i = 0; i < this.treeTags.length; i++) 
        {
            if (this.treeTags[i].body.toLowerCase() === tag)
            {
                this.treeTags.splice( i, 1);
                this.addTempTags ();
                return true;
            }
        }

    };
    prototype.addTempTags                           = function ()
    {
        $("#tagBody").val('');
        $("#tag-list").empty();
        var tagListContent = "";

        for (var i = 0; i < this.treeTags.length; i++) 
        {
            tagListContent +=   "<a class='tag disabled'>" + 
                                    this.treeTags[i].body + 
                                "</a><a class='tagDelete' href='javascript:arborescence.drawing.removeTag(\""+ this.treeTags[i].body +"\")'><i class='fa fa-times-circle'></i></a>";
        };

        $('#tag-list').append( tagListContent );
    };
    prototype.searchTreeByTag                       = function (tag)
    {
        $.ajax({
            type        : 'POST',
            url         : "/arborescence/trees",
            contentType : "application/x-www-form-urlencoded",
            data        : 
            {
                tag             : tag
            },
            success: function(response)
            {
                $("#treesByTagBody").empty();
                $("#treesByTagBody").html(response);
                $('#treesByTag').modal('show');
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                console.log('Save failed//'+errorThrown+'//'+textStatus);
            }
        });
    };

    // ====================================================================== //
    // ========== // E X E R C I S E // ===================================== //
    // ====================================================================== //
    // ========== C H E C K 
    prototype.verifyExercise                        = function ()
    {
        var jsonNodes       = this.createNodesJSON();
        var jsonRelations   = this.createRelationJSON();
        var jsonDrawnObjects= this.createDrawnJSON();
        var jsonNotes       = this.createNotesJSON();

        $.ajax({
            type        : 'POST',
            url         : "/arborescence/trees/exercise/verify",
            contentType : "application/x-www-form-urlencoded",
            data        : 
            {
                exerciseId          : this.exercise,
                treeNodes           : jsonNodes,
                treeRelations       : jsonRelations,
                treeDrawnObjects    : jsonDrawnObjects
            },
            success: function(response)
            {
                $("#verifyExerciseMessage").removeClass('hidden');
                $("#verifyExerciseMessage").html(response);
                $("#verifyExerciseModal").modal('show');
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                console.log('Failed//'+errorThrown+'//'+textStatus);
            }
        });
    };

    // ====================================================================== //
    // ========== // C O M M O N S // ======================================= //
    // ====================================================================== //
    // The end is a node
    prototype.createNodeRelation                    = function(start, end, drawing, properties)
    {
        var relation                    = new arborescence.Relation();
        arborescence.key                += 1;
        relation.key                    = arborescence.key;
        relation.start                  = {};
        relation.start.type             = arborescence.AnchorType.NODE;
        relation.start.value            = start;
        relation.end                    = {};
        relation.end.type               = arborescence.AnchorType.NODE;
        relation.end.value              = end;

        if (drawing && drawing.position)
            relation.drawing.position   = drawing.position;
        else
        {
            relation.drawing.position.xS    = start.drawing.position.x;
            relation.drawing.position.yS    = start.drawing.position.y;
            relation.drawing.position.xF    = end.drawing.position.x;
            relation.drawing.position.yF    = end.drawing.position.y;
        }

        if (drawing && drawing.type)
            relation.drawing.type       = drawing.type;
        if (drawing && drawing.tilt)
            relation.drawing.tilt       = drawing.tilt;
        if (drawing && drawing.direction)
            relation.drawing.direction  = drawing.direction;

        if (properties && properties.isSubTreeLink)
            relation.properties.isSubTreeLink = properties.isSubTreeLink;
        if (properties && properties.isAttenteLink)
            relation.properties.isAttenteLink = properties.isAttenteLink;
        if (properties && properties.isLeftAttenteLink)
            relation.properties.isLeftAttenteLink = properties.isLeftAttenteLink;
        if (properties && properties.isRightAttenteLink)
            relation.properties.isRightAttenteLink = properties.isRightAttenteLink;

        this.setSaved(false);
        return relation;
    };
    // The end is a relation
    prototype.createRelationRelation                = function(start, end, drawing, properties)
    {
        var relation                        = new arborescence.Relation();
        arborescence.key                    += 1;
        relation.key                        = arborescence.key;
        relation.start                      = {};
        relation.start.type                 = arborescence.AnchorType.NODE;
        relation.start.value                = start;
        relation.end                        = {};
        relation.end.type                   = arborescence.AnchorType.RELATION;
        relation.end.value                  = end;

        if (drawing && drawing.position)
            relation.drawing.position   = drawing.position;
        if (drawing && drawing.type)
            relation.drawing.type       = drawing.type;
        if (drawing && drawing.tilt)
            relation.drawing.tilt       = drawing.tilt;
        if (drawing && drawing.direction)
            relation.drawing.direction  = drawing.direction;

        if (properties && properties.isSubTreeLink)
            relation.properties.isSubTreeLink = properties.isSubTreeLink;
        if (properties && properties.isAttenteLink)
            relation.properties.isAttenteLink = properties.isAttenteLink;

        this.setSaved(false);
        return relation;
    };
    // ========== C R E A T E // S U B T R E E =================================
    prototype.createSubTree                         = function(drawing, properties)
    {
        var node                        = new arborescence.Node();
        arborescence.key                += 1;
        node.key                        = arborescence.key;
        node.level                      = arborescence.level;
        node.value                      = 
        {
            fonction                : this.Fonctions.DEFAULT,
            structureIntegrative    : this.StructuresIntegratives.DEFAULT,
            terme                   : null
        };

        if(drawing.position)
            node.drawing.position       = drawing.position;
        if(drawing.type)
            node.drawing.type           = drawing.type;
        if(drawing.align)
            node.drawing.align          = drawing.align;

        if (properties.isVerticalRelationNode)
            node.properties.isVerticalRelationNode = properties.isVerticalRelationNode;

        this.setSaved(false);
        return node;
    };
    // ========== G E T // N O D E ============================================
    prototype.getNode                               = function(key)
    {
        for (var i = 0; i < this.subTrees.length; i++)
        {
            return this.recursiveGetNode(this.subTrees[i], key);
        }
        return null;
    };
    prototype.recursiveGetNode                      = function (node, key)
    {
        if (node )
        {
            if (node.key === Number(key))
            {
                return node;
            } else
            {
                this.recursiveGetNode(node.left, key);
                this.recursiveGetNode(node.right, key);        
            }
        }
    };
    // ========== G E T // R E L A T I O N =====================================
    prototype.getRelation                           = function(key)
    {
        for (var i = 0; i < this.relationsToDraw.length; i++)
        {
            var relation = this.relationsToDraw[i];
            if (relation.key === Number(key))
                return relation;
        }
        return null;
    };
    // ========== C R E A T E // N O D E =======================================
    prototype.createNode                            = function ()
    {};
    // ========== ==============================================================
    prototype.searchAllNotices                      = function(criteria) 
    {
        $.ajax({
            type        : 'POST',
            url         : "search/notice/all",
            contentType : "application/x-www-form-urlencoded",
            data        : 
            {},
            success: function(response)
            {
                $("#noticeModalBody").empty();
                $("#noticeModalBody").html(response);
                $('#noticeModal').modal('show');
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                console.log('Failed//'+errorThrown+'//'+textStatus);
            }
        });
    };
    // =========================================================================
    // ========== C H E C K // R U L E S =======================================
    // =========================================================================
    prototype.checkImplications                     = function(node)
    {
        this.checkFonctionImplications (node);
        this.checkStrIntegrativeImplications (node);  
    };
    prototype.checkFonctionImplications             = function (node)
    {
        switch (node.value.fonction)
        {
            case this.Fonctions.PHRASE:
                node.value.structureIntegrative = this.StructuresIntegratives.GPPREMIER;
                break;
            case this.Fonctions.SOUSPHRASE:
                node.value.structureIntegrative = this.StructuresIntegratives.GPPREMIERPRIME;
                break;
            default:
                break;
        }
    };
    prototype.checkStrIntegrativeImplications       = function (node)
    {
        if(node && node.left && node.right)
        {
            // ===== GP1 ==>> PREDICAT 
            if (node.value.structureIntegrative === this.StructuresIntegratives.GPPREMIER)
            {
                node.right.value.fonction = this.Fonctions.PREDICAT;
            }

            // ===== GP2 ==>> PREDICAT SECOND
            if (node.value.structureIntegrative === this.StructuresIntegratives.GPSECOND)
            {
                node.right.value.fonction = this.Fonctions.PREDICATSECOND;
            }

            // ===== GDx ==>> DETERMINANT
            if (node.value.structureIntegrative[0] === 'G' &&
                    node.value.structureIntegrative[1] === 'D')
            {
                node.right.value.fonction = this.Fonctions.DETERMINANT;
                if (node.value.structureIntegrative === this.StructuresIntegratives.GDNOM)
                    node.left.value.structureIntegrative = this.StructuresIntegratives.NOM;
                if (node.value.structureIntegrative === this.StructuresIntegratives.GDADJECTIF)
                    node.left.value.structureIntegrative = this.StructuresIntegratives.ADJECTIF;
                if (node.value.structureIntegrative === this.StructuresIntegratives.GDADVERBE)
                    node.left.value.structureIntegrative = this.StructuresIntegratives.ADVERBE;
                if (node.value.structureIntegrative === this.StructuresIntegratives.GDPRONOM)
                    node.left.value.structureIntegrative = this.StructuresIntegratives.PRONOM;
                if (node.value.structureIntegrative === this.StructuresIntegratives.GDVERBE)
                    node.left.value.structureIntegrative = this.StructuresIntegratives.VERBE;
                if (node.value.structureIntegrative === this.StructuresIntegratives.GDCONNECTEUR)
                    node.left.value.structureIntegrative = this.StructuresIntegratives.CCOORD;
            }
        }
    };
    prototype.checkRelations                        = function(node)
    {
        // ===== C H E C K // 2EME LETTRE
        if ((node.value.structureIntegrative[1] === 'P'))
        {
            // Relation type double                      
            this.updateRelationByNode(
                node.right.key, 
                node.left.key, 
                arborescence.RelationType.PREDICATION);
        } else if ((node.value.structureIntegrative[1] === 'D'))
        {
            // Relation type simple
            this.updateRelationByNode(
                node.right.key, 
                node.left.key, 
                arborescence.RelationType.DETERMINATION);
        }
    };
    prototype.checkRules                            = function()
    {};
    prototype.cleanNodesAndRelations                = function()
    {
        this.cleanUnlinkNodes();
        this.cleanBrokenRelations();
    };
    prototype.cleanUnlinkNodes                      = function ()
    {};
    prototype.cleanBrokenRelations                  = function()
    {
        this.createItemList();
        for (var i = 0; i < this.relationsToDraw.length; i++)
        {
            var relation = this.relationsToDraw[i];
            if(!relation.start 
                    && !relation.end )
            {
                relation.properties.isDeleted = true;
                return true;
            }
        }
    };
    prototype.setSaved                              = function (state)
    {
        arborescence.isSaved    = state;
        if (arborescence.isSaved)
        {
            $("#saveStatus").empty();
            $("#saveStatus").append("<small><i class='fa fa-circle color-green'></i> Toutes les modifications ont été sauvegardées.<small>");
        } else
        {
            $("#saveStatus").empty();
            $("#saveStatus").append("<i class='fa fa-circle color-red'></i> Des modifications n'ont pas encore été sauvegardées.");
        }
    };
    prototype.isSaved                               = function ()
    {
        return arborescence.isSaved;
    };

    // ====================================================================== //
    // ========== // A D M I N I S T R A T O R // =========================== //
    // ====================================================================== //
    prototype.searchTreesTool                   = function (criteria)
    {
    }
    prototype.searchUsersTool                   = function(criteria) 
    {
        $.ajax({
            type        : 'POST',
            url         : "/arborescence/admin/search",
            contentType : "application/x-www-form-urlencoded",
            data        : 
            {
                submit          : 'searchUser',
                name            : criteria['name'],
                email           : criteria['email'],
                tags            : criteria['tags']         
            },
            success: function(response)
            {
                $("#usersFromSearchBody").empty();
                $("#usersFromSearchBody").html(response);
                $('#usersFromSearch').modal('show');
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                console.log('Failed//'+errorThrown+'//'+textStatus);
            }
        });
    };

    return Tree;

})();