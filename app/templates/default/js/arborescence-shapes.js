var arborescence = arborescence || {};

arborescence.TreeShapes = (function ()
{
    function TreeShapes()
    {};

    // ====================================================================== //
    // ========== D  A S H E D . C O N T E N T                                //
    // ====================================================================== //
    (createjs.Graphics.Dash             = function(instr)
    {
        if(instr == null)
        {
            instr = [0];
        }
        this.instr = instr;
    }).prototype.exec                   = function(ctx) 
    {
        var context = ctx || arborescence.canvas.getContext('2d');
        context.setLineDash(this.instr);
    };
    createjs.Graphics.prototype.dash    = function(instr) 
    {
        return this.append(new createjs.Graphics.Dash(instr));
    };

    // ====================================================================== //
    // ========== C O M M O N . S H A P E S                                   //
    // ====================================================================== //
    TreeShapes.rectangle                = function (rect)
    {
        rect.strokeThickness = rect.strokeThickness || 0;
        rect.strokeColor = rect.strokeColor || "#000";
        rect.fillColor = rect.fillColor || "#fff";
        rect.x = rect.x || 0;
        rect.y = rect.y || 0;
        rect.width = rect.width || 0;
        rect.height = rect.height || 0;
        rect.alpha   = rect.alpha || 1;

        // ===== D R A W =======================================================
        var shape = new createjs.Shape();
        if (rect.strokeThickness > 0)
        {
            shape.graphics.setStrokeStyle(rect.strokeThickness);
            shape.graphics.beginStroke(rect.strokeColor);
        }
        shape.graphics.beginFill(rect.fillColor);
        shape.set({
            cursor: 'pointer',
            alpha: rect.alpha
        });
        shape.graphics.rect(rect.x, rect.y, rect.width, rect.height);

        return shape;
    };
    TreeShapes.postIt                = function (postIt)
    {
        postIt.strokeThickness      = postIt.strokeThickness || 0;
        postIt.strokeColor          = postIt.strokeColor || "#000";
        postIt.fillColor            = postIt.fillColor || "#fff";
        postIt.x                    = postIt.x || 0;
        postIt.y                    = postIt.y || 0;
        postIt.width                = postIt.width || 0;
        postIt.height               = postIt.height || 0;
        postIt.alpha                = postIt.alpha || 1;

        // ===== D R A W =======================================================
        var shape = new createjs.Shape();
        if (postIt.strokeThickness > 0)
        {
            shape.graphics.setStrokeStyle(postIt.strokeThickness);
            shape.graphics.beginStroke(postIt.strokeColor);
        }
        shape.graphics.beginFill(postIt.fillColor);
        shape.set({
            cursor: 'pointer',
            alpha: postIt.alpha
        });
        // shape.graphics.rect(postIt.x, postIt.y, postIt.width, postIt.height);
        shape.graphics.moveTo(postIt.x + 8, postIt.y)
                        .lineTo(postIt.x + postIt.width, postIt.y)
                        .lineTo(postIt.x + postIt.width, postIt.y + postIt.height)
                        .lineTo(postIt.x, postIt.y + postIt.height)
                        .lineTo(postIt.x, postIt.y + 8)
                        .lineTo(postIt.x + 8, postIt.y);

        return shape;
    };
    TreeShapes.line                     = function (line)
    {
        var x1 = line.x1 || 0;
        var y1 = line.y1 || 0;
        var x2 = line.x2 || 0;
        var y2 = line.y2 || 0;
        var color = line.color || "#000";

        var line = new createjs.Shape();
        line.graphics.ss(1)
                        .s(color)
                        .mt(x1, y1)
                        .lt(x2, y2);

        return line;
    };
    TreeShapes.curvedArrow              = function (arrow)
    {
        var xS              = arrow.xS      || 0;
        var yS              = arrow.yS      || 0;
        var xM              = arrow.xM      || 0;
        var yM              = arrow.yM      || 0;
        var xF              = arrow.xF      || 0;
        var yF              = arrow.yF + 15 || 0;
        var color           = "#073642";
        var type            = arrow.type;
        var relationType    = arrow.relationType;
        
        var arrowDraw   = new createjs.Container();
        switch (Number(relationType))
        {
            case 1:
                arrowDraw.addChild(new arborescence.TreeShapes.dashedCurveArrow(xS, yS, xM, yM, xF, yF, 8));
                break;
            case 2:
                arrowDraw.addChild(new arborescence.TreeShapes.dashedCurveArrow(xS - 3, yS, xM - 3, yM, xF - 3, yF, 8));              
                arrowDraw.addChild(new arborescence.TreeShapes.dashedCurveArrow(xS + 3, yS, xM - 3, yM, xF + 3, yF, 8));
                break;
            default:
                console.log("//");
        }
        
        var polygon     = new createjs.Shape();
        polygon.graphics.beginStroke(color);
        polygon.graphics.beginFill(color);
        
        var ptX1    = xF;
        var ptY1    = yF - 15;
        var ptX2    = ptX1 - 6;
        var ptY2    = ptY1 + 15;
        var ptX3    = ptX2 + 15;
        var ptY3    = ptY2;
        polygon.graphics.moveTo(ptX1, ptY1)
                .lineTo(ptX2, ptY2)
                .lineTo(ptX3, ptY3)
                .lineTo(ptX1, ptY1);
        arrowDraw.addChild(polygon);
        arrowDraw.set({
            cursor: 'pointer'
        });
        
        return arrowDraw;
    };
    // ====================================================================== //
    TreeShapes.dashedLine               = function(x1, y1, x2, y2, dashLen) 
    {
        var dashedArrow = new createjs.Container();
        
        var dashedLine = new createjs.Shape();
        dashedLine.graphics.ss(1)
                                .s(arborescence.Colors.BLACK)
                                .mt(x1, y1);
        dashedLine.graphics;

        var dX = x2 - x1;
        var dY = y2 - y1;
        var dashes = Math.floor(Math.sqrt(dX * dX + dY * dY) / dashLen);
        var dashX = dX / dashes;
        var dashY = dY / dashes;

        var q = 0;
        while (q++ < dashes) 
        {
            x1 += dashX;
            y1 += dashY;
            dashedLine.graphics[q % 2 === 0 ? 'mt' : 'lt'](x1, y1);
        }
        dashedLine.graphics[q % 2 === 0 ? 'mt' : 'lt'](x2, y2); 
        
        dashedArrow.addChild(dashedLine);
        
        return dashedArrow;
    };
    TreeShapes.dashedCurveArrow         = function(x1, y1, x2, y2, x3, y3, dashLen) 
    {
        var color       = arborescence.Colors.BLACK;
        var dashedArrow = new createjs.Container();
        
        var dashedCurve = new createjs.Shape();
        
        dashedCurve.graphics
                .setStrokeStyle(1)
                .beginStroke(color);
        dashedCurve.graphics
                .moveTo(x1, y1);

        dashedCurve.graphics.dash([dashLen, dashLen/2]);
        dashedCurve.graphics.quadraticCurveTo(x2, y2, x3, y3);
        
        dashedArrow.addChild(dashedCurve);
        
        dashedArrow.set({
           cursor   : 'pointer' 
        });
        
        return dashedArrow;
    };

    // ====================================================================== //
    // ========== N O D E                                                     //
    // ====================================================================== //
    TreeShapes.node                     = function (node)
    {
        var nodeDrawing             = new createjs.Container();

        switch (node.drawing.type)
        {
            case arborescence.NodeType.NORMAL:
                nodeDrawing = new arborescence.TreeShapes.nodeNormal(node);
                break;
            case arborescence.NodeType.ATTENTE:
                nodeDrawing = new arborescence.TreeShapes.nodeAttente(node);
                break;
            default:
                console.log('NODETYPE incorrect!');
        }
        
        return nodeDrawing;
    };
    TreeShapes.nodeNormal               = function (node)
    {
        var nodeDrawing             = new createjs.Container();

        // Position
        nodeDrawing.x               = node.drawing.position.x;
        nodeDrawing.y               = node.drawing.position.y + 8;
        // Size
        nodeDrawing.width           = arborescence.NODEWIDTH;
        nodeDrawing.height          = arborescence.NODEHEIGHT;

        var color                   = node.drawing.color;

        var functionDrawing         = new createjs.Text(
            node.value.fonction, 
            "bold " + node.drawing.fontSize + "px " + arborescence.FONT, 
            color
        );
        functionDrawing.textAlign   = "center";
        nodeDrawing.addChild(functionDrawing);

        var strIntDrawing           = new createjs.Text(
            "(" + node.value.structureIntegrative + ")", 
            node.drawing.fontSize + "px " + arborescence.FONT, 
            color
        );
        strIntDrawing.y             = functionDrawing.y + node.drawing.fontSize + 4;
        strIntDrawing.textAlign     = "center";
        nodeDrawing.addChild(strIntDrawing);

        if (node.value.terme)
        {
            var termeDrawing        = new createjs.Text(
                node.value.terme, 
                "italic " + node.drawing.fontSize + "px " + arborescence.FONT, 
                color
            );
            termeDrawing.y          = strIntDrawing.y + node.drawing.fontSize + 4;
            termeDrawing.textAlign  = "center";
            nodeDrawing.addChild(termeDrawing);
        }

        return nodeDrawing;
    };
    TreeShapes.nodeAttente              = function (node)
    {
        var nodeDrawing             = new createjs.Container();

        // Position
        nodeDrawing.x               = node.drawing.position.x;
        nodeDrawing.y               = node.drawing.position.y + node.drawing.fontSize;
        // Size
        nodeDrawing.width           = arborescence.NODEWIDTH;
        nodeDrawing.height          = arborescence.NODEHEIGHT;

        var color                   = node.drawing.color;

        var text                    = node.value.fonction !== '' ? node.value.fonction : '******';
        var functionDrawing         = new createjs.Text(
            "(" + text + ")", 
            "bold " + node.drawing.fontSize + "px " + arborescence.FONT, 
            color
        );
        functionDrawing.textAlign   = "center";
        functionDrawing.y           = node.drawing.fontSize;
        nodeDrawing.addChild(functionDrawing);
        return nodeDrawing;
    };

    // ====================================================================== //
    // ========== N O T E                                                     //
    // ====================================================================== //
    TreeShapes.note                     = function (note)
    {
        var noteDrawing             = new createjs.Container();
        noteDrawing.name            = 'note';
        // noteDrawing.cursor          = 'pointer';

        // Position
        noteDrawing.x               = note.drawing.position.x;
        noteDrawing.y               = note.drawing.position.y;
        // Size
        noteDrawing.width           = note.drawing.width + 8;
        noteDrawing.height          = arborescence.NODEHEIGHT;

        // T E X T
        var noteText                = new createjs.Text(
            note.value, 
            "normal " + note.drawing.fontSize + "px " + arborescence.FONT, 
            "#000"
        );
        noteText.textAlign          = "left";
        noteText.lineWidth          = note.drawing.width;

        noteDrawing.addChild(noteText);
        
        return noteDrawing;
    };
    TreeShapes.noteNormal               = function (note)
    {
        var noteDrawing             = new createjs.Container();

        // Position
        noteDrawing.x               = note.drawing.position.x;
        noteDrawing.y               = note.drawing.position.y + 8;
        // Size
        noteDrawing.width           = note.drawing.width + 8;
        noteDrawing.height          = arborescence.NODEHEIGHT;

        // T E X T
        var noteText                = new createjs.Text(
            note.value, 
            "normal " + note.drawing.fontSize + "px " + arborescence.FONT, 
            "#000"
        );
        noteText.textAlign          = "center";
        noteText.lineWidth          = note.drawing.width;

        noteDrawing.addChild(noteText);

        return noteDrawing;
    };

    // ====================================================================== //
    // ========== R E L A T I O N                                             //
    // ====================================================================== //
    TreeShapes.relation                 = function (relation)
    {
        var relationDrawing = new createjs.Container();

        switch(relation.drawing.tilt)
        {
            case arborescence.Tilt.HORIZONTAL:
                relationDrawing = new arborescence.TreeShapes.relationHor(relation);
                break;
            case arborescence.Tilt.VERTICAL:
                relationDrawing = new arborescence.TreeShapes.relationVer(relation);
                break;
            case arborescence.Tilt.CURVE:
                break;
            default:
                console.log('TILT incorrect!');
        }

        return relationDrawing;
    };
    TreeShapes.relationHor              = function (relation)
    {
        var relationDrawing = new createjs.Container();
        var isAttenteLink = relation.properties.isAttenteLink;
        var isSubTreeLink = relation.properties.isSubTreeLink;
        if (isSubTreeLink)
        {   
            var xS          = relation.drawing.position.xS + arborescence.NODEWIDTH/2 || 0;
            var yS          = relation.drawing.position.yS + arborescence.FONTSIZE + 4 || 0;
            var xF          = relation.drawing.position.xF || 100;
            var yF          = relation.drawing.position.yF + arborescence.FONTSIZE + 4 || 100;
        }else
        {
            var xS          = relation.drawing.position.xS - arborescence.NODEWIDTH/2 - 8 || 0;
            var yS          = relation.drawing.position.yS + arborescence.FONTSIZE || 0;
            var xF          = relation.drawing.position.xF + arborescence.NODEWIDTH/2 + 8 || 100;
            var yF          = relation.drawing.position.yF + arborescence.FONTSIZE || 100;
        }
        var type        = relation.drawing.type || arborescence.RelationType.LIAISON;
        var color       = relation.drawing.color || arborescence.Colors.BLACK;
        var direction   = relation.drawing.direction;

        if(relation.properties.isSubTreeLink)
        {
            if (relation.drawing.direction === arborescence.Direction.WEST)
                xF += 4;
            else
                xF -= 4;
        }


        switch (type)
        {
            case arborescence.RelationType.LIAISON:
                if (direction === arborescence.Direction.WEST)
                {
                    if (isSubTreeLink)
                    {
                       xS = relation.drawing.position.xS - arborescence.NODEWIDTH/2 || 100;
                       xF = relation.drawing.position.xF + arborescence.NODEWIDTH/2 || 100;
                    }
                }else if (direction === arborescence.Direction.EAST)
                {
                    if (isSubTreeLink)
                    {

                        xF = relation.drawing.position.xF - arborescence.NODEWIDTH/2 || 100;
                    }
                }
                var line = new createjs.Shape();
                line.graphics.setStrokeStyle(1)
                                .beginStroke(color)
                                .moveTo(xS, yS)
                                .lineTo(xF, yF);

                relationDrawing.addChild(line);
                break;

            case arborescence.RelationType.DETERMINATION:
                if (direction === arborescence.Direction.WEST)
                {
                    if (isSubTreeLink)
                    {
                       xS = relation.drawing.position.xS - arborescence.NODEWIDTH/2 || 100;
                       xF = relation.drawing.position.xF + arborescence.NODEWIDTH/2 || 100;

                    }
                    var lineStart = new createjs.Shape();
                    lineStart.graphics.setStrokeStyle(1)
                        .beginStroke(color)
                        .moveTo(xF, yF)
                        .lineTo(xF + 24, yF);

                    var line = new createjs.Shape();
                    line.graphics.setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS, yS)
                        .lineTo(xF + 24, yF);
                } else if (direction === arborescence.Direction.EAST)
                {
                    if (isSubTreeLink)
                    {

                        var xF = relation.drawing.position.xF - arborescence.NODEWIDTH/2 || 100;
                    }
                    var lineStart = new createjs.Shape();
                    lineStart.graphics.setStrokeStyle(1)
                        .beginStroke(color)
                        .moveTo(xF, yF)
                        .lineTo(xF - 24, yF);

                    var line = new createjs.Shape();
                    line.graphics.setStrokeStyle(1)
                        .beginStroke(color)
                        .moveTo(xS, yS)
                        .lineTo(xF - 24, yF);
                }

                relationDrawing.addChild(lineStart);
                relationDrawing.addChild(line);
                break;

            case arborescence.RelationType.PREDICATION:
                if (direction === arborescence.Direction.WEST)
                {
                    if (isSubTreeLink)
                    {
                        xS = relation.drawing.position.xS - arborescence.NODEWIDTH/2 || 100;
                        xF = relation.drawing.position.xF + arborescence.NODEWIDTH/2 || 100;
                    }

                    var lineStart = new createjs.Shape();
                    lineStart.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF + 15, yF - 3)
                        .lineTo(xF + 24, yF - 3);
                    var lineStart2 = new createjs.Shape();
                    lineStart2.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF + 15, yF + 3)
                        .lineTo(xF + 24, yF + 3);

                    var line = new createjs.Shape();
                    line.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS, yS - 3)
                        .lineTo(xF + 24, yF - 3);
                    var line2 = new createjs.Shape();
                    line2.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS, yS + 3)
                        .lineTo(xF + 24, yF + 3);
                } else if (direction === arborescence.Direction.EAST)
                {
                    if (isSubTreeLink)
                    {
                        xF = relation.drawing.position.xF - arborescence.NODEWIDTH/2 || 100;
                    }

                    var lineStart = new createjs.Shape();
                    lineStart.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF - 15, yF - 3)
                        .lineTo(xF - 24, yF - 3);
                    var lineStart2 = new createjs.Shape();
                    lineStart2.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF - 15, yF + 3)
                        .lineTo(xF - 24, yF + 3);

                    var line = new createjs.Shape();
                    line.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS, yS - 3)
                        .lineTo(xF - 24, yF - 3);
                    var line2 = new createjs.Shape();
                    line2.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS, yS + 3)
                        .lineTo(xF - 24, yF + 3);
                }

                relationDrawing.addChild(lineStart);
                relationDrawing.addChild(lineStart2);

                relationDrawing.addChild(line);
                relationDrawing.addChild(line2);
                break;

            case arborescence.RelationType.ENONCIATION:
                if (direction === arborescence.Direction.WEST)
                {
                    if (isSubTreeLink)
                    {
                        xS = relation.drawing.position.xS - arborescence.NODEWIDTH/2 || 100;
                        xF = relation.drawing.position.xF + arborescence.NODEWIDTH/2 || 100;
                    }

                    var lineStart = new createjs.Shape();
                    lineStart.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF + 15, yF - 3)
                        .lineTo(xF + 24, yF - 3);
                    var lineStart2 = new createjs.Shape();
                    lineStart2.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF + 15, yF)
                        .lineTo(xF + 24, yF);
                    var lineStart3 = new createjs.Shape();
                    lineStart3.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF + 15, yF + 3)
                        .lineTo(xF + 24, yF + 3);


                    var line = new createjs.Shape();
                    line.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS, yS - 3)
                        .lineTo(xF + 24, yF - 3);
                    var line2 = new createjs.Shape();
                    line2.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS, yS)
                        .lineTo(xF + 24, yF);
                    var line3 = new createjs.Shape();
                    line3.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS, yS + 3)
                        .lineTo(xF + 24, yF + 3);
                } else if (direction === arborescence.Direction.EAST)
                {
                    if (isSubTreeLink)
                    {
                        xF = relation.drawing.position.xF - arborescence.NODEWIDTH/2 || 100;
                    }

                    var lineStart = new createjs.Shape();
                    lineStart.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF - 15, yF - 3)
                        .lineTo(xF - 24, yF - 3);
                    var lineStart2 = new createjs.Shape();
                    lineStart2.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF - 15, yF)
                        .lineTo(xF - 24, yF);
                    var lineStart3 = new createjs.Shape();
                    lineStart3.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF - 15, yF + 3)
                        .lineTo(xF - 24, yF + 3);


                    var line = new createjs.Shape();
                    line.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS, yS - 3)
                        .lineTo(xF - 24, yF - 3);
                    var line2 = new createjs.Shape();
                    line2.graphics
                            .setStrokeStyle(1).beginStroke(color)
                            .moveTo(xS, yS)
                            .lineTo(xF - 24, yF);
                    var line3 = new createjs.Shape();
                    line3.graphics
                            .setStrokeStyle(1).beginStroke(color)
                            .moveTo(xS, yS + 3)
                            .lineTo(xF - 24, yF + 3);
                }

                relationDrawing.addChild(lineStart);
                relationDrawing.addChild(lineStart2);
                relationDrawing.addChild(lineStart3);

                relationDrawing.addChild(line);
                relationDrawing.addChild(line2);
                relationDrawing.addChild(line3);
                break;

            case arborescence.RelationType.ATTENTE:

                if (direction === arborescence.Direction.WEST)
                {
                    if (isSubTreeLink)
                    {
                        xS = relation.drawing.position.xS - arborescence.NODEWIDTH/2 || 100;
                        xF = relation.drawing.position.xF + arborescence.NODEWIDTH/2 || 100;
                    }

                    if (isAttenteLink)
                    {
                        var xS          = relation.drawing.position.xS + arborescence.NODEWIDTH/2 || 0;
                        var xF          = relation.drawing.position.xF || 0;

                        // Relation
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xS + 15, yS + arborescence.FONTSIZE, xF - arborescence.NODEWIDTH/2, yF + arborescence.FONTSIZE, 5
                        ));
                    }else
                    {
                        // Start line
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xF + 15, yF, xF + 30, yF, 5
                        ));
                        // Relation
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xS, yS, xF + 30, yF, 5
                        ));
                    }
                }else if (direction === arborescence.Direction.EAST)
                {
                    if (isSubTreeLink)
                    {
                        var xF = relation.drawing.position.xF - arborescence.NODEWIDTH/2 || 100;
                    }
                    if (relation.properties.isAttenteLink)
                    {
                        var xS          = relation.drawing.position.xS - arborescence.NODEWIDTH/2 || 0;
                        var xF          = relation.drawing.position.xF || 0;
                        // Relation
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xS, yS + arborescence.FONTSIZE, xF + arborescence.NODEWIDTH - 15, yF + arborescence.FONTSIZE, 5
                        ));
                    }else
                    {
                        // Start line
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xF - 15, yF, xF - 30, yF, 5
                        ));
                        // Relation
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xS, yS, xF - 30, yF, 5
                        ));
                    }
                }
                
                break;

            case arborescence.RelationType.ATTENTESECOND:
                if (direction === arborescence.Direction.WEST)
                {
                    if (isSubTreeLink)
                    {
                        xS = relation.drawing.position.xS - arborescence.NODEWIDTH/2 || 100;
                        xF = relation.drawing.position.xF + arborescence.NODEWIDTH/2 || 100;
                    }

                    if (relation.properties.isAttenteLink)
                    {
                        var xS          = relation.drawing.position.xS + arborescence.NODEWIDTH/2|| 0;
                        var xF          = relation.drawing.position.xF|| 0;

                        // Relation
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xS + 15, yS + arborescence.FONTSIZE - 3, xF - arborescence.NODEWIDTH/2, yF + arborescence.FONTSIZE - 3, 5
                        ));
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xS + 15, yS + arborescence.FONTSIZE + 3, xF - arborescence.NODEWIDTH/2, yF + arborescence.FONTSIZE + 3, 5
                        ));
                    }else
                    {
                        // Start line
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xF + 15, yF - 3, xF + 30, yF - 3, 5
                        ));
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xF + 15, yF + 3, xF + 30, yF + 3, 5
                        ));
                        // Relation
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xS, yS - 3, xF + 30, yF - 3, 5
                        ));
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xS, yS + 3, xF + 30, yF + 3, 5
                        ));
                    }
                }else if (direction === arborescence.Direction.EAST)
                {
                    if (isSubTreeLink)
                    {
                        var xF = relation.drawing.position.xF - arborescence.NODEWIDTH/2 || 100;
                    }

                    if (relation.properties.isAttenteLink)
                    {
                        var xS          = relation.drawing.position.xS - arborescence.NODEWIDTH/2 || 0;
                        var xF          = relation.drawing.position.xF || 0;
                        // Relation
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xS, yS + arborescence.FONTSIZE - 3, xF + arborescence.NODEWIDTH - 15, yF + arborescence.FONTSIZE - 3, 5
                        ));
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xS, yS + arborescence.FONTSIZE + 3, xF + arborescence.NODEWIDTH - 15, yF + arborescence.FONTSIZE + 3, 5
                        ));
                    }else
                    {
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xF - 15, yF - 3, xF - 30, yF - 3, 5
                        ));
                        relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                            xF - 15, yF + 3, xF - 30, yF + 3, 5
                        ));
                        // Relation
                        relationDrawing.addChild(
                            new arborescence.TreeShapes.dashedLine(
                                xS + 6, yS - 3, xF - 30, yF - 3, 5
                            )
                        );
                        relationDrawing.addChild(
                            new arborescence.TreeShapes.dashedLine(
                                xS + 6, yS + 3, xF - 30, yF + 3, 5
                            )
                        );
                    }
                }

                break;

            default:
        }

        if (type !== arborescence.RelationType.LIAISON)
        {
            var polygon = new createjs.Shape();
            polygon.graphics.beginStroke(color);
            polygon.graphics.beginFill(color);

            if (direction === arborescence.Direction.WEST)
            {
                if (relation.properties.isAttenteLink)
                {
                    var ptX1 = xS;
                    var ptY1 = yS + arborescence.FONTSIZE;
                    var ptX2 = ptX1 + 15;
                    var ptY2 = ptY1 - 5;
                    var ptX3 = ptX2;
                    var ptY3 = ptY1 + 5;
                } else
                {
                    var ptX1 = xF;
                    var ptY1 = yF;
                    var ptX2 = ptX1 + 15;
                    var ptY2 = ptY1 - 5;
                    var ptX3 = ptX2;
                    var ptY3 = ptY1 + 5;
                }
            } else if (direction === arborescence.Direction.EAST)
            {   
                if (relation.properties.isAttenteLink)
                {
                    var ptX1 = xF + arborescence.NODEWIDTH/2 + 8;
                    var ptY1 = yF + arborescence.FONTSIZE;
                    var ptX2 = ptX1 + 15;
                    var ptY2 = ptY1 - 5;
                    var ptX3 = ptX2;
                    var ptY3 = ptY1 + 5;
                } else
                {
                    var ptX1 = xF;
                    var ptY1 = yF;
                    var ptX2 = ptX1 - 15;
                    var ptY2 = ptY1 - 5;
                    var ptX3 = ptX2;
                    var ptY3 = ptY1 + 5;
                }
            }
            polygon.graphics.moveTo(ptX1, ptY1)
                .lineTo(ptX2, ptY2)
                .lineTo(ptX3, ptY3)
                .lineTo(ptX1, ptY1);

            relationDrawing.addChild(polygon);
        }
        relationDrawing.set({
            cursor: 'pointer'
        });

        return relationDrawing;
    };
    TreeShapes.relationVer              = function (relation)
    {
        var relationDrawing = new createjs.Container();
        var isSubTreeLink = relation.properties.isSubTreeLink;

        var xS          = relation.drawing.position.xS || 0;
        var yS          = relation.drawing.position.yS || 0;
        var xF          = relation.drawing.position.xF || 100;
        var yF          = relation.drawing.position.yF + arborescence.FONTSIZE|| 100;

        var type        = relation.drawing.type || arborescence.RelationType.LIAISON;
        var color       = relation.drawing.color || arborescence.Colors.BLACK;
        var direction   = relation.drawing.direction;
        switch (type)
        {
            case arborescence.RelationType.LIAISON:
                var line = new createjs.Shape();
                line.graphics.setStrokeStyle(1)
                                .beginStroke(color)
                                .moveTo(xS, yS)
                                .lineTo(xF, yF);

                relationDrawing.addChild(line);
                
                break;
            case arborescence.RelationType.DETERMINATION:
                var lineStart = new createjs.Shape();
                lineStart.graphics.setStrokeStyle(1)
                        .beginStroke(color)
                        .moveTo(xF, yF + 15)
                        .lineTo(xF, yF + 25);

                var line = new createjs.Shape();
                line.graphics.setStrokeStyle(1).beginStroke(color)
                                .moveTo(xS, yS)
                                .lineTo(xF, yF + 25);

                relationDrawing.addChild(lineStart);
                relationDrawing.addChild(line);
                
                break;
            case arborescence.RelationType.PREDICATION:
                var lineStart = new createjs.Shape();
                lineStart.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF - 3, yF + 15)
                        .lineTo(xF - 3, yF + 25);
                var lineStart2 = new createjs.Shape();
                lineStart2.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF + 3, yF + 15)
                        .lineTo(xF + 3, yF + 25);

                var line = new createjs.Shape();
                line.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS - 3, yS)
                        .lineTo(xF - 3, yF + 25);
                var line2 = new createjs.Shape();
                line2.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS + 3, yS)
                        .lineTo(xF + 3, yF + 25);

                relationDrawing.addChild(lineStart);
                relationDrawing.addChild(lineStart2);

                relationDrawing.addChild(line);
                relationDrawing.addChild(line2);
                
                break;
            case arborescence.RelationType.ENONCIATION:
                var lineStart = new createjs.Shape();
                lineStart.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF - 3, yF + 15)
                        .lineTo(xF - 3, yF + 25);
                var lineStart2 = new createjs.Shape();
                lineStart2.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF, yF + 15)
                        .lineTo(xF, yF + 25);
                var lineStart3 = new createjs.Shape();
                lineStart3.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xF + 3, yF + 15)
                        .lineTo(xF + 3, yF + 25);

                var line = new createjs.Shape();
                line.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS - 3, yS)
                        .lineTo(xF - 3, yF + 25);
                var line2 = new createjs.Shape();
                line2.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS, yS)
                        .lineTo(xF, yF + 25);
                var line3 = new createjs.Shape();
                line3.graphics
                        .setStrokeStyle(1).beginStroke(color)
                        .moveTo(xS + 3, yS)
                        .lineTo(xF + 3, yF + 25);

                relationDrawing.addChild(lineStart);
                relationDrawing.addChild(lineStart2);
                relationDrawing.addChild(lineStart3);

                relationDrawing.addChild(line);
                relationDrawing.addChild(line2);
                relationDrawing.addChild(line3);
                
                break;
            case arborescence.RelationType.ATTENTE:

                var xS          = relation.drawing.position.xF || 0;
                var yS          = relation.drawing.position.yF || 0;
                var xF          = relation.drawing.position.xS || 100;
                var yF          = relation.drawing.position.yS + arborescence.NODEHEIGHT|| 100;
                if( relation.properties.isLeftAttenteLink )
                    xS  -= arborescence.NODEWIDTH/3
                else
                    xS  += arborescence.NODEWIDTH/3;

                if (direction === arborescence.Direction.NORTH)
                {
                    // Start Line
                    relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                        xF, yF, xF, yF + 30, 5
                    ));
                    // Relation
                    relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                        xS, yS, xF, yF + 30, 5
                    ));
                }else if (direction === arborescence.Direction.SOUTH)
                {
                    // WIP
                }
                break;
            case arborescence.RelationType.ATTENTESECOND:
                if (direction === arborescence.Direction.NORTH)
                {
                    // Start Line
                    relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                        xF - 3, yF + 15, xF - 3, yF + 30, 5
                    ));
                    relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                        xF + 3, yF + 15, xF + 3, yF + 30, 5
                    ));

                    // Relation
                    relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                        xS - 3, yS, xF - 3, yF + 30, 5
                    ));
                    relationDrawing.addChild(new arborescence.TreeShapes.dashedLine(
                        xS + 3, yS, xF + 3, yF + 30, 5
                    ));
                }else if (direction === arborescence.Direction.SOUTH)
                {
                    // WIP
                }
                break;
            default:
        }

        if (type !== arborescence.RelationType.LIAISON)
        {
            var polygon = new createjs.Shape();
            polygon.graphics.beginStroke(color);
            polygon.graphics.beginFill(color);

            if (direction === arborescence.Direction.NORTH)
            {
                var ptX1 = xF;
                var ptY1 = yF;
                var ptX2 = ptX1 - 5;
                var ptY2 = ptY1 + 15;
                var ptX3 = ptX1 + 5;
                var ptY3 = ptY2;
            } else if (direction === arborescence.Direction.SOUTH)
            {   
                var ptX1 = xF;
                var ptY1 = yF;
                var ptX2 = ptX1 - 5;
                var ptY2 = ptY1 - 15;
                var ptX3 = ptX1 + 5;
                var ptY3 = ptY2;
            }
            polygon.graphics.moveTo(ptX1, ptY1)
                .lineTo(ptX2, ptY2)
                .lineTo(ptX3, ptY3)
                .lineTo(ptX1, ptY1);

            relationDrawing.addChild(polygon);
        }

        relationDrawing.set({
            cursor: 'pointer'
        });

        return relationDrawing;
    };

    // ====================================================================== //
    // ========== D R A W N . O B J E C T                                     //
    // ====================================================================== //
    TreeShapes.drawnObject              = function (drawnObject)
    {
        var xS              = drawnObject.drawing.position.xS || 0;
        var yS              = drawnObject.drawing.position.yS || 0;
        var xM              = drawnObject.drawing.position.xM || 0;
        var yM              = drawnObject.drawing.position.yM || 0;
        var xF              = drawnObject.drawing.position.xF || 0;
        var yF              = drawnObject.drawing.position.yF + 15 || 0;
        var color           = drawnObject.drawing.color || arborescence.Colors.BLACK;
        var type            = drawnObject.drawing.type;
        var relationType    = drawnObject.drawing.relationType;

        if (xS < xF)
        {
            var xS          = drawnObject.drawing.position.xS;
            var yS          = drawnObject.drawing.position.yS;
        }else
        {
            var xS          = drawnObject.drawing.position.xS - arborescence.NODEWIDTH;
            var yS          = drawnObject.drawing.position.yS;
        }
        
        var arrowDraw   = new createjs.Container();
        switch (Number(type))
        {
            case 1:
                var dashedCurve = new createjs.Shape();
        
                dashedCurve.graphics
                        .setStrokeStyle(1)
                        .beginStroke(color);
                dashedCurve.graphics
                        .moveTo(xS, yS);

                dashedCurve.graphics.quadraticCurveTo(xM, yM, xF, yF + 15);
                
                dashedCurve.set({
                   cursor   : 'pointer' 
                });
                arrowDraw.addChild( dashedCurve );
                break;
            case 2:
                var curve = 
                arrowDraw.addChild(
                    new arborescence.TreeShapes.dashedCurveArrow(xS, yS, xM, yM, xF, yF + 15, 4));
                break;
            default:
                console.log("// Default drawnObject");
        }
        
        var polygon     = new createjs.Shape();
        polygon.graphics.beginStroke(color);
        polygon.graphics.beginFill(color);
        
        var ptX1    = xF;
        var ptY1    = yF;
        var ptX2    = ptX1 - 5;
        var ptY2    = ptY1 + 15;
        var ptX3    = ptX1 + 5;
        var ptY3    = ptY2;
        polygon.graphics.moveTo(ptX1, ptY1)
                .lineTo(ptX2, ptY2)
                .lineTo(ptX3, ptY3)
                .lineTo(ptX1, ptY1);
        arrowDraw.addChild(polygon);
        arrowDraw.set({
            cursor: 'pointer'
        });
        
        return arrowDraw;
    };

    return TreeShapes;

})();
