var arborescence = arborescence || {};

arborescence.Relation = (function()
{
    function Relation()
    {
        this.initialize();
    };
    
    var prototype = Relation.prototype;
    
    prototype.initialize = function()
    {
        this.key                = 0;
        
        // ===== E N C H O R S
        this.start              = null;
        this.end                = null;

        // ===== D R A W I N G S // P R P
        this.drawing = 
        {
            position    : 
            {
                xS  : 0,
                yS  : 0,
                xF  : 0,
                yF  : 0
            },
            color       : arborescence.Colors.BLACK,
            tilt        : arborescence.Tilt.HORIZONTAL,
            type        : arborescence.RelationType.LIAISON,
            direction   : arborescence.Direction.WEST
        };

        // ===== O T H E R S
        this.properties = 
        {
            isDeleted           : false,
            isSubTreeLink       : false,
            isAttenteLink       : false,
            isLeftAttenteLink   : false,
            isRightAttenteLink  : true,
            isVisible           : true
        };
    };
    
    return Relation;
    
})();
