var arborescence = arborescence || {};

arborescence.Note = (function()
{
    function Note()
    {
        this.initialize();
    }
    
    var prototype = Note.prototype;
    
    prototype.initialize = function()
    {

        this.key            = 0;

        // ===== V A L U E
        this.value          = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat lorem in erat fringilla, vitae semper lectus suscipit.";

        // ===== D R A W I N G // S E T T I N G S
        this.drawing = 
        {
            position     : 
            {
                x   : 0,
                y   : 0
            },
            width       : 124,
            fontSize    : arborescence.FONTSIZE,
            background  : '#FCF0AD'
        };

        // ===== O T H E R
        this.properties = 
        {
            isDeleted               : false,
        };
    };
    
    return Note;
})();
