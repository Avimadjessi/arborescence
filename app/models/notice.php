<?php
namespace models;

/**
*
*
* @package arborescence.app.models
* @author Carlos Avimadjessi
*/
class Notice extends \core\model {
	
	public function update($data, $where)
	{
		$this->_db->update(PREFIX."NOTICE", $data, $where);
	}
	public function insert($data)
	{
		$this->_db->insert(PREFIX."NOTICE", $data);

		return $this->_db->lastInsertId('id');
	}

	public function getNoticeListByUser($userId)
	{
		$data = $this->_db->select(
			"SELECT notice.* 
				FROM ".PREFIX."NOTICE as notice 
				WHERE notice.user = :user OR notice.private = :private
				ORDER BY notice.created_at DESC LIMIT 2", 
			array(':user' => $userId, ':private' => false));

		return $data;
	}

	public function getNoticeByUser($userId)
	{
		$data = $this->_db->select(
			"SELECT notice.* 
				FROM ".PREFIX."NOTICE as notice 
				WHERE notice.user = :user OR notice.private = :private
				ORDER BY notice.created_at DESC", 
			array(':user' => $userId, ':private' => false));

		return $data;
	}

}