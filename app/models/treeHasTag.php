<?php
namespace models;

/**
*
*
* @package arborescence.app.models
* @author Carlos Avimadjessi
*/
class TreeHasTag extends \core\model {
	
	public function update($data, $where)
	{
		$this->_db->update(PREFIX."TREEHASTAG", $data, $where);
	}
	public function insert($data)
	{
		$this->_db->insert(PREFIX."TREEHASTAG", $data);
		return $this->_db->lastInsertId('id');
	}

	public function isTreeLinkedTag($tagId, $treeId)
	{
		$data = $this->_db->select("SELECT id FROM ".PREFIX."TREEHASTAG WHERE tag = :tag AND tree = :tree",
			array(	':tag' 		=> $tagId, ':tree' 	=> $treeId)
		);

		return (sizeof($data) > 0);
	}

}