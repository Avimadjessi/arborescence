<?php
namespace models;

/**
* Authentication model 
*
* @package arborescence.app.models
* @author Carlos Avimadjessi
*/
class Auth extends \core\model {

	// ---------- G E T T E R S ------------------------------------------------
	public function getHash($userEmail)
	{
		$data = $this->_db->select("SELECT password FROM ".PREFIX."USER WHERE email = :email",
			array(':email' => $userEmail));

		return $data[0]->password;
	}
	public function getId($userEmail)
	{
		$data = $this->_db->select("SELECT id FROM ".PREFIX."USER WHERE email = :email",
			array(':email' => $userEmail));

		return $data[0]->id;
	}
	public function getUser($userEmail)
	{
		$data = $this->_db->select("SELECT * FROM ".PREFIX."USER WHERE email = :email",
			array(':email' => $userEmail));

		return $data[0];
	}
	public function getUsers()
	{
		$data = $this->_db->select("SELECT * FROM ".PREFIX."USER");

		return $data;
	}
	public function getFullname($userEmail)
	{
		$data = $this->_db->select("SELECT fullname FROM ".PREFIX."USER WHERE email = :email",
			array(':email' => $userEmail));

		return $data[0]->fullname;
	}	
	public function update($data, $where)
	{
		$this->_db->update(PREFIX."USER", $data, $where);
	}
	public function insert($data)
	{
		$this->_db->insert(PREFIX."USER", $data);
	}

	// ---------- S E A R C H --------------------------------------------------
	public function getUsersByNameAndEmailAndTag ($name, $email, $tag)
	{
		$data = $this->_db->select(
			"SELECT user.* 
				FROM ".PREFIX."USER as user 
				JOIN ".PREFIX."USERHASTAG as userHasTag 
					ON user.id = userHasTag.user 
				JOIN ".PREFIX."TAG as tag 
				    ON tag.id = userHasTag.tag 
				WHERE tag.body like '%".$tag."%' AND user.email like '%".$email."%' AND user.fullname like '%".$name."%' 
				ORDER BY user.fullname");

		return $data;
	}
	public function getUsersByNameAndEmail ($name, $email)
	{
		$data = $this->_db->select(
			"SELECT * 
				FROM ".PREFIX."USER 
				WHERE email like '%".$email."%' AND fullname like '%".$name."%' 
				ORDER BY fullname");

		return $data;
	}
	public function getUsersByNameAndTag ($name, $tag)
	{
		$data = $this->_db->select(
			"SELECT user.* 
				FROM ".PREFIX."USER as user 
				JOIN ".PREFIX."USERHASTAG as userHasTag 
					ON user.id = userHasTag.user 
				JOIN ".PREFIX."TAG as tag 
				    ON tag.id = userHasTag.tag 
				WHERE user.fullname like '%".$name."%' AND tag.body like '%".$tag."%' 
				ORDER BY user.fullname");

		return $data;
	}
	public function getUsersByEmailAndTag ($email, $tag)
	{
		$data = $this->_db->select(
			"SELECT user.* 
				FROM ".PREFIX."USER as user 
				JOIN ".PREFIX."USERHASTAG as userHasTag 
					ON user.id = userHasTag.user 
				JOIN ".PREFIX."TAG as tag 
				    ON tag.id = userHasTag.tag 
				WHERE user.email like '%".$email."%' AND tag.body like '%".$tag."%' 
				ORDER BY user.fullname");

		return $data;
	}
	public function getUsersByName ($name)
	{
		$data = $this->_db->select(
			"SELECT *
				FROM ".PREFIX."USER 
				WHERE fullname like '%".$name."%' 
				ORDER BY fullname");

		return $data;
	}
	public function getUsersByEmail ($email)
	{
		$data = $this->_db->select(
			"SELECT * 
				FROM ".PREFIX."USER 
				WHERE email like '%".$email."%' 
				ORDER BY fullname");

		return $data;
	}
	public function getUsersByTag ($tag)
	{
		$data = $this->_db->select(
			"SELECT user.* 
				FROM ".PREFIX."USER as user 
				JOIN ".PREFIX."USERHASTAG as userHasTag 
					ON user.id = userHasTag.user 
				JOIN ".PREFIX."TAG as tag 
				    ON tag.id = userHasTag.tag 
				WHERE tag.body like '%".$tag."%' 
				ORDER BY user.fullname");

		return $data;
	}

	// ---------- C H E C K ----------------------------------------------------
	public function userExists($userId)
	{
		$data = $this->_db->select("SELECT id FROM ".PREFIX."USER WHERE id = :id",
			array(':id' => $userId));

		return (sizeof($data) > 0);
	}
	public function userEmailExists($userEmail)
	{
		$data = $this->_db->select("SELECT id FROM ".PREFIX."USER WHERE email = :email",
			array(':email' => $userEmail));

		return (sizeof($data) > 0);
	}
}