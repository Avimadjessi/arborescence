<?php
namespace models;

/**
*
*
* @package arborescence.app.models
* @author Carlos Avimadjessi
*/
class Tree extends \core\model {

	// ---------- G E T T E R S ------------------------------------------------
	public function getId($userId)
	{
		$data = $this->_db->select("SELECT id FROM ".PREFIX."TREE WHERE user = :user",
			array(':user' => $userId));

		return $data[0]->id;
	}
	public function getTree($treeId)
	{
		$data = $this->_db->select("SELECT * FROM ".PREFIX."TREE WHERE id = :id", 
			array(':id' => $treeId));

		return $data[0];
	}
	public function getFirstTree($userId)
	{
		$data = $this->_db->select("SELECT * FROM ".PREFIX."TREE WHERE user = :user",
			array(':user' => $userId));

		return $data[0];
	}
	public function getBySlug($userId, $slug)
	{
		$data = $this->_db->select("SELECT * FROM ".PREFIX."TREE WHERE user = :user 
			AND slug = :slug", array(':user' => $userId, ':slug' => $slug));

		return $data[0];
	}
	public function getTreeList($userId)
	{
		$data = $this->_db->select(
			"SELECT tree.*, user.fullname, lastUser.fullname as lastUserFullname
				FROM ".PREFIX."TREE as tree 
					JOIN ".PREFIX."USER as user ON tree.user = user.id 
					JOIN ".PREFIX."USER as lastUser ON tree.lastUser = lastUser.id
				WHERE tree.user = :user", 
			array(':user' => $userId));

		return $data;
	}
	public function getTreeListByTag($tagId, $userId)
	{
		$data = $this->_db->select(
			"SELECT tree.*, user.fullname, lastUser.fullname as lastUserFullname
				FROM ".PREFIX."TREE as tree 
					JOIN ".PREFIX."USER as user ON tree.user = user.id 
					JOIN ".PREFIX."USER as lastUser ON tree.lastUser = lastUser.id  
					JOIN ".PREFIX."TREEHASTAG as treeHasTag ON tree.id = treeHasTag.tree 
				WHERE treeHasTag.tag = :tagId AND tree.user = :userId ", 
			array(':tagId' => $tagId, ':userId' => $userId));

		return $data;
	}
	public function update($data, $where)
	{
		$this->_db->update(PREFIX."TREE", $data, $where);
	}
	public function insert($data)
	{
		try 
		{
			$result = $this->_db->insert(PREFIX."TREE", $data);
			return true;
		} catch(Exception $e) 
		{
			return false;
		}
	}
	public function delete($data)
	{	
		$tree = $this->getTree($data['id']);
		if ($tree)
		{
			// Delete linked tags to tree
			$tags 			= $this->_db->select(
				"SELECT tag FROM ".PREFIX."TREEHASTAG WHERE tree = :treeId",
				array(':treeId' => $data['id'] )
			);
			$hasTagData 	= array('tree' => $data['id']);
			$this->_db->delete(PREFIX."TREEHASTAG", $hasTagData, "NO_LIMIT");
			foreach ($tags as $tag) 
			{
				$linkCount 	= $this->_db->select (
					"SELECT count(*) as links  
						FROM ".PREFIX."TREEHASTAG as treeHasTag 
							JOIN ".PREFIX."USERHASTAG as userHasTag 
								ON userHasTag.tag = treeHasTag.tag 
							JOIN ".PREFIX."USER as user 
								ON user.id = userHasTag.user 
						WHERE user.id = :user AND treeHasTag.tag = :tagId",
					array (':user' => $tree->user,':tagId' => $tag->tag)
				);

				if ($linkCount[0]->links == 0)
				{
					// Delete linked tags to user
					$ownTagData 	= array('user' => $tree->user, 'tag' => $tag->tag);
					$this->_db->delete(PREFIX."USERHASTAG", $ownTagData);
				}
			}

			// Delete tree
			if ($tree->exercise != null)
			{
				$this->_db->delete(PREFIX."EXERCISESOLUTION", array('tree' => $tree->id, 'exercise' => $tree->exercise));
			}

			$this->_db->delete(PREFIX."TREE", $data);

			$newData = array(
				'oldId' 			=> $tree->id,
				'jsonNodes'			=> $tree->jsonNodes,
				'jsonRelations'		=> $tree->jsonRelations,
				'jsonDrawnObjects'	=> $tree->jsonDrawnObjects,
				'jsonNotes'			=> $tree->jsonNotes,
				'user'				=> $tree->user,
				'lastUser'			=> $tree->lastUser,
				'name'				=> $tree->name,
				'message'			=> $tree->message,
				'exercise'			=> $tree->exercise,
				'slug'				=> $tree->slug,
				'version'			=> $tree->version,
				'created_at'		=> $tree->created_at,
				'updated_at'		=> $tree->updated_at
			);
			$this->_db->insert(PREFIX."TREE_ARCHIVE", $newData);
		}
	}

	// ---------- S E A R C H . // ---------------------------------------------
	public function getTrees ()
	{
		$data = $this->_db->select("SELECT * FROM ".PREFIX."TREE");

		return $data;
	}
	public function getTreesByNameAndEmailAndTag ($name, $email, $tag)
	{
		$data = $this->_db->select(
			"SELECT tree.*, user.fullname , lastUser.fullname as lastUserFullname 
				FROM ".PREFIX."TREE as tree 
					JOIN ".PREFIX."USER as user 
						ON user.id = tree.user 
					JOIN ".PREFIX."USER as lastUser 
						ON tree.lastUser = lastUser.id 
					JOIN ".PREFIX."USERHASTAG as userHasTag 
						ON userHasTag.user = user.id 
					JOIN ".PREFIX."TAG as tag 
						ON tag.id = userHasTag.tag 
					WHERE tag.body like '%".$tag."%' AND user.email like '%".$email."%' AND tree.name like '%".$name."%' 
				ORDER BY tree.name");

		return $data;
	}
	public function getTreesByNameAndEmail ($name, $email)
	{
		$data = $this->_db->select(
			"SELECT tree.*, user.fullname , lastUser.fullname as lastUserFullname 
				FROM ".PREFIX."TREE as tree 
					JOIN ".PREFIX."USER as user 
						ON user.id = tree.user 
					JOIN ".PREFIX."USER as lastUser 
						ON tree.lastUser = lastUser.id
					WHERE user.email like '%".$email."%' AND tree.name like '%".$name."%' 
				ORDER BY tree.name");

		return $data;
	}
	public function getTreesByEmailAndTag ($email, $tag)
	{
		$data = $this->_db->select(
			"SELECT tree.*, user.fullname , lastUser.fullname as lastUserFullname 
				FROM ".PREFIX."TREE as tree 
					JOIN ".PREFIX."USER as user 
						ON user.id = tree.user 
					JOIN ".PREFIX."USER as lastUser 
						ON tree.lastUser = lastUser.id
					JOIN ".PREFIX."USERHASTAG as userHasTag 
						ON userHasTag.user = user.id 
					JOIN ".PREFIX."TAG as tag 
						ON tag.id = userHasTag.tag 
					WHERE tag.body like '%".$tag."%' AND user.email like '%".$email."%'  
				ORDER BY tree.name");

		return $data;
	}
	public function getTreesByNameAndTag ($name, $tag)
	{
		$data = $this->_db->select(
			"SELECT tree.*, user.fullname , lastUser.fullname as lastUserFullname 
				FROM ".PREFIX."TREE as tree 
					JOIN ".PREFIX."USER as user 
						ON user.id = tree.user 
					JOIN ".PREFIX."USER as lastUser 
						ON tree.lastUser = lastUser.id
					JOIN ".PREFIX."USERHASTAG as userHasTag 
						ON userHasTag.user = user.id 
					JOIN ".PREFIX."TAG as tag 
						ON tag.id = userHasTag.tag 
					WHERE tag.body like '%".$tag."%' AND tree.name like '%".$name."%' 
				ORDER BY tree.name");

		return $data;
	}
	public function getTreesByName ($name)
	{
		$data = $this->_db->select(
			"SELECT tree.*, user.fullname , lastUser.fullname as lastUserFullname 
				FROM ".PREFIX."TREE as tree 
					JOIN ".PREFIX."USER as user 
						ON user.id = tree.user 
					JOIN ".PREFIX."USER as lastUser 
						ON tree.lastUser = lastUser.id
					WHERE tree.name like '%".$name."%' 
				ORDER BY tree.name");

		return $data;
	}
	public function getTreesByEmail ($email)
	{
		$data = $this->_db->select(
			"SELECT tree.*, user.fullname , lastUser.fullname as lastUserFullname 
				FROM ".PREFIX."TREE as tree 
					JOIN ".PREFIX."USER as user 
						ON user.id = tree.user 
					JOIN ".PREFIX."USER as lastUser 
						ON tree.lastUser = lastUser.id 
					WHERE user.email like '%".$email."%'  
				ORDER BY tree.name");

		return $data;
	}
	public function getTreesByTag ($tag)
	{
		$data = $this->_db->select(
			"SELECT tree.*, user.fullname , lastUser.fullname as lastUserFullname 
				FROM ".PREFIX."TREE as tree 
					JOIN ".PREFIX."USER as user 
						ON user.id = tree.user 
					JOIN ".PREFIX."USERHASTAG as userHasTag 
						ON userHasTag.user = user.id 
					JOIN ".PREFIX."TAG as tag 
						ON tag.id = userHasTag.tag 
					WHERE tag.body like '%".$tag."%'  
				ORDER BY tree.name");

		return $data;
	}

	// ---------- C H E C K ----------------------------------------------------
	public function treeExists($userId, $slug)
	{
		$data = $this->_db->select("SELECT id FROM ".PREFIX."TREE WHERE user = :user AND slug = :slug",
			array(':user' => $userId, ':slug' => $slug));

		return (sizeof($data) > 0);
	}

}
