<?php
namespace models;

/**
*
*
* @package arborescence.app.models
* @author Carlos Avimadjessi
*/
class Tag extends \core\model {
	
	public function update($data, $where)
	{
		$this->_db->update(PREFIX."TAG", $data, $where);
	}
	public function insert($data)
	{
		$this->_db->insert(PREFIX."TAG", $data);

		return $this->_db->lastInsertId('id');
	}
	public function delete($data)
	{	
		$this->_db->delete(PREFIX."TREEHASTAG", $data);
	}
	public function getByBody($body)
	{
		$data = $this->_db->select("SELECT * FROM ".PREFIX."TAG WHERE body = :body", 
			array(':body' => $body));

		return $data[0];
	}
	public function getBySlug($slug)
	{
		$data = $this->_db->select("SELECT * FROM ".PREFIX."TAG WHERE slug = :slug", 
			array(':slug' => $slug));

		return $data[0];
	}

	public function getTagListByTree($treeId)
	{
		$data = $this->_db->select(
			"SELECT tag.* 
				FROM ".PREFIX."TAG as tag  
					JOIN ".PREFIX."TREEHASTAG as treeHasTag ON treeHasTag.tag = tag.id 
				WHERE treeHasTag.tree = :tree", 
			array(':tree' => $treeId));

		return $data;
	}

	public function getTagListByUser($userId)
	{
		$data = $this->_db->select(
			"SELECT tag.* 
				FROM ".PREFIX."TAG as tag  
					JOIN ".PREFIX."USERHASTAG as userHasTag ON userHasTag.tag = tag.id 
				WHERE userHasTag.user = :user", 
			array(':user' => $userId));

		return $data;
	}

	public function tagExists($slug)
	{
		$data = $this->_db->select("SELECT id FROM ".PREFIX."TAG WHERE slug = :slug",
			array(':slug' => $slug));

		return (sizeof($data) > 0);
	}

}