<?php
namespace models;

/**
*
*
* @package arborescence.app.models
* @author Carlos Avimadjessi
*/
class UserHasTag extends \core\model {
	
	public function update($data, $where)
	{
		$this->_db->update(PREFIX."USERHASTAG", $data, $where);
	}
	public function insert($data)
	{
		$this->_db->insert(PREFIX."USERHASTAG", $data);
		return $this->_db->lastInsertId('id');
	}

	public function isUserLinkedTag($tagId, $userId)
	{
		$data = $this->_db->select("SELECT id FROM ".PREFIX."USERHASTAG WHERE tag = :tag AND user = :user",
			array(':tag' => $tagId, ':user' => $userId));

		return (sizeof($data) > 0);
	}

}