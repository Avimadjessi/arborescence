<?php
namespace models;

/**
* Exercise Model
*
* @package arborescence.app.models
* @author Carlos Avimadjessi
*/
class Exercise extends \core\model {

	// ---------- G E T T E R S ------------------------------------------------
	public function getExerciseId($slug)
	{
		$data = $this->_db->select("SELECT id FROM ".PREFIX."EXERCISE WHERE slug = :slug",
			array(':slug' => $slug));

		return $data[0]->id;
	}
	public function getExercise($exerciseId)
	{
		$data = $this->_db->select("SELECT * FROM ".PREFIX."EXERCISE WHERE id = :id", 
			array(':id' => $exerciseId));

		return $data[0];
	}
	public function getExBySlug($userId, $slug)
	{
		$data = $this->_db->select(
			"SELECT ex.*
			FROM ".PREFIX."EXERCISE as ex 
				JOIN ".PREFIX."USERHASEXERCISE as uhe ON uhe.user = :userId 
			WHERE ex.slug = :slug", 
		array(':userId' => $userId, ':slug' => $slug));

		return $data[0];
	}
	public function getExerciseBySlug($userLevel, $slug)
	{
		$data = $this->_db->select(
			"SELECT ex.*
			FROM ".PREFIX."EXERCISE as ex 
				JOIN ".PREFIX."USER as user ON ex.user = user.id 
			WHERE user.level <= :level AND slug = :slug", 
		array(':level' => $userLevel, ':slug' => $slug));

		return $data[0];
	}
	public function getExerciseList($userId)
	{
		$data = $this->_db->select(
			"SELECT ex.*
				FROM ".PREFIX."EXERCISE as ex 
					JOIN ".PREFIX."USERHASEXERCISE as uhe ON ex.id = uhe.exercise
				WHERE uhe.user = :user", 
			array(':user' => $userId));

		return $data;
	}
	public function updateExercise($data, $where)
	{
		$this->_db->update(PREFIX."EXERCISE", $data, $where);
	}
	public function insertExercise($data)
	{
		try 
		{
			$result = $this->_db->insert(PREFIX."EXERCISE", $data);
			return true;
		} catch(Exception $e) 
		{
			return false;
		}
	}
	public function insertSolution($data)
	{
		try
		{
			$result = $this->_db->insert(PREFIX."EXERCISESOLUTION", $data);
			return true;
		} catch (Exception $e)
		{
			return false;
		}
	}
	public function insertUserHasExercise($data)
	{
		try
		{
			$result = $this->_db->insert(PREFIX."USERHASEXERCISE", $data);
			return true;
		} catch(Exception $e) 
		{
			return false;
		}
	}
	public function deleteExercise($data)
	{	
		if($data['slug'])
		{
			$sql = 
				"DELETE FROM ".PREFIX."USERHASEXERCISE 
					WHERE exercise IN (SELECT e.id 
											FROM ".PREFIX."EXERCISE as e 
											WHERE e.slug = :slug)";
			$statement 		= $this->_db->prepare($sql);
			$statement->bindValue ("slug", $data['slug']);
			
			$statement->execute();

			// Delete exercise
			$this->_db->delete(PREFIX."EXERCISE", $data);
		}
	}

	// ---------- C H E C K ----------------------------------------------------
	public function exerciseExists($userId, $slug)
	{
		$data = $this->_db->select("SELECT id FROM ".PREFIX."EXERCISE WHERE user = :user AND slug = :slug",
			array(':user' => $userId, ':slug' => $slug));

		return (sizeof($data) > 0);
	}
	public function userhasexerciseExists($userId, $exerciseId)
	{
		$data = $this->_db->select("SELECT id FROM ".PREFIX."USERHASEXERCISE WHERE user = :user AND exercise = :exercise",
			array(':user' => $userId, ':exercise' => $exerciseId));

		return (sizeof($data) > 0);
	}
	
}
