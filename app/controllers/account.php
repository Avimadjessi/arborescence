<?php
namespace controllers;
use \core\view,
	\helpers\session,
	\helpers\url,
	\helpers\password,
	\models\auth,
	\models\tree;

/**
* Account controller
* 
* Used to manage profile display and modification routes
*
* @package arborescence.app.controllers
* @author Carlos Avimadjessi
*/
class Account extends \core\controller
{
	private $_auth;
	private $_tree;
	private $_tag;
	private $_treeHasTag;
	private $_userHasTag;
	private $_notice;

	/**
	* Constructor
	* 
	* Initialize used models and set session parameters 
	*/
	public function __construct()
	{
		if(!Session::get('loggedin'))
		{
			Url::redirect('login');
		}

		$this->_auth = new auth();
		$this->_tree = new tree();

		$this->_tag 		= new \models\tag();
		$this->_treeHasTag	= new \models\treeHasTag();
		$this->_userHasTag 	= new \models\userHasTag();

		$this->_notice 	= new \models\notice();

		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);

		$userTags 		= $this->_tag->getTagListByUser($user->id);
		$userNotices 	= $this->_notice->getNoticeListByUser($user->id);

		Session::set('userTags', $userTags);
		Session::set('userNotices', $userNotices);

		$allNotices 	= $this->_notice->getNoticeByUser($user->id);
		Session::set('allNotices', $allNotices);
	}

	/**
	* Render user profile page
	*/
	public function profile()
	{
		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);
		if(!$user)
		{
			Url::redirect();
		}
		$data['title']			= 'Compte';
		$data['sub-title'] 		= $user->fullname;

		$data['user']			= $user;	

		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('account/profile', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

	/**
	* Render user profile edit page and updade profile  
	*/
	public function edit()
	{
		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);
		if ($user)
		{
			if ($user->level == 1)
				Url::redirect('profile');
		} else
		{
			Url::redirect();
		}

		if(isset($_POST['submit']))
		{
			$fullname 			= htmlspecialchars($_POST['userFullname']);
			$occupation 		= htmlspecialchars($_POST['userOccupation']);
			$password 			= $_POST['userPassword'];

			$data = array(
				'fullname' 		=> $fullname,
				'occupation'	=> $occupation
			);

			$where = array('id' 		=> $user->id);

			if (!$password)
			{
				$this->_auth->update($data, $where);
				$user 					= $this->_auth->getUser($userEmail);
				Session::set('loggedUser', [
					'id'		=> $user->id,
					'email'		=> $user->email,
					'fullname'	=> $user->fullname,
					'occupation'=> $user->occupation,
					'level' 	=> $user->level
				]);

				$data['success'][] 		= 'Modification(s) sauvegargée(s)';
			} else 
			{
				$newPassword 			= $_POST['userNewPassword'];
				$newPasswordConfirm 	= $_POST['userNewPasswordConfirm']; 

				if ( $newPassword == $newPasswordConfirm )
				{
					if ( Password::verify($password, $user->password) )
					{
						$passHash 			= Password::Make($newPassword);
						$data['password'] 	= $passHash;

						$this->_auth->update($data, $where);
						$user 				= $this->_auth->getUser($userEmail);
						Session::set('loggedUser', [
							'id'		=> $user->id,
							'email'		=> $user->email,
							'fullname'	=> $user->fullname,
							'level' 	=> $user->level
						]);


						$data['success'][] 	= 'Modification(s) sauvegargée(s)';
						$data['success'][] 	= 'Mot de passe modifié!';
					} else 
					{
						$data['failure'][] = 'Mot de passe incorrect.';
					}
				}else
				{
					$data['failure'][] = 'Le mot de passe et le mot de passe de confirmation ne correspondent pas.';
				}
			}
			
		}

		$data['title']			= 'Compte';
		$data['sub-title'] 		= $user->fullname;

		$data['user']			= $user;
		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('account/edit', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

	/**
	* Render user profile's edit image page and update profile image 
	*/
	public function editImage()
	{
		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);
		if ($user)
		{
			if ($user->level == 1)
				Url::redirect('profile');
		} else
		{
			Url::redirect();
		}

		if(isset($_FILES['profile-image']))
		{

			$image 				= $_FILES['profile-image'];
			$image_name 		= 'app/templates/default/images/profile/_profile_'.$user->email.'.png';

			if ($image)
			{
				if (move_uploaded_file($_FILES['profile-image']['tmp_name'], $image_name))
				{
					$data = array(
						'image' 		=> $image_name
					);
					$data['success'][] 		= 'Image de profil correctement sauvegargée!';
				} else
				{
					$data['failure'][] = 'Echec de l\'upload, veillez rééssayer.';
				}
			}
		}

		$data['title']			= 'Compte';
		$data['sub-title'] 		= $user->fullname;

		$data['user']			= $user;
		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('account/editImage', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

	/**
	* Render user delete account page and delete user account
	*/
	public function delete()
	{
		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);
		if ($user)
		{
			if ($user->level == 1)
				Url::redirect('profile');
		} else
		{
			Url::redirect();
		}

		if(isset($_POST['submit']))
		{
			$data = array('active' 		=> false);
			$where = array('id' 		=> $this->_auth->getId($userEmail));
			$this->_auth->update($data, $where);

			$user 				= $this->_auth->getUser($userEmail);
			Url::redirect('logout');
		}

		$data['title']			= 'Compte';
		$data['sub-title'] 		= $user->fullname;

		$data['user']			= $user;
		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('account/delete', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

}