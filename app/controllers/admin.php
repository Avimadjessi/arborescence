<?php
namespace controllers;
use \core\view,
	\helpers\session,
	\helpers\url,
	\helpers\password,
	\helpers\helper,
	\models\auth,
	\models\exercise,
	\models\tree;

/**
* Administrator role controller
* 
* @package arborescence.app.controllers
* @author Carlos Avimadjessi
*/
class Admin extends \core\controller
{
	private $_auth;
	private $_tree;
	private $_exercise;
	private $_tag;
	private $_treeHasTag;
	private $_userHasTag;
	private $_notice;

	/**
	* Constructor
	* 
	* Initialize used models and set session parameters 
	*/
	public function __construct()
	{
		if (Session::get('loggedin'))
			if (Session::get('loggedUser')['level'] === '9')
				;
			else
				Url::redirect('login');
		else
			Url::redirect('login');

		$this->_auth 			= new auth();
		$this->_exercise 		= new exercise();

		$this->_tree 			= new tree();

		$this->_tag 			= new \models\tag();
		$this->_treeHasTag		= new \models\treeHasTag();
		$this->_userHasTag 		= new \models\userHasTag();

		$this->_notice 			= new \models\notice();

		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);

		$userTags 				= $this->_tag->getTagListByUser($user->id);
		$userNotices 			= $this->_notice->getNoticeListByUser($user->id);

		Session::set('userTags', $userTags);
		Session::set('userNotices', $userNotices);

		$allNotices 			= $this->_notice->getNoticeByUser($user->id);
		Session::set('allNotices', $allNotices);
	}

	/**
	* Render admin information page
	*/
	public function informations()
	{
		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);
		if(!$user)
		{
			Url::redirect();
		}
		$data['title']			= 'Admin Info';
		$data['sub-title'] 		= $user->fullname;

		$data['user']			= $user;	

		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('admin/about', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

	/**
	* Render notice creation page and create notice
	*/
	public function notice()
	{
		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);

		if(isset($_POST['submit']))
		{
			$title 			= htmlspecialchars($_POST['noticeTitle']);
			$body 			= htmlspecialchars($_POST['noticeBody']);

			$data = array(
				'title' 		=> $title,
				'body'			=> $body,
				'private' 		=> false,
				'created_at' 	=> date('Y-m-d G:i:s'),
				'user' 			=> $user->id
			);

			$this->_notice->insert($data);

			$data['success'][] 	= 'Annonce publiée!';

			unset($_POST['submit']);
		}

		$data['title']			= 'Notification';
		$data['sub-title'] 		= $user->fullname;

		$data['user']			= $user;
		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('admin/notice', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}
	
	/**
	* Used to load a tree on the canvas with a given tree slug
	*
	* @param string $slug an unique tree value
	*/
	public function load ($slug)
	{
		if (isset($_POST['save']))
		{
			$this->canvas();
		} else
		{
			$userEmail					= Session::get('loggedUser')['email'];
			$user 						= $this->_auth->getUser($userEmail);
			if(!$user)
			{
				Url::redirect('logout');
			}
			$data['exercise']			= $this->_exercise->getExerciseBySlug($user->level, $slug);
			$data['treeLoaded']			= true;

			$data['title']				= 'Charger';
			$data['sub-title'] 			= Session::get('loggedUser')['fullname'];

			$data['user']				= $user;	

			$data['treeTags']			= $this->_tag->getTagListByTree($data['exercise']->id);

			
			View::rendertemplate('header', $data);
			View::rendertemplate('headerMark', $data);
			View::rendertemplate('sidemenu', $data);
			View::render('admin/canvas', $data);
			View::rendertemplate('footerMark', $data);
			View::rendertemplate('footer', $data);
		}
	}

	/**
	* Render a new exercise in the canvas
	* 
	* Creates a new tree with default values and load it into the canvas.
	* The new tree is unnamed and unsaved.
	*/
	public function canvas ()
	{
		if (isset($_POST['save']))
		{
			$name 				= htmlspecialchars($_POST['name']);
			$description 		= htmlspecialchars($_POST['description']);
			$jsonNodes			= $_POST['jsonNodes'];
			$jsonRelations		= $_POST['jsonRelations'];
			$jsonDrawnObjects	= $_POST['jsonDrawnObjects'];
			$jsonNotes 			= $_POST['jsonNotes'];
			$email				= Session::get('loggedUser')['email'];
			$user 				= $this->_auth->getUser($email);
			$userId				= $user->id;
			$slug 				= Helper::slugify($name);

			
			// ========== V A L I D A T I O N ==================================
			if ($this->_exercise->exerciseExists($userId, $slug))
			{
				// Update
				$data = array(
					'jsonNodes'			=> $jsonNodes,
					'jsonRelations'		=> $jsonRelations,
					'jsonDrawnObjects'	=> $jsonDrawnObjects,
					'jsonNotes' 		=> $jsonNotes,
					'description' 		=> $description
				);
				$exercise 	= $this->_exercise->getExerciseBySlug($user->level, $slug);
				$where 		= array('id' => $exercise->id);
				$this->_exercise->updateExercise($data, $where);

				$data['title']			= 'Dessin';
				$data['sub-title'] 		= $data['name'];
				$data['exercise']		= $exercise;
			} else
			{
				// Insert
				$data = array(
					'jsonNodes'			=> $jsonNodes,
					'jsonRelations'		=> $jsonRelations,
					'jsonDrawnObjects'	=> $jsonDrawnObjects,
					'jsonNotes' 		=> $jsonNotes,
					'user'				=> $userId,
					'name'				=> $name,
					'description'		=> $description,
					'slug'				=> $slug,
					'created_at'		=> date('Y-m-d G:i:s')
				);
				$this->_exercise->insertExercise($data);
				$exerciseId = $this->_exercise->getExerciseId($slug);
				$dataUHE = array(
					'exercise' 			=> $exerciseId,
					'user' 				=> $userId,
					'created_at'		=> date('Y-m-d G:i:s')
				);
				$this->_exercise->insertUserHasExercise($dataUHE);

				// Notice
				$dataNotice = array(
					'user' 		=> $userId,
					'title' 	=> 'Exercice créé!',
					'body' 		=> '"'.$name.'"',
					'created_at'=> date('Y-m-d G:i:s')
				);
				$this->_notice->insert($dataNotice);

				Session::set('treeName', $data['name']);

				$data['title']				= 'Dessin';
				$data['sub-title'] 			= $data['name'];
				$data['exercise']			= $this->_exercise->getExerciseBySlug($user->level, $slug);
			}

			unset($_POST['save']);
		} else
		{
			$userEmail 					= Session::get('loggedUser')['email'];
			$user 						= $this->_auth->getUser($userEmail);
			if(!$user)
			{
				Url::redirect('logout');
			}
			$data['title']				= 'Exercice';
			$data['sub-title'] 			= Session::get('loggedUser')['fullname'];
			
			$data['user']				= $user;	

			View::rendertemplate('header', $data);
			View::rendertemplate('headerMark', $data);
			View::rendertemplate('sidemenu', $data);
			View::render('admin/canvas', $data);
			View::rendertemplate('footerMark', $data);
			View::rendertemplate('footer', $data);
		}
	}

	/**
	* Used to delete an exercise
	* 
	* @param string $slug an unique exercise value
	*/
	public function delete ($slug)
	{
		$data['title']			= 'Delete';
		$data['sub-title'] 		= Session::get('loggedUser')['fullname'];

		$email					= Session::get('loggedUser')['email'];
		$userId					= $this->_auth->getId($email);

		if($this->_exercise->exerciseExists($userId, $slug))
		{
			$value 				= array(
				'slug'	=> $slug
			);
			$this->_exercise->deleteExercise($value);

			// Notice
			$dataNotice = array(
				'user' 		=> $userId,
				'title' 	=> 'Exercice supprimé!',
				'type' 		=> 'EXERCISE_DELETED',
				'body' 		=> '"'.$slug.'"',
				'created_at'=> date('Y-m-d G:i:s')
			);
			$this->_notice->insert($dataNotice);

			Url::redirect('trees');
		}	 
	}

	/**
	* Used to share an exercise with users
	* 
	* @return a success message or failures
	*/
	public function share ()
	{
		$exerciseId 		= $_POST['exercise'];
		$users 				= $_POST['users'];

		$userEmail			= Session::get('loggedUser')['email'];

		$admin				= $this->_auth->getUser($userEmail);

		$returnMessage 		= '';
		if ($admin)
		{
			$exercise 			= $this->_exercise->getExercise($exerciseId);

			if($exercise)
			{
				foreach ($users as $user) 
				{
					// User has the exercise ?
					if ($this->_exercise->userhasexerciseExists($user, $exerciseId))
					{
						$returnMessage .= 'Id '.$user.' a déjà cet exercice.<br/>';
					} else 
					{
						$dataUHE = array(
							'exercise' 			=> $exerciseId,
							'user' 				=> $user,
							'created_at'		=> date('Y-m-d G:i:s')
						);
						$this->_exercise->insertUserHasExercise($dataUHE);
					}
				}
				$returnMessage .= '<label class="label label-info">Terminé!</label>';
			}
		}
		echo $returnMessage;	
	}

	// ----------  Search ------------------------------------------------------
	/**
	* Render search page. Used to search users and trees
	*/
	public function search ()
	{
		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);
		
		if (isset($_POST['submit']))
		{
			if ($_POST['submit'] == 'searchUser')
			{
				$nameUser	= htmlspecialchars($_POST['username']);
				$emailUser 	= htmlspecialchars($_POST['useremail']);
				$tagUser 	= trim($_POST['usertag']);

				$searchCriteria = null;
				if ( ($nameUser != null && $nameUser != '') 
					&& ($emailUser != null && $emailUser != '')
					&& ($tagUser != null && $tagUser != '') )
				{
					// search by name AND email AND tag
					$searchCriteria 		= "Nom : \"".$nameUser."\" // E-mail : \"".$emailUser."\" // Tag : \"".$tagUser."\"";
					$data['userList']		= $this->_auth->getUsersByNameAndEmailAndTag($nameUser, $emailUser, $tagUser);

				} else if (($nameUser != null && $nameUser != '') && ($emailUser != null && $emailUser != ''))
				{
					// search by name AND email
					$searchCriteria 		= "Nom : \"".$nameUser."\" // E-mail : \"".$emailUser."\"";
					$data['userList']		= $this->_auth->getUsersByNameAndEmail($nameUser, $emailUser);
				} else if (($nameUser != null && $nameUser != '') && ($tagUser != null && $tagUser != ''))
				{
					// search by name AND tag
					$searchCriteria 		= "Nom : \"".$nameUser."\" // Tag : \"".$tagUser."\"";
					$data['userList']		= $this->_auth->getUsersByNameAndTag($nameUser, $tagUser);
				} else if (($tagUser != null && $tagUser != '') && ($emailUser != null && $emailUser != ''))
				{
					// search by email AND tag
					$searchCriteria 		= "E-mail : \"".$emailUser."\" // Tag : \"".$tagUser."\"";
					$data['userList']		= $this->_auth->getUsersByEmailAndTag($emailUser, $tagUser);
				}else if ($nameUser != null && $nameUser != '') 
				{
					// search by name
					$searchCriteria 		= "Nom : \"".$nameUser."\"";
					$data['userList'] 		= $this->_auth->getUsersByName($nameUser);
				} else if ($emailUser != null && $emailUser != '') {
					// search by email
					$searchCriteria 		= "E-mail : \"".$emailUser."\"";
					$data['userList'] 		=	$this->_auth->getUsersByEmail($emailUser);
				} else if ($tagUser != null && $tagUser != '') {
					// search by tag
					$searchCriteria 		= "Tag : \"".$tagUser."\"";
					$data['userList'] 		= $this->_auth->getUsersByTag($tagUser);
				} else 
				{
					$searchCriteria 		= "Tous les utilisateurs.";
					$data['userList'] 		= $this->_auth->getUsers();
				}
				
				if ($searchCriteria != null)
					$data['searchCriteria'] 		= $searchCriteria;
				
				unset($_POST['submit']);

				unset($_POST['useremail']);
				unset($_POST['username']);
				unset($_POST['usertag']);

				View::render('account/usersSimplified', $data);

			} else if ($_POST['submit'] == 'searchTree')
			{
				$nameTree 	= htmlspecialchars($_POST['treename']);
				$emailTree 	= htmlspecialchars($_POST['useremail']);
				$tagTree 	= trim($_POST['usertag']);

				$searchCriteria = null;
				if ( ($nameTree != null && $nameTree != '') 
					&& ($emailTree != null && $emailTree != '')
					&& ($tagTree != null && $tagTree != '') )
				{
					// search by name AND email AND tag
					$searchCriteria 		= "Nom : \"".$nameTree."\" // E-mail : \"".$emailTree."\" // Tag : \"".$tagTree."\"";
					$data['treeList']		= $this->_tree->getTreesByNameAndEmailAndTag($nameTree, $emailTree, $tagTree);

				} else if (($nameTree != null && $nameTree != '') && ($emailTree != null && $emailTree != ''))
				{
					// search by name AND email
					$searchCriteria 		= "Nom : \"".$nameTree."\" // E-mail : \"".$emailTree."\"";
					$data['treeList']		= $this->_tree->getTreesByNameAndEmail($nameTree, $emailTree);
				} else if (($nameTree != null && $nameTree != '') && ($tagTree != null && $tagTree != ''))
				{
					// search by name AND tagemail
					$searchCriteria 		= "Nom : \"".$nameTree."\" // Tag : \"".$tagTree."\"";
					$data['treeList']		= $this->_tree->getTreesByNameAndTag($nameTree, $tagTree);
				} else if (($tagTree != null && $tagTree != '') && ($emailTree != null && $emailTree != ''))
				{
					// search by email AND tag
					$searchCriteria 		= "E-mail : \"".$emailTree."\" // Tag : \"".$tagTree."\"";
					$data['treeList']		= $this->_tree->getTreesByEmailAndTag($emailTree, $tagTree);
				}else if ($nameTree != null && $nameTree != '') 
				{
					// search by name
					$searchCriteria 		= "Nom : \"".$nameTree."\"";
					$data['treeList'] 		= $this->_tree->getTreesByName($nameTree);
				} else if ($emailTree != null && $emailTree != '') {
					// search by email
					$searchCriteria 		= "E-mail : \"".$emailTree."\"";
					$data['treeList'] 		= $this->_tree->getTreesByEmail($emailTree);
				} else if ($tagTree != null && $tagTree != '') {
					// search by tag
					$searchCriteria 		= "Tag : \"".$tagTree."\"";
					$data['treeList'] 		= $this->_tree->getTreesByTag($tagTree);
				} else 
				{
					$searchCriteria 		= "Tous les utilisateurs.";
					$data['treeList'] 		= $this->_tree->getTrees();
				}
				
				if ($searchCriteria != null)
					$data['searchCriteria'] 		= $searchCriteria;
				
				unset($_POST['submit']);
				
				unset($_POST['useremail']);
				unset($_POST['treename']);
				unset($_POST['usertag']);

				View::render('account/treesSimplifiedAdmin', $data);

			} else
			{
				// Do nothing
			}
		} else 
		{
			$data['title']			= 'Recherche';
			$data['sub-title'] 		= $user->fullname;

			$data['user']			= $user;

			View::rendertemplate('header', $data);
			View::rendertemplate('headerMark', $data);
			View::rendertemplate('sidemenu', $data);
			View::render('admin/search', $data);
			View::rendertemplate('footerMark', $data);
			View::rendertemplate('footer', $data);
		}
	}
	/**
	* Render search result page
	*/
	public function searchResult ()
	{
		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);

		$data['title']			= 'Recherche';
		$data['sub-title'] 		= $user->fullname;

		$data['user']			= $user;

		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('admin/result', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

}