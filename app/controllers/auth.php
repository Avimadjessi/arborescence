<?php
namespace controllers;
use \core\view,
	\helpers\session,
	\helpers\password,
	\helpers\url,
	\helpers\gump;

/**
* Authentication controller
*
* @package arborescence.app.controllers
* @author Carlos Avimadjessi
*/
class Auth extends \core\controller
{
	private $_model;

	/**
	* Constructor.
	* 
	* Initialize used models and set session parameters. 
	*/
	public function __construct()
	{
		$this->_model = new \models\auth();
	}

	/**
	* Render login page. Used to sign an user in.
	*/
	public function login()
	{
		if(Session::get('loggedin'))
		{
			Url::redirect();
		}

		if(isset($_POST['submit']))
		{
			$validator 		= new Gump();

			$_POST 			= $validator->sanitize($_POST);

			$validator->validation_rules(array(
				'userEmail' 	=> 'required|valid_email',
				'password'		=> 'required|max_len,60|min_len,8'
			));
			$validator->filter_rules(array(
				'userEmail' 	=> 'trim|sanitize_email',
				'password' 		=> 'trim'
			));

			$userEmail		= $_POST['userEmail'];
			$password		= $_POST['password'];
			
			// V A L I D A T I O N
			if (Password::verify($password, $this->_model->getHash($userEmail)) === false)
			{
				$data['failure'][] 	= 'Nom d\'utilisateur ou mot de passe incorrect!';
				$data['failure'][] 	= 'Si vous avez supprimé le compte, <a href="recover" >réactivez le</a>.';
			}

			if(!isset($data['failure']))
			{
				$user 			= $this->_model->getUser($userEmail);
				if($user->active == true)
				{
					Session::set('loggedin', true);
					Session::set('loggedUser', [
						'id'		=> $user->id,
						'email'		=> $user->email,
						'occupation'=> $user->occupation,
						'fullname'	=> $user->fullname,
						'level' 	=> $user->level
					]);

					$data 		= array('lastLoggedin' => date('Y-m-d G:i:s'));
					$where 		= array('id' => $this->_model->getId($userEmail));
					$this->_model->update($data, $where);

					Url::redirect('trees');
				}else
				{
					$data['failure'][] 	= 'Nom d\'utilisateur ou mot de passe incorrect!';
					$data['failure'][] 	= 'Si vous avez supprimé le compte, <a href="recover">réactivez le</a>.';
				}
			}
			unset($_POST['submit']);
		}

		$data['title']		= 'Connexion';
		$data['sub-title'] 	= 'Se connecter';
		$data['email']		= $userEmail;
		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('auth/login', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

	/**
	* Render register page. Used to sign and user up.
	*/
	public function register()
	{
		if(Session::get('loggedin'))
		{
			Url::redirect('');
		}

		if(isset($_POST['submit']))
		{
			$userEmail			= $_POST['userEmail'];
			$userFullname		= htmlspecialchars($_POST['userFullname']);
			$userOccupation 	= htmlspecialchars($_POST['userOccupation']);
			$password			= $_POST['password'];
			$passwordConfirm	= $_POST['passwordConfirm'];

			// V A L I D A T I O N
			if($this->_model->userEmailExists($userEmail) === true)
			{
				$data['failure'][] = 'Cette adresse email est déjà utilisée!';
			}
			if(strcmp($password, $passwordConfirm) !== 0)
			{
				$data['failure'][] = 'Le mot de passe et le mot de passe de confirmation ne correspondent pas.';
			}	
			if(isset($data['failure']))
			{
				$data = array_merge ( $data, array(
						'email'			=> $userEmail,
						'fullname'		=> $userFullname,
						'occupation' 	=> $userOccupation)
				);
			} else 
			{
				$data = array(
					'email'			=> $userEmail,
					'fullname'		=> $userFullname,
					'occupation' 	=> $userOccupation
				);
				$data['lastLoggedin'] 	= date('Y-m-d G:i:s');
				$data['created_at']		= date('Y-m-d G:i:s');
				$data['password']		= Password::Make($password);

				$this->_model->insert($data);

				copy('app/templates/default/images/logo_124.png', 
					'app/templates/default/images/profile/_profile_' . $userEmail . '.png');

				$data['success'][]	= 'Compte créé! Vous pouvez vous connecter.';

				Url::redirect('login');
			}
		}
		$data['title']		= 'Enregistrement';
		$data['sub-title'] 	= 'Créer un compte';
		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('auth/register', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

	/**
	* Render recover page. Used to recover a deleted account.
	*/
	public function recover()
	{
		if(isset($_POST['submit']))
		{
			$userEmail		= $_POST['userEmail'];
			$password		= $_POST['password'];
			
			// V A L I D A T I O N
			if(Password::verify($password, $this->_model->getHash($userEmail)) == false)
			{
				$data['failure'][] 	= 'Nom d\'utilisateur ou mot de passe incorrect!';
			}

			if(!isset($data['failure']))
			{
				$user 			= $this->_model->getUser($userEmail);
				if($user->active == false)
				{
					$data = array('active' 		=> true);
					$where = array('id' 		=> $this->_model->getId($userEmail));
					$this->_model->update($data, $where);

					$user 				= $this->_model->getUser($userEmail);
					$data['success'][] 	= 'Compte réactivé! Vous pouvez vous connecter.';

					Url::redirect('login');
				}else
				{
					$data['failure'][] 	= 'Ce compte n\'est pas désactivé!';
				}
			}
			unset($_POST['submit']);
		}

		$data['title']	= 'Recover';
		$data['sub-title'] 	= 'Récupérer un compte';
		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('auth/recover', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}
	
	/**
	* Sign an user out.
	*/
	public function logout()
	{
		Session::destroy();
		Url::redirect();
	}
}