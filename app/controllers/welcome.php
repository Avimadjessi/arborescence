<?php namespace controllers;

use \core\view,
	\helpers\session,
	\helpers\url,
	\helpers\date,
	\helpers\phpmailer\mail;

/**
 * Welcome controller
 *
 * @package arborescence.app.controllers
 * @author Carlos Avimadjessi
 */
class Welcome extends \core\controller{

	private $_tag;
	private $_treeHasTag;
	private $_userHasTag;

	/**
	* Constructor
	* 
	* Initialize used models and set session parameters 
	*/
	public function __construct()
	{
		parent::__construct();
		if(Session::get('loggedin'))
		{
			$this->_auth 	= new \models\auth();

			$this->_tag 		= new \models\tag();
			$this->_treeHasTag	= new \models\treeHasTag();
			$this->_userHasTag 	= new \models\userHasTag();

			// Tags in session
			$userEmail 				= Session::get('loggedUser')['email'];
			$user 					= $this->_auth->getUser($userEmail);

			$userTags 		= $this->_tag->getTagListByUser($user->id);
			Session::set('userTags', $userTags);
		}
	}

	/**
	 * Define Index page title and render home page
	 */
	public function index() 
	{
		$data['title'] = 'Bienvenue';
		
		View::rendertemplate('header', $data);
		View::render('welcome/welcome', $data);
		View::rendertemplate('footer', $data);
	}

	/**
	* Render about page
	*/
	public function about () 
	{
		if (Session::get('loggedin'))
		{
			$userEmail 			= Session::get('loggedUser')['email'];
			$user 				= $this->_auth->getUser($userEmail);
			if($user)
			{
				$data['user']				= $user;
			}
		}
		
		$data['title']			= 'À propos';
		$data['sub-title'] 		= 'À propos';
		
		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('welcome/about', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

	/**
	* Render the FAQ page and used to send a mail when the FAQ form is filled and submited.
	*/
	public function faq () 
	{
		if(isset($_POST['submit']))
		{
			unset($_POST['submit']);
			$senderFullname = htmlspecialchars($_POST['askFullname']);
			$senderEmail 	= $_POST['askEmail'];
			$senderMessage 	= htmlspecialchars($_POST['askMessage']);
			$senderDate 	= Date::verbose(date('Y-m-d G:i:s'));
			$loggedUserStr 	= "";
			if(Session::get('loggedin'))
			{
				$loggedUserStr = htmlspecialchars(Session::get('loggedUser')['occupation'])."<br/>";
				$profileImgUrl = 'http://gramm-r.ulb.ac.be/arborescence/app/templates/default/images/profile/_profile_' . Session::get('loggedUser')['email'] . '.png';
				$loggedUserStr = $loggedUserStr."<br/><img style='text-align:center;width: 80px; height: 80px;-webkit-border-radius: 40px;-moz-border-radius: 40px;border-radius: 40px;background-repeat: no-repeat;' src='".$profileImgUrl."'> <br/>";
			}

			// ===== // Mail ===================================================

			$mail 			= new mail();
			$mail->Port 	= 587;

			$mail->setFrom('carlos.avim@gmail.com', 'Arborescence FAQ');
			$mail->addAddress($senderEmail, $senderFullname);
			$mail->addAddress('carlos.avim@gmail.com', 'Carlos A.');
			$mail->addAddress('lmeinert@gmail.com', 'Lionel Meinertzhagen');	

			$mail->addReplyTo($senderEmail, $senderFullname);

			$mail->isHTML(true);
			$mail->subject('Arborescence - FAQ');
			$mail->AddEmbeddedImage("app/templates/default/images/logo_124.png", "_logo", "Arborescence logo");
			$mail->body(
				"
				<!DOCTYPE html>
				<html>
					<head>
						<!-- Site meta -->
						<meta charset='utf-8'>
						<title>Question de ".$senderFullname."</title>
						<style>

							/* Ceci est un message de la platforme Arborescence // Outil de dessin d'arbre grammatical de l'Université Libre de Bruxelles. 
							Veillez ignorer ce message si vous n'avez jamais donné votre adresse email sur Arborescence ou envoyez un message à l'adresse mail si dessous pour toute question.
							*/

							hr
							{
								border: rgba(208,220,71,1) 2px solid;
							}

							/* ===== T A B L E
							==================================================================== 
							*/
							table
							{
								width: 100%;
							}
							tbody
							{
								width: 100%;
							}
							tr td
							{
								min-height: 140px;
								padding: 4px;
								padding-right: 36px;
								text-align: justify;
							}
							td img
							{
								width: 100%;
							}
						</style>
					</head>
					<body style='font-family: Verdana, sans-serif;background-color: #D0DC47;padding:10%'>
						<div class='container' style='text-align:center; padding: 24px 0px; background-color: white;'>
							<img src='cid:_logo'></img>
							<div style='font-size: 1.2em; letter-spacing: 8px; text-transform: uppercase; padding: 8px 0px;'>Arborescence</div>
							<div style='letter-spacing: 8px;font-weight: bold; padding-bottom: 8px;'><b>Question d'utilisateur</b></div>
							<div style='font-size: 0.8em; letter-spacing: 2px;'>".$senderDate."</div>
							<br/>
							<hr>
							<br/>
							<div style='text-align:left; padding: 12px 48px;'>
								<br/>

								<!-- Deux Colonnes -->
								<table>
									<thead>
										<tr>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<!-- Ligne 1 -->
										<tr>
											<!-- Col 1 -->
											<td style='width: 30%;'>
												<h3>Auteur</h3>
												<hr/>
												<label>".htmlspecialchars($senderFullname)."</label>
												<br/> "
												.$loggedUserStr."
												<label>".htmlspecialchars($senderEmail)."</label>
											</td>
											<!-- Col 2 -->
											<td style='width: 50%;'>
											".htmlspecialchars($senderMessage)."
											</td>
										</tr>
									</tbody>
									<tfoot>
									</tfoot>
								</table>
								<br/>
							</div>
							<hr/>
							<br/>
							<span style='color : #D0DC47;'><a href='http://gramm-r.ulb.ac.be/arboresence'>Arborescence</a></span> | lmeinert@ulb.ac.be | +32 2 650 44 42
							<br/>
							<span style='letter-spacing:4px;'>Université Libre de Bruxelles</span>
							<br/>
							Gramm-r | Arborescence &copy; 2015
						</div>
					    <footer>
					    </footer>
				    </body>
				</html>
				"
			);
			if ($mail->send())
			{
				// Successfuly sent
				$data['success'][] 	= "Courrier envoyé!<br/>Un responsable vous répondra sous peu à votre adresse de courriel.";
			} else
			{
				// Failed
				$data['failure'][]	= "Echec de l'envoi du courrier.<br/>Vérifiez votre adresse de courriel et rééssayez!";
			}
		}

		$data['title']			= 'FAQ';
		$data['sub-title'] 		= 'FAQ';

		if (Session::get('loggedin'))
		{
			$userEmail 			= Session::get('loggedUser')['email'];
			$user 				= $this->_auth->getUser($userEmail);
			if($user)
			{
				$data['user']	= $user;
			}
		}
		
		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('welcome/faq', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

}
