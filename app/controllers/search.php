<?php
namespace controllers;
use \core\view,
	\helpers\session,
	\helpers\url,
	\helpers\password,
	\models\auth,
	\models\tree;

/**
* Search Controller
*
* @package arborescence.app.controllers
* @author Carlos Avimadjessi
*/
class Search extends \core\controller
{
	private $_auth;
	private $_tree;
	private $_tag;
	private $_treeHasTag;
	private $_userHasTag;
	private $_notice;

	/**
	* Constructor
	* 
	* Initialize used models and set session parameters 
	*/
	public function __construct()
	{
		if(!Session::get('loggedin'))
		{
			Url::redirect('login');
		}

		$this->_auth = new auth();
		$this->_tree = new tree();

		$this->_tag 		= new \models\tag();
		$this->_treeHasTag	= new \models\treeHasTag();
		$this->_userHasTag 	= new \models\userHasTag();

		$this->_notice 	= new \models\notice();

		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);

		$userTags 		= $this->_tag->getTagListByUser($user->id);
		$userNotices 	= $this->_notice->getNoticeListByUser($user->id);

		Session::set('userTags', $userTags);
		Session::set('userNotices', $userNotices);
		$allNotices 	= $this->_notice->getNoticeByUser($user->id);
		Session::set('allNotices', $allNotices);
	}

	/**
	* Retreive all notices and render a view containing them
	*/
	public function noticeGetAll()
	{
		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);
		if(!$user)
		{
			Url::redirect();
		}
		$data['title']			= 'Notice';
		$data['sub-title'] 		= $user->fullname;

		$data['user']			= $user;

		$allNotices 	= $this->_notice->getNoticeByUser($user->id);
		Session::set('allNotices', $allNotices);

		View::render('account/noticesSimplified', $data);
	}

}