<?php
namespace controllers;
use \core\view,
	\helpers\session,
	\helpers\password,
	\helpers\arborescenceParser,
	\helpers\helper,
	\helpers\url;

/**
* Draw controller
* 
* Manage everything around the canvas
* @package arborescence.app.controllers
* @author Carlos Avimadjessi
*/
class Draw extends \core\controller
{
	private $_auth;
	private $_tree;
	private $_exercise;
	private $_tag;
	private $_treeHasTag;
	private $_userHasTag;
	private $_notice;

	/**
	* Constructor
	* 
	* Initialize used models and set session parameters 
	*/
	public function __construct ()
	{
		if(!Session::get('loggedin'))
		{
			Url::redirect('login');
		}
		$this->_auth 		= new \models\auth();
		$this->_exercise 	= new \models\exercise();
		$this->_tree 		= new \models\tree();
		$this->_tag 		= new \models\tag();
		$this->_treeHasTag	= new \models\treeHasTag();
		$this->_userHasTag 	= new \models\userHasTag();
		$this->_notice 		= new \models\notice();

		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);

		$userTags 		= $this->_tag->getTagListByUser($user->id);
		$userNotices 	= $this->_notice->getNoticeListByUser($user->id);

		Session::set('userTags', $userTags);
		Session::set('userNotices', $userNotices);

		$allNotices 	= $this->_notice->getNoticeByUser($user->id);
		Session::set('allNotices', $allNotices);
	}

	/**
	* Render the canvas page, save and reload trees
	* 
	* @param Integer userId null by default. Used when an admin load an user's tree 
	*/ 
	public function canvas ($asUserId=null)
	{
		if (isset($_POST['save']))
		{
			$name 				= htmlspecialchars($_POST['name']);
			$jsonNodes			= $_POST['jsonNodes'];
			$jsonRelations		= $_POST['jsonRelations'];
			$jsonDrawnObjects	= $_POST['jsonDrawnObjects'];
			$jsonNotes 			= $_POST['jsonNotes'];
			$treeTags 			= $_POST['treeTags'];
			$treeVersion		= $_POST['version'];
			$email				= Session::get('loggedUser')['email'];
			$userId				= $this->_auth->getId($email);
			$slug 				= Helper::slugify($name);

			
			// ========== V A L I D A T I O N
			if($this->_tree->treeExists($userId, $slug))
			{
				// Update
				$data = array(
					'jsonNodes'			=> $jsonNodes,
					'jsonRelations'		=> $jsonRelations,
					'jsonDrawnObjects'	=> $jsonDrawnObjects,
					'jsonNotes' 		=> $jsonNotes
				);

				$where 		= array('id' => $this->_tree->getBySlug($userId, $slug)->id);
				$this->_tree->update($data, $where);

				$data['title']			= 'Dessin';
				$data['tree']			= $this->_tree->getBySlug($userId, $slug);

			} else if (isset($asUserId))
			{
				// Admin update
				$isAdmin				= (Session::get('loggedUser')['level'] == 9);

				if ($isAdmin)
				{
					$data = array(
						'jsonNodes'			=> $jsonNodes,
						'jsonRelations'		=> $jsonRelations,
						'jsonDrawnObjects'	=> $jsonDrawnObjects,
						'jsonNotes' 		=> $jsonNotes
					);

					$where 		= array('id' => $this->_tree->getBySlug($asUserId, $slug)->id);
					$this->_tree->update($data, $where);

					$data['title']			= 'Dessin';
					$data['tree']			= $this->_tree->getBySlug($asUserId, $slug);
				} else 
				{
					Url::redirect('logout');
				}				
			} else
			{
				// Insert
				$data = array(
					'jsonNodes'			=> $jsonNodes,
					'jsonRelations'		=> $jsonRelations,
					'jsonDrawnObjects'	=> $jsonDrawnObjects,
					'jsonNotes' 		=> $jsonNotes,
					'user'				=> $userId,
					'name'				=> $name,
					'slug'				=> $slug,
					'created_at'		=> date('Y-m-d G:i:s'),
					'lastUser'			=> $userId,
					'version' 			=> 0.1
				);
				$this->_tree->insert($data);

				// Notice
				$dataNotice = array(
					'user' 		=> $userId,
					'title' 	=> 'Arbre créé!',
					'type' 		=> 'TREE_SAVED',
					'body' 		=> '"'.$name.'"',
					'created_at'=> date('Y-m-d G:i:s')
				);
				$this->_notice->insert($dataNotice);

				Session::set('treeName', $data['name']);

				$data['title']			= 'Dessin';
				$data['tree']			= $this->_tree->getBySlug($userId, $slug);

			}
			if ($treeTags)
			{
				// Save tags
				foreach ($treeTags as $key => $tag) 
				{
					if ($tag[id])
					{
						if ($tag[deleted])
							$this->untag($tag[id]);
					} else
					{
						$this->tag($slug, $tag[body]);
					}
				}
			}

			unset($_POST['save']);

		} else if (isset($_POST['reload']))
		{
			unset($_POST['reload']);
			
			if (isset($_POST['name']))
			{
				$name 				= htmlspecialchars($_POST['name']);
				$slug 				= Helper::slugify($name);

				echo DIR.'trees/'.$slug.'/canvas';
			} else 
			{
				$data['failure'][] = "Erreur durant le chargement de l'arbre";
				echo DIR.'trees';
			}
		} else		
		{
			$userEmail 					= Session::get('loggedUser')['email'];
			$user 						= $this->_auth->getUser($userEmail);
			if(!$user)
			{
				Url::redirect('logout');
			}
			$data['title']				= 'Dessin';
			$data['sub-title'] 			= Session::get('loggedUser')['fullname'];
			$data['treeList']			= $this->_tree->getTreeList($user->id);

			$data['user']				= $user;	

			View::rendertemplate('header', $data);
			View::rendertemplate('headerMark', $data);
			View::rendertemplate('sidemenu', $data);
			View::render('draw/canvas', $data);
			View::rendertemplate('footerMark', $data);
			View::rendertemplate('footer', $data);
		}
	} 

	/**
	* Render an user's exercise
	*
	* @param String $slug the exercise's slug 
	*/
	public function canvasExercise ($slug)
	{
		$solutionName 		= $_POST['solutionName'];
		if (isset($_POST['save']))
		{
			unset($_POST['save']);
		} else if (isset($_POST['reload']))
		{
			unset($_POST['reload']);
		} else		
		{
			$exerciseId 				= $_POST['exerciseId'];
			$solutionSlug 				= Helper::slugify($solutionName);
			$userEmail 					= Session::get('loggedUser')['email'];
			$user 						= $this->_auth->getUser($userEmail);
			if(!$user || !$exerciseId)
			{
				Url::redirect('logout');
			}

			// ========== V A L I D A T I O N
			if($this->_exercise->solutionExists($user->id, $solutionSlug))
			{
				// Load 
				$data = array(
					'jsonNodes'			=> $jsonNodes,
					'jsonRelations'		=> $jsonRelations,
					'jsonDrawnObjects'	=> $jsonDrawnObjects,
					'jsonNotes' 		=> $jsonNotes
				);

				$where 		= array('id' => $this->_tree->getBySlug($userId, $slug)->id);
				$this->_tree->update($data, $where);

				$data['title']			= 'Dessin';
				$data['tree']			= $this->_tree->getBySlug($userId, $slug);

			}else
			{
				// Insert
				$data = array(
					'jsonNodes'			=> $jsonNodes,
					'jsonRelations'		=> $jsonRelations,
					'jsonDrawnObjects'	=> $jsonDrawnObjects,
					'jsonNotes' 		=> $jsonNotes,
					'exercise' 			=> $exerciseId,
					'user'				=> $userId,
					'name'				=> $name,
					'slug'				=> $slug,
					'created_at'		=> date('Y-m-d G:i:s'),
					'lastUser'			=> $userId,
					'version' 			=> 0.1
				);
				$this->_tree->insert($data);

				// Notice
				$dataNotice = array(
					'user' 		=> $userId,
					'title' 	=> 'Arbre créé!',
					'type' 		=> 'TREE_SAVED',
					'body' 		=> '"'.$name.'"',
					'created_at'=> date('Y-m-d G:i:s')
				);
				$this->_notice->insert($dataNotice);

				Session::set('treeName', $data['name']);

				$data['title']			= 'Dessin';
				$data['tree']			= $this->_tree->getBySlug($userId, $slug);

			}
			
			$exercise 				= $this->_exercise->getExBySlug($user->id, $slug);
			$data['tree']			= $exercise;
			$data['treeLoaded']		= true;

			$data['title']				= 'Dessin';
			$data['sub-title'] 			= Session::get('loggedUser')['fullname'];
			$data['treeList']			= $this->_tree->getTreeList($user->id);
			$data['user']				= $user;	
			$data['treeTags']			= [];//$this->_tag->getTagListByTree($data['tree']->id);

			View::rendertemplate('header', $data);
			View::rendertemplate('headerMark', $data);
			View::rendertemplate('sidemenu', $data);
			View::render('draw/canvas', $data);
			View::rendertemplate('footerMark', $data);
			View::rendertemplate('footer', $data);
		}
	}

	/**
	* Render user's tree list
	*/
	public function trees ()
	{
		if(!Session::get('loggedin'))
		{
			Url::redirect('login');
		}
		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);
		if(!$user)
		{
			Url::redirect('logout');
		}
		$data['title']			= 'Arbres';
		$data['sub-title'] 		= $user->fullname;

		$data['user']			= $user;
		$data['treeList'] 		= array();	
		if (isset($_POST['tag']))
		{
			$tag 					= $this->_tag->getByBody($_POST['tag']);
			if ($tag)
			{

				$data['searchCriteria'] 		= 'Mot clé : '.$tag->body;
				// tagId + user
				$data['treeList']		= $this->_tree->getTreeListByTag($tag->id, $user->id);
			}	

			unset($_POST['tag']);


			View::render('account/treesSimplified', $data);
		} else 
		{
			$data['treeList']			= $this->_tree->getTreeList($user->id);
			$data['exerciseList']		= $this->_exercise->getExerciseList($user->id);
			
			if ($user->level >= 9)
				$data['userList'] 		= $this->_auth->getUsers();

			View::rendertemplate('header', $data);
			View::rendertemplate('headerMark', $data);
			View::rendertemplate('sidemenu', $data);
			View::render('account/trees', $data);
			View::rendertemplate('footerMark', $data);
			View::rendertemplate('footer', $data);
		}
	}

	/**
	* Used to load a tree based on his slug
	* @param String $slug the tree's slug
	* @param Integer $asUserId null by default except when an admin load an other user tree
	*/
	public function load ($slug, $asUserId=null)
	{
		if (isset($_POST['save']))
		{
			$this->canvas($asUserId);
		} else
		{
			$userEmail					= Session::get('loggedUser')['email'];
			$user 						= $this->_auth->getUser($userEmail);
			if(!$user)
			{
				Url::redirect('logout');
			}
			// Admin load
			if (isset($asUserId))
				$userId 				= $asUserId;
			else
				$userId					= $user->id;
			
			$data['tree']				= $this->_tree->getBySlug($userId, $slug);
			$data['treeLoaded']			= true;

			if ($data['tree']->exercise)
			{
				$data['exercise'] 		= $this->_exercise->getExercise($data['tree']->exercise);
			}

			$data['title']				= 'Charger';
			$data['sub-title'] 			= Session::get('loggedUser')['fullname'];
			$data['treeList']			= $this->_tree->getTreeList($user->id);

			$data['user']				= $user;	
			
			$data['treeTags']			= $this->_tag->getTagListByTree($data['tree']->id);

			View::rendertemplate('header', $data);
			View::rendertemplate('headerMark', $data);
			View::rendertemplate('sidemenu', $data);
			View::render('draw/canvas', $data);
			View::rendertemplate('footerMark', $data);
			View::rendertemplate('footer', $data);
		}
	}

	/**
	* Starts an exercise creating and saving a tree from exercise
	*/
	public function startExercise ()
	{
		if(isset($_POST))
		{
			$solutionName 	= htmlspecialchars($_POST['solutionName']);
			$exerciseId 	= $_POST['exerciseId'];

			$userEmail		= Session::get('loggedUser')['email'];
			$user 			= $this->_auth->getUser($userEmail);
			$slug 			= Helper::slugify($solutionName);

			if(!$user)
			{
				Url::redirect('logout');
			}

			$exercise 		= $this->_exercise->getExercise($exerciseId);
			$num 			= 1; 
			$tmpName 		= $solutionName;
			while ($this->_tree->treeExists($user->id, $slug))
			{
				// $solutionName 	= $solutionName.$num;
				$tmpName 		= $solutionName.' '.$num;
				$slug 			= Helper::slugify($tmpName);
				$num++;
			}
			$solutionName = $tmpName;

			$data 			= array(
				'jsonNodes'			=> $exercise->jsonNodes,
				'jsonRelations'		=> $exercise->jsonRelations,
				'jsonDrawnObjects'	=> $exercise->jsonDrawnObjects,
				'jsonNotes' 		=> $exercise->jsonNotes,
				'user'				=> $user->id,
				'name'				=> $solutionName,
				'slug'				=> $slug,
				'exercise'			=> $exerciseId,
				'created_at'		=> date('Y-m-d G:i:s'),
				'lastUser'			=> $user->id,
				'version' 			=> 0.1
			);
			$this->_tree->insert($data);
			$tree 					= $this->_tree->getBySlug($user->id, $slug);

			$dataSolution 	= array(
				'exercise'			=> $exercise->id,
				'tree'				=> $tree->id,
				'created_at'		=> date('Y-m-d G:i:s')
			);
			$this->_exercise->insertSolution($dataSolution);
			
			$data['tree']			= $tree;
			$data['exercise'] 		= $exercise;
			$data['treeLoaded']		= true;

			$data['title']			= 'Charger';
			$data['sub-title'] 		= Session::get('loggedUser')['fullname'];
			$data['treeList']		= $this->_tree->getTreeList($user->id);

			$data['user']			= $user;
			
			$data['treeTags']		= $this->_tag->getTagListByTree($data['tree']->id);

			View::rendertemplate('header', $data);
			View::rendertemplate('headerMark', $data);
			View::rendertemplate('sidemenu', $data);
			View::render('draw/canvas', $data);
			View::rendertemplate('footerMark', $data);
			View::rendertemplate('footer', $data);

			unset($_POST);
		}
	}

	/**
	* Check the rightness of an exercise
	*/
	public function verifyExercise ()
	{
		if (isset($_POST))
		{
			$exerciseId 			= $_POST['exerciseId'];
			$treeNodes 				= $_POST['treeNodes'];
			$treeRelations 			= $_POST['treeRelations'];
			$treeDrawnObjects 		= $_POST['treeDrawnObjects'];

			$exercise 		= $this->_exercise->getExercise($exerciseId);
			if (!$exercise)
			{
				echo "Exercice introuvable! Vérifiez que vous avez toujours cet exercice. ";

				return false;
			} else 
			{
				$parserTree 		= new arborescenceParser();
				$parserExercise		= new arborescenceParser();

				// ----- Tree 
				$nodes 				= $treeNodes['tree']['subtrees'];
				$relations 			= $treeRelations['relations'];
				$treeDrawnObjects	= isset($treeDrawnObjects) ? $treeDrawnObjects['drawnObjects'] : array();

				$parserTree->parse($nodes, 'node');
				$parserExercise->parse($exercise->jsonNodes, 'node', true);

				$nodesTree 			= $parserTree->getNodes();
				$nodesExercise 		= $parserExercise->getNodes();
				$nbNodesTree 		= count($nodesTree);
				$nbNodesExercise 	= count($nodesExercise);
				
				// ---------- Check number of nodes ----------------------------
				if ($nbNodesTree != $nbNodesExercise)
				{
					if($nbNodesTree >  $nbNodesExercise)
						echo "<label class='label label-danger'>Exercice Incorrect!</label> <br/><br/>Effectuez cet exercice avec le <b>minimum</b> de noeuds.";
					else
						echo "<label class='label label-danger'>Exercice Incorrect!</label> <br/><br/>Cet exercise est <b>incomplet</b>.";

					return false;
				}
				// --------- Compare each node ---------------------------------
				for ($i=0; $i < $nbNodesTree; $i++) 
				{ 
					$equals = $parserTree->nodeEquals($nodesTree[$i], $nodesExercise[$i]);
					if (!$equals)
					{
						echo "<label class='label label-danger'>Exercice Incorrect!</label> <br/><br/>
						Erreur détectée. Vérifiez à nouveau l'exercice.";
						return false;
					}
				}
			}

			unset($_POST);
			echo "<label class='label label-success'>Exercice Correct</label> <br/><br/>Veuillez envoyer l'exercice à un responsable pour confirmer cette vérification.";
			return true;
		}
	}

	/**
	* Render drawing help page
	*/
	public function help ()
	{
		$userEmail					= Session::get('loggedUser')['email'];
		$user 						= $this->_auth->getUser($userEmail);
		if(!$user)
		{
			Url::redirect('logout');
		}
		
		$data['title']			= 'Aide';
		$data['sub-title'] 		= Session::get('loggedUser')['fullname'];
		$data['user']			= $user;	
		
		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('draw/help', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

	/**
	* Used to delete a tree
	* 
	* @param String $slug the tree's slug
	*/
	public function delete ($slug)
	{
		$data['title']			= 'Delete';
		$data['sub-title'] 		= Session::get('loggedUser')['fullname'];

		$email					= Session::get('loggedUser')['email'];
		$userId					= $this->_auth->getId($email);

		if($this->_tree->treeExists($userId, $slug))
		{
			$tree				= $this->_tree->getBySlug($userId, $slug);
			$data 				= array(
				'id'	=> $tree->id
			);

			$this->_tree->delete($data);

			// Notice
			$dataNotice = array(
				'user' 		=> $userId,
				'title' 	=> 'Arbre supprimé!',
				'type' 		=> 'TREE_DELETED',
				'body' 		=> '"'.$tree->name.'"',
				'created_at'=> date('Y-m-d G:i:s')
			);
			$this->_notice->insert($dataNotice);

			Url::redirect('trees');
		}	 
	}

	/**
	* Used send a copy of a tree to an other user 
	*/
	public function share ()
	{
		$email 				= $_POST['email'];
		$slug 				= $_POST['slug'];
		$message 			= htmlspecialchars($_POST['message']);

		$userEmail			= Session::get('loggedUser')['email'];
		$userId				= $this->_auth->getId($userEmail);

		$user				= $this->_auth->getUser($email);

		if ($user)
		{
			$tree 			= $this->_tree->getBySlug($userId, $slug);

			if($tree)
			{
				$oldUser 			= $tree->user;
				$tree->user 		= $user->id;
				$tree->lastUser 	= $oldUser;

				$data = array(
					'jsonNodes'			=> $tree->jsonNodes,
					'jsonRelations'		=> $tree->jsonRelations,
					'jsonDrawnObjects'	=> $tree->jsonDrawnObjects,
					'jsonNotes'			=> $tree->jsonNotes,
					'user'				=> $tree->user,
					'lastUser' 			=> $tree->lastUser,
					'name'				=> $tree->name,
					'message' 			=> $message,
					'slug'				=> $tree->slug,
					'created_at'		=> $tree->created_at,
					'updated_at'		=> $tree->updated_at,
				);

				$newTree 	= $this->_tree->getBySlug($user->id, $slug);

				if ($newTree)
				{
					// The receiver do have the tree > error
					echo 'error-tree';
				} else 
				{
					// The receiver doesn't have the tree > send
					$result = $this->_tree->insert($data);

					// Notice
					$dataNotice = array(
						'user' 		=> $userId,
						'title' 	=> 'Arbre envoyé!',
						'body' 		=> '"'.$tree->name.'" à '. $email,
						'type' 		=> 'TREE_SENT',
						'created_at'=> date('Y-m-d G:i:s')
					);
					$this->_notice->insert($dataNotice);

					// Notice
					$otherUserId		= $this->_auth->getId($email);
					$otherDataNotice = array(
						'user' 		=> $otherUserId,
						'title' 	=> 'Arbre reçu!',
						'type' 		=> 'TREE_RECEIVED',
						'body' 		=> '"'.$tree->name.'" de '. $userEmail,
						'created_at'=> date('Y-m-d G:i:s')
					);
					$this->_notice->insert($otherDataNotice);

					echo 'success';
				}

			}else
			{
				echo 'unknow';
			}

		} else 
		{
			echo 'error-user';
		}
	}

	// ========== T A G ========================================================
	/**
	* Find and delete a tag based on id
	*
	* @param Integer $id the tag's ID 
	*/
	public function untag ($id)
	{
		if ($id)
		{
			$this->_tag->delete (array('id' => $id ));
		}
	}

	/**
	* Link a tag to a tree
	*
	* @param String $slug tree's tag
	* @param String $tagBody the tag's content
	*/
	public function tag ($slug, $tagBody)
	{
		$userEmail			= Session::get('loggedUser')['email'];
		$userId				= $this->_auth->getId($userEmail);

		if (!$slug)
		{
			echo 'No slug - Tree not saved.';
		}
		$tree 				= $this->_tree->getBySlug($userId, $slug);
		$tagSlug 			= Helper::slugify($tagBody);
		
		if($tree)
		{
			if ($this->_tag->tagExists($tagSlug))
			{
				// Tag exists
				// Get tagId
				$tag 		= $this->_tag->getBySlug($tagSlug);

				// Check if tag is linked to the tree
				if ($this->_treeHasTag->isTreeLinkedTag($tag->id, $tree->id))
				{
					echo 'Tag déjà ajouté à l\'arbre';
					// Is linked =>> user own it
					// Trying to add a tag already added >> Nothing change
				} else
				{
					// Not linked to the tree yet
					if ($this->_userHasTag->isUserLinkedTag($tag->id, $userId))
					{
						// The user already used that tag but not yet on this tree
						// Link tag to tree
						$dataHasTag 	= array(
							'tree' 			=> $tree->id,
							'tag' 			=> $tag->id,
							'created_at' 	=> date('Y-m-d G:i:s')
						);
						$hasTag 	= $this->_treeHasTag->insert($dataHasTag);

					} else
					{
						// The user never used that tag
						// Link tag to tree
						$dataHasTag 	= array(
							'tree' 			=> $tree->id,
							'tag' 			=> $tag->id,
							'created_at' 	=> date('Y-m-d G:i:s')
						);
						$hasTag 	= $this->_treeHasTag->insert($dataHasTag);

						// Link tag to user
						$dataOwnTag 	= array(
							'user' 			=> $userId,
							'tag' 			=> $tag->id,

							'created_at' 	=> date('Y-m-d G:i:s')
						);
						$ownTag 	= $this->_userHasTag->insert($dataOwnTag);
					}
				}
				// Check if tag is linked to the user
			} else 
			{
				// Tag doesn't exist yet
				// Create tag
				$dataTag 		= array(
					'body' 			=> $tagBody,
					'slug' 			=> $tagSlug,
					'created_at' 	=> date('Y-m-d G:i:s')
				);
				$tagId 		= $this->_tag->insert($dataTag);

				// Link tag to tree
				$dataHasTag 	= array(
					'tree' 			=> $tree->id,
					'tag' 			=> $tagId,
					'created_at' 	=> date('Y-m-d G:i:s')
				);
				$hasTag 	= $this->_treeHasTag->insert($dataHasTag);

				// Link tag to user
				$dataOwnTag 	= array(
					'user' 			=> $userId,
					'tag' 			=> $tagId,

					'created_at' 	=> date('Y-m-d G:i:s')
				);
				$ownTag 	= $this->_userHasTag->insert($dataOwnTag);
			}

			// Refresh tag list displayed

		} else 
		{
			echo 'Veuillez sauvegarder votre arbre avant d\'y ajouter des tags';
		}
	}

	/**
	* Search trees and Render the list based on tag
	*
	* @param String $slug the tag's slug
	*/
	public function searchUserTag ($slug)
	{
		if(!Session::get('loggedin'))
		{
			Url::redirect('login');
		}
		$userEmail 				= Session::get('loggedUser')['email'];
		$user 					= $this->_auth->getUser($userEmail);
		if(!$user)
		{
			Url::redirect('logout');
		}
		$data['title']			= 'Arbres';
		$data['sub-title'] 		= $user->fullname;

		$data['user']			= $user;
		$data['treeList'] 		= array();	

		$tag 					= $this->_tag->getBySlug($slug);
		if ($tag)
		{

			$data['sub-title'] 		= 'Mot-clé : '.$tag->body;
			$data['treeList']		= $this->_tree->getTreeListByTag($tag->id, $user->id);
		}

		View::rendertemplate('header', $data);
		View::rendertemplate('headerMark', $data);
		View::rendertemplate('sidemenu', $data);
		View::render('account/treesResult', $data);
		View::rendertemplate('footerMark', $data);
		View::rendertemplate('footer', $data);
	}

}