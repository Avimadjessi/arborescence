<?php
use \core\error;
?>
<div class="bodyWrapper">

	<div class="bodyMargin">
		
		<div class="bodyContent">
			<div id="slide_about" class="slide">
				<h4>Publier un message</h4>
				<hr/>
				<div> 
					<label>Envoyer une annonce à tout le monde</label>
					<br/>
					<?php
						$errors = $data['failure'];
						if($errors)
						{
					?>
							<div class="alert alert-danger center">
								<?php
									foreach ($errors as $key => $value) 
									{
									 	echo $errors[$key]."<br/>";
									} 
								?>
							</div>
					<?php
						}
					?>
					<?php
						$success = $data['success'];
						if($success)
						{
					?>
							<div class="alert alert-success center">
								<?php
									foreach ($success as $key => $value) 
									{
									 	echo $success[$key]."<br/>";
									} 
								?>
							</div>
					<?php
						}
					?>
				    <form role="form" method="POST" class="form-default-lg" >

				        <!-- ---------- T I T L E ---------------------- -->
				        <input type="text" name="noticeTitle" 
				               value="" maxlength="64"
				               class="form-control" placeholder="Titre de l'annonce"
				               required autofocus>

				        <!-- ---------- B O D Y ------------------------ -->
				        <textarea type="text" name="noticeBody" style="resize:none;"
				               value=""
				               rows="2" maxlength="256" 
				               class="form-control last-input" placeholder="Corps de l'annonce"
				               required></textarea>

				        <!-- ---------- S U B M I T -------------------- -->
				        <button type="submit" name="submit" value="notice" class="btn btn-lg btn-primary">
				            <i class="fa fa-send"></i> Publier 
				        </button>

				    </form>
				</div>
			</div>
		</div>

	</div>

</div>

<script>
	activateMenuItem('adminNotice', 'adminItems');
</script>