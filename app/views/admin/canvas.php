<?php
use \core\error,
    \helpers\date;
?>

<div class="bodyWrapper">

    <div class="bodyMargin">
        
        <div id="body-arbre" class="bodyContent">
            <!-- ========== B U T T O N // O P T I O N S ====================-->
            <div class="text-center">
                <div class="btn-group">
                    <button data-toggle="dropdown" 
                            class="btn btn-primary dropdown-toggle">
                        <i class="fa fa-tree"></i> 
                        E X E R C I C E
                        <i class="caret"></i>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo DIR?>admin/exercise/canvas">
                                <i class="fa fa-plus"></i> 
                                Nouveau
                            </a>
                        </li>
                        <li>
                            <a href="javascript:arborescence.drawing.editTreeTitle()">
                                <i class="fa fa-edit"></i> 
                                Éditer le titre
                            </a>
                        </li>
                        <li>
                            <a href="javascript:arborescence.drawing.editExerciseDesc()">
                                <i class="fa fa-question"></i> 
                                Rédiger / Modifier l'énoncé
                            </a>
                        </li>
                        <li>
                            <a href="javascript:arborescence.drawing.saveExercise()">
                                <i class="fa fa-save"></i> Sauvegarder l'exercice
                            </a>
                        </li>
                        <li>
                            <a href="javascript:arborescence.drawing.generatePNG()">
                                <i class="fa fa-image"></i> Générer une image
                            </a>
                        </li>
                        <li class="divider"></li> 
                        <li>
                            <a href="<?php echo DIR?>trees">
                                <i class="fa fa-times"></i> Quitter l'éditeur
                            </a>
                        </li>   
                    </ul>
                </div>
                <div class="btn-group">
                    <button data-toggle="dropdown" 
                            class="btn btn-warning dropdown-toggle">
                        <i class="fa fa-gears"></i> 
                        O P T I O N S
                        <i class="caret"></i>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="javascript:arborescence.drawing.resizeCanvas()">
                                <i class="fa fa-arrows"></i> 
                                Redimensionner le canevas
                            </a>
                        </li>
                        <li>
                            <a href="javascript:arborescence.drawing.changeFont()">
                                <i class="fa fa-font"></i> 
                                Modifier la police de caractère
                            </a>
                        </li>
                        <li>
                            <a href="javascript:arborescence.drawing.switchSnapGridFlag()">
                                <i class="fa fa-anchor"></i> 
                                Activer/Désactiver les points d'ancrage
                            </a>
                        </li> 
                        <li>
                            <a href="javascript:arborescence.drawing.switchDrawGridFlag()">
                                <i class="fa fa-bars"></i> 
                                Afficher/Retirer la grille
                            </a>
                        </li>     
                    </ul>
                </div>
            </div>
            <br/>
            <!-- ========== C A N V A S // T O O L S ======================  -->
            <div class="text-center">
                <button id="tools" class="icon icon-paint unactive" 
                        onclick="javascript:arborescence.drawing.useTools()">
                </button>
                <span id="#drawTools#">  
                    <button id="relationAttenteTool" class="icon icon-attente unactive" 
                            onclick="javascript:arborescence.drawing.useToolArc()">
                    </button>
                    <button id="noteTool" class="icon icon-note unactive" 
                            onclick="javascript:arborescence.drawing.useToolNote()">
                    </button>
                    <button id="deleteTool" class="icon icon-delete unactive" 
                            onclick="javascript:arborescence.drawing.useToolDelete()">
                    </button>

                </span>
            </div>

            <!-- ========== M O D A L // C A N V A S // R E S I Z E ======== -->
            <div class="modal fade" id="resizeCanvasModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title color-default">
                                C A N E V A S . O P T I O N S
                            </h4>
                        </div>

                        <div class="modal-body">
                            <form action="javascript:arborescence.drawing.submitResizeCanvas();" 
                                    method="post" role="form" class="form-default">

                                <h2 class="form-default-heading color-default">Redimensionner le canevas</h2>
                                
                                <input class="form-control form-input" name="canvasWidth"
                                       type="number" placeholder="Largeur"></input>

                                <input class="form-control last-input" name="canvasHeight"
                                       type="number" placeholder="Hauteur"></input>
                                <br/>

                                <button type="submit" class="btn btn-success btn-block"> 
                                    <i class="fa fa-check"></i> Valider 
                                </button>                           
                            </form>
                        </div>

                        <div class="modal-footer">
                            <ul id="relations-errors" class="centered">
                            </ul>
                        </div>

                    </div>
                </div>
            </div>

            <!-- ========== M O D A L // C A N V A S // F O N T ============ -->
            <div class="modal fade" id="changeFontModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title  color-default">
                                C A N E V A S . O P T I O N S
                            </h4>
                        </div>

                        <div class="modal-body">
                            <form action="javascript:arborescence.drawing.submitChangeFont();" method="post" role="form" class="form-default">

                                <h2 class="form-default-heading color-default">Changer la police</h2>
                                <select id="tree-font" class="form-control form-input">
                                    <option value="Arial">Arial</option>
                                    <option value="Cambria">Cambria</option>
                                    <option value="Courier New">Courier New</option>
                                    <option value="Helvetica">Helvetica</option>
                                    <option value="Lucida Console">Lucida Console</option>
                                    <option value="Times New Roman">Times New Roman</option>
                                </select>
                                <input id="tree-font-size" class="form-control form-input" 
                                       type="number" name="font-size"
                                       step="0.5" min="8" max="16"
                                       placeholder="Taille police">            
                                </input>
                                <br/>

                                <button type="submit" class="btn btn-success btn-block"> 
                                    <i class="fa fa-check"></i> Valider 
                                </button>                           
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ========== M O D A L // T R E E // T I T L E ============== -->
            <div class="modal fade" id="editTreeTitleModal" role="dialog" aria-labelledby="relationModalLabel">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-description  color-default" id="">
                                A R B R E . O P T I O N S
                            </h4>
                        </div>

                        <div class="modal-body">
                            <form action="javascript:arborescence.drawing.submitEditTreeTitle();" method="post" role="form" class="form-default">
                                
                                <input class="form-control form-input" id="exerciseTitle" name="title"
                                       type="text" placeholder="Titre de l'arbre" autofocus></input>
                                <br/>
                                <span class="block">
                                    <input type="checkbox" id="treeBold" name="bold" value="bold">Bold 
                                    <input type="checkbox" id="treeItalic" name="italic" value="italic">Italique
                                </span>
                                <br/>
                                <button type="submit" class="btn btn-success btn-block"> 
                                    <i class="fa fa-check"></i> Valider 
                                </button>                           
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <!-- ========== M O D A L // E X // D E S C ============== -->
            <div class="modal fade" id="editExerciseDescModal" role="dialog" aria-labelledby="relationModalLabel">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title  color-default">
                                E X E R C I C E . O P T I O N S
                            </h4>
                        </div>

                        <div class="modal-body">
                            <form action="javascript:arborescence.drawing.submitEditExerciseDesc();" method="post" role="form" class="form-default">
                                <h2 class="form-default-heading color-default">Éditer l'énoncé de l'exercice</h2>
                                <textarea class="form-control form-input" id="exercise-description"
                                        rows="4" style="resize:none;" value="<?php echo $data['exercise']->description ?>"
                                    ></textarea>

                                <button type="submit" class="btn btn-success btn-block"> 
                                    <i class="fa fa-check"></i> Valider 
                                </button>                           
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            
            <!-- ========== M O D A L // T R E E / S A V E  ================ -->
            <div class="modal fade" id="saveModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title  color-default" id="relationModalLabel">
                                A R B R E . O P T I O N S
                            </h4>
                        </div>

                        <div class="modal-body">
                            <form action="javascript:arborescence.drawing.submitSaveExercise();" method="post" role="form" class="form-default">

                                <h2 class="form-default-heading color-default">Entrez le nom de l'arbre</h2>
                                <input class="form-control form-input" id="exercise-name" name="exercise-name" 
                                    value="<?php echo $data['exercise']->name ?>"
                                    type="text" placeholder="Nom de l'arbre" required autofocus>
                                <br/>
                                <button type="submit" class="btn btn-success"> 
                                    <i class="fa fa-save"></i> Sauvegarder
                                </button>
                                <button class="btn btn-danger" data-dismiss="modal"> 
                                    <i class="fa fa-times"></i> Cancel
                                </button>
                            </form>  
                        </div>
                    </div>
                </div>
            </div>
            <!-- ========== E N D // B U T T O N // O P T I O N S ========== -->

            <br/>

            <div id="saveStatus"></div>
            <div id="arbre">
                
                <canvas id="arbre-canvas"></canvas>

                <!-- ========== M O D A L // N O D E ======================= -->
                <div class="modal fade" id="nodeModal" role="dialog" 
                    aria-labelledby="nodeModalLabel" aria-hidden="true">

                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title color-node" id="nodeModalLabel">
                                    N O E U D . O P T I O N S
                                </h4>
                            </div>

                            <div class="modal-body">

                                <div role="tabpanel">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#nodeOpt" aria-controls="nodeOpt" role="tab" data-toggle="tab">
                                                Options
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#nodeAdvOpt" aria-controls="nodeAdvOpt" role="tab" data-toggle="tab">
                                                Options avancées
                                            </a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="nodeOpt">
                                            <form action="javascript:arborescence.drawing.submitEditNode();" method="post" role="form" class="form-default">

                                                <h2 class="form-default-heading color-node">Éditer un noeud</h2>
                                                <select id="node-fonction" class="form-control form-input">
                                                    <option value="">******</option>
                                                    <option value="Phrase">Phrase</option>
                                                    <option value="Sous-Phrase">Sous-Phrase</option>
                                                    <option value="Noyau">Noyau</option>
                                                    <option value="Prédicat">Prédicat</option>
                                                    <option value="P2">P2</option>
                                                    <option value="Dét.">Dét.</option>
                                                    <option value="Dét. Q.">Dét. Q</option>
                                                    <option value="Dét. Ca">Dét. Ca</option>
                                                    <option value="Dét. Enonc.">Dét. Enonc.</option>
                                                    <option value="Lig.">Lig.</option>
                                                    <option value="rel">rel</option>
                                                </select>

                                                <select id="node-si" class="form-control form-input">
                                                    <option value="******">******</option>
                                                    <option value="GP1">GP1</option>
                                                    <option value="GP1'">GP1'</option>
                                                    <option value="GP2">GP2</option>
                                                    <option value="GDN">GDN</option>
                                                    <option value="GDN'">GDN'</option>
                                                    <option value="GDN''">GDN''</option>
                                                    <option value="GDN'''">GDN'''</option>
                                                    <option value="GDN''''">GDN''''</option>
                                                    <option value="GDPron.">GDPron.</option>
                                                    <option value="GDAdj.">GDAdj.</option>
                                                    <option value="GDV">GDV</option>
                                                    <option value="GDAdv.">GDAdv.</option>
                                                    <option value="GDC">GDC</option>
                                                    <option value="nom">nom</option>
                                                    <option value="pronom">pronom</option>
                                                    <option value="Adj.">Adj.</option>
                                                    <option value="verbe">verbe</option>
                                                    <option value="Adv.">Adv.</option>
                                                    <option value="C.Sub.">C.Sub.</option>
                                                    <option value="C.Coord.">C.Coord.</option>
                                                    <option value="interjection">interjection</option>
                                                    <option value="Δ">Δ</option>
                                                    <option value="ϴ">ϴ</option>
                                                    <option value="Ø">Ø</option>

                                                    <option value="COORD.">COORD.</option>
                                                    <option value="JUXTA.C">JUXTA.C.</option>

                                                    <optgroup label="Autres">
                                                        <option value="N/Adj.">N/Adj.</option>
                                                        <option value="V/N">V/N</option>
                                                        <option value="V/Adj.">V/Adj.</option>
                                                        <option value="Adj./N">Adj./N</option>
                                                        <option value="Adj./Adv.">Adj./Adv.</option>
                                                        <option value="Adj./C">Adj./C</option>
                                                        <option value="Adj./Pron.">Adv./C</option>
                                                        <option value="Adv./N">Adv./N</option>
                                                        <option value="Adv./Adj.">Adv./Adj.</option>
                                                        <option value="Adv./C">Adv./C</option>
                                                        <option value="Adv./Pron">Adv./Pron.</option>
                                                        <option value="Adj./Pron">Adj./Pron.</option>
                                                        <option value="C.Sub./Pron.">C.Sub./Pron.</option>
                                                    </optgroup>
                                                </select>

                                                <input class="form-control form-input" name="terme"
                                                       type="text" placeholder="Terme">
                                                
                                                <input class="form-control last-input" id="nodeKey" name="key"
                                                       type="hidden">
                                                <br/>
                                                <button type="submit" class="btn btn-success btn-block">
                                                    <i class="fa fa-check"></i> Valider 
                                                </button>

                                            </form>

                                            <span class="center block">
                                                <button class="btn btn-primary" onclick="javascript:arborescence.drawing.addNodeOnNode();"> 
                                                    <i class="fa fa-plus"></i> N.
                                                </button>

                                                <button class="btn btn-info" onclick="javascript:arborescence.drawing.addLeftNodeAttenteOnNode();"> 
                                                    <i class="fa fa-plus"></i> R. Attente Gauche
                                                </button>

                                                <button class="btn btn-info" onclick="javascript:arborescence.drawing.addRightNodeAttenteOnNode();"> 
                                                    <i class="fa fa-plus"></i> R. Attente Droite
                                                </button>
                                            </span>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="nodeAdvOpt">
                                            <form action="javascript:arborescence.drawing.submitExerciseEditAdvNode();" 
                                                method="post" role="form" class="form-default" >
                                                (Modifiez ces valeurs uniquement si vous savez ce que vous faites)
                                                <br/>
                                                <div class="form-group">
                                                    <label for="node-font-size">Taille de la police de caractère du noeud</label>
                                                    <input id="node-font-size" class="form-control"
                                                        type="number" step="0.5" min="8" max="16" placeholder="Taille de police">
                                                </div>
                                                <div class="form-group">
                                                    <label>Position et affichage</label>
                                                    <input id="node-x-position" class="form-control"
                                                        type="number" placeholder="Position x du noeud">
                                                    <input id="node-y-position" class="form-control"
                                                        type="number" placeholder="Position y du noeud">
                                                    <input class="checkbox-inline float-left" id="node-value-visible" type="checkbox"> Afficher la valeur du noeud
                                                    <br/>
                                                    <input class="checkbox-inline float-left" id="node-children-visible" type="checkbox"> Afficher le développement du noeud
                                                </div>
                                                <input class="form-control last-input" id="node-adv-key"
                                                       type="hidden">
                                                
                                                <button type="submit" class="btn btn-success btn-block">
                                                    <i class="fa fa-check"></i> Valider 
                                                </button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
                <!-- ========== E N D // M O D A L // N O D E ============== -->

                <!-- ========== M O D A L // N O D E // D E L E T E ======== -->
                <div class="modal fade" id="nodeModalDelete" role="dialog">

                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title color-node" id="nodeModalLabel">
                                    N O E U D . S U P P R E S S I O N 
                                </h4>
                            </div>

                            <div class="modal-body">
                                <form action="javascript:arborescence.drawing.submitDeleteSubtree();" method="post" role="form" class="form-default">
                                    <h2 class="form-default-heading color-node">Supprimer un noeud</h2>
                                    Suppression définitive du noeud.
                                    <input class="form-control last-input" id="nodeKeyToDelete" name="key"
                                           type="hidden"></input>
                                    <br/>
                                    <button type="submit" class="btn btn-success btn-block">
                                        <i class="fa fa-check"></i> Valider 
                                    </button>

                                    <span class="center block">
                                    </span>
                                </form>
                            </div>

                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
                <!-- ========== E N D // M O D A L // N O D E ============== -->
                
                <!-- ========== M O D A L // N O D E // A T T E N T E======= -->
                <div class="modal fade" id="nodeAttenteModal" role="dialog" 
                    aria-labelledby="nodeModalLabel" aria-hidden="true">

                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title color-node">
                                    N O E U D . O P T I O N S
                                </h4>
                            </div>

                            <div class="modal-body">

                                <form action="javascript:arborescence.drawing.submitEditNodeAttente();" 
                                      method="post" role="form" class="form-default">

                                    <h2 class="form-default-heading color-node">Éditer un noeud</h2>

                                    <select id="node-attente-fonction" 
                                            class="form-control form-input">
                                        <option value="">******</option>
                                        <option value="Phrase">Phrase</option>
                                        <option value="Sous-Phrase">Sous-Phrase</option>
                                        <option value="Noyau">Noyau</option>
                                        <option value="Prédicat">Prédicat</option>
                                        <option value="P2">P2</option>
                                        <option value="Dét.">Dét.</option>
                                        <option value="Dét. Q.">Dét. Q</option>
                                        <option value="Dét. Ca">Dét. Ca</option>
                                        <option value="Dét. Enonc.">Dét. Enonc.</option>
                                        <option value="Lig.">Lig.</option>
                                        <option value="rel">rel</option>
                                    </select>

                                    <input class="form-control last-input" id="nodeAttenteKey" name="key"
                                           type="hidden" placeholder="Clef"></input>
                                    <br/>
                                    <button type="submit" class="btn btn-success btn-block">
                                        <i class="fa fa-check"></i> Valider 
                                    </button>

                                </form>

                                <span class="center block">
                                    <button class="btn btn-primary" onclick="javascript:arborescence.drawing.addNodeOnNode();"> 
                                        <i class="fa fa-plus"></i> N.
                                    </button>
                                </span>

                            </div>

                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
                <!-- ========== E N D // M O D A L // N O D E ============== -->

                <!-- ========== M O D A L // R E L A T I O N =============== -->
                <div class="modal fade" id="relationModal" role="dialog" 
                    aria-labelledby="relationModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title color-relation" id="relationModalLabel">
                                    R E L A T I O N . O P T I O N S
                                </h4>
                            </div>

                            <div class="modal-body">

                                <form action="javascript:arborescence.drawing.submitEditRelation();" method="post" role="form" class="form-default">

                                    <h2 class="form-default-heading color-relation">Éditer une relation</h2>

                                    <select id="relation-type" class="form-control form-input">
                                        <option value="0">Liaison</option>
                                        <option value="1">Détermination</option>
                                        <option value="2">Prédication</option>
                                        <option value="3">Détermination de l'énonciation</option>
                                    </select>
                                    <div class="form-group hidden">
                                        <input class="checkbox-inline float-left" id="relation-visible" type="checkbox"> Afficher la relation
                                        <br/>
                                        <input class="checkbox-inline float-left" id="relation-value-visible" type="checkbox"> Afficher la valeur de la relation
                                    </div>
                                    <input class="form-control" id="relationKey" name="relationKey"
                                           type="hidden" ></input>
                                    <br/>

                                    <button type="submit" class="btn btn-success btn-block"> 
                                        <i class="fa fa-check"></i> Valider 
                                    </button>   

                                </form>

                                <span class="center block">
                                    <button class="btn btn-primary" onclick="javascript:arborescence.drawing.addNodeOnRelation();">
                                        <i class="fa fa-plus"></i> Noeud
                                    </button>

                                    <!-- <button class="btn btn-danger" onclick="javascript:arborescence.drawing.submitDeleteRelationNode();">
                                        <i class="fa fa-minus"></i> N.
                                    </button> -->
                                </span>

                            </div>

                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
                <!-- ========== E N D // M O D A L // R E L A T I O N ====== -->

                <!-- ========== M O D A L // R E L A T I O N // D E L E T E = -->
                <div class="modal fade" id="relationModalDelete" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title color-relation">
                                    R E L A T I O N . S U P P R E S S I O N
                                </h4>
                            </div>

                            <div class="modal-body">

                                <form action="javascript:arborescence.drawing.submitDeleteRelation();" method="post" role="form" class="form-default">

                                    <h2 class="form-default-heading color-relation">Supprimer une relation</h2>

                                    Suppression définitive de la relation.
                                    <input class="form-control" id="relationKeyToDelete" name="relationKeyToDelete"
                                           type="hidden" ></input>
                                    <br/>

                                    <button type="submit" class="btn btn-success btn-block"> 
                                        <i class="fa fa-check"></i> Valider 
                                    </button>   

                                </form>

                                <span class="center block">
                                </span>

                            </div>

                            <div class="modal-footer">
                            </div>

                        </div>
                    </div>
                </div>
                <!-- ========== E N D // M O D A L // R E L A T I O N ====== -->

                <!-- ========== M O D A L // R E L A T I O N // A T T ====== -->
                <div class="modal fade" id="relationAttModal" role="dialog" 
                    aria-labelledby="relationModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title color-relation" id="relationModalLabel">
                                    R E L A T I O N . A T T E N T E . O P T I O N S
                                </h4>
                            </div>

                            <div class="modal-body">

                                <form action="javascript:arborescence.drawing.submitEditRelationAtt();" method="post" role="form" class="form-default">

                                    <h2 class="form-default-heading color-relation">Éditer une relation d'attente</h2>

                                    <select id="relationAtt-type" class="form-control form-input">
                                        <option value="4">Relation d'attente (Dét.)</option>
                                        <option value="5">Relation d'attente (Préd.)</option>
                                    </select>
                                    <input id="relationAtt-key" class="form-control"
                                           type="hidden" ></input>
                                    <br/>

                                    <button type="submit" class="btn btn-success btn-block"> 
                                        <i class="fa fa-check"></i> Valider 
                                    </button>   

                                </form>

                                <span class="center block">
                                    <button class="btn btn-primary" onclick="javascript:arborescence.drawing.addNodeOnRelation();">
                                        <i class="fa fa-plus"></i> Noeud
                                    </button>
                                </span>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- ========== E N D // R E L A T I O N // A T T ========== -->

                <!-- ========== M O D A L // D R A W N // O B J E C T ====== -->
                <div class="modal fade" id="drawnObjectModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title color-default" id="objectModalLabel">
                                    D E S S I N . O P T I O N S
                                </h4>
                            </div>

                            <div class="modal-body">
                                <form action="javascript:arborescence.drawing.submitEditDrawnObject();" 
                                        method="post" role="form" class="form-default">

                                    <h2 class="form-default-heading color-default">Éditer le dessin</h2>
                                    <select id="drawnObjectType" class="form-control form-input hidden" disabled="disabled">
                                        <option value="NODE">Node</option>
                                        <option value="RELATION">Relation</option>
                                        <option value="RANDOM">Random</option>
                                    </select>
                                    <select id="drawnObjectRelationType" class="form-control form-input">
                                        <option value="1">Glissement avec changement de fonction</option>
                                        <option value="2">Glissement de la relation d'attente à la relation effective</option>
                                    </select>
                                    <input type="hidden" name="drawnObjectKey"></input>
                                    <br/>

                                    <button type="submit" class="btn btn-success btn-block"> 
                                        <i class="fa fa-check"></i> Valider 
                                    </button>                           
                                </form>
                                <span class="center block">
                                    <!-- <button class="btn btn-danger" onclick="javascript:arborescence.drawing.submitDeleteDrawnObject();"> 
                                        <i class="fa fa-save"></i> Supp. le dessin
                                    </button> -->
                                </span>
                            </div>

                            <div class="modal-footer">
                                <ul id="relations-errors" class="centered">
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- ========== E N D // D R A W N // O B J E C T ========== -->

                <!-- ========== M O D A L // D R A W N // O B J E C T ====== -->
                <div class="modal fade" id="drawnObjectModalDelete" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title color-default">
                                    D E S S I N . S U P P R E S S I O N
                                </h4>
                            </div>

                            <div class="modal-body">
                                <form action="javascript:arborescence.drawing.submitDeleteDrawnObject();" 
                                        method="post" role="form" class="form-default">

                                    <h2 class="form-default-heading color-default">Suppression d'un objet</h2>
                                    Suppression définitive de l'objet.
                                    <input type="hidden" id='drawnObjectKeyToDelete' name="drawnObjectKey"></input>
                                    <br/>

                                    <button type="submit" class="btn btn-success btn-block"> 
                                        <i class="fa fa-check"></i> Valider 
                                    </button>                           
                                </form>
                                <span class="center block">
                                </span>
                            </div>

                            <div class="modal-footer">
                                <ul id="relations-errors" class="centered">
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- ========== E N D // D R A W N // O B J E C T ========== -->

                <!-- ========== M O D A L // N O T E ======================= -->
                <div class="modal fade" id="noteModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title color-default" id="noteLabel">
                                    N O T E . O P T I O N S
                                </h4>
                            </div>

                            <div class="modal-body">
                                <form action="javascript:arborescence.drawing.submitEditNote();" 
                                        method="post" role="form" class="form-default">

                                    <h2 class="form-default-heading color-default">Éditer la note</h2>
                                    <!-- <input class="form-control form-input" id="note-value"
                                                       type="text" placeholder="Note"></input> -->
                                    <textarea class="form-control form-input" id="note-value"
                                        rows="4" style="resize:none;" value=""
                                    ></textarea>
                                    <div class="form-group hidden">
                                        <label>Position</label>
                                        <input id="note-x-position" class="form-control"
                                            type="number" placeholder="Position x du noeud">
                                        <input id="note-y-position" class="form-control"
                                            type="number" placeholder="Position y du noeud">
                                    </div>

                                    <div class="form-group">
                                        <label>Longueur ligne</label>
                                        <input id="note-width" class="form-control"
                                            type="number" step="1" placeholder="Line width">
                                    </div>

                                    <div class="form-group hidden">
                                        <label for="note-font-size">Taille de la police de caractère</label>
                                        <input id="note-font-size" class="form-control"
                                            type="number" step="0.5" min="8" max="16" placeholder="Taille de police">
                                    </div>

                                    <input type="hidden" id="note-key"></input>
                                    <br/>

                                    <button type="submit" class="btn btn-success btn-block"> 
                                        <i class="fa fa-check"></i> Valider 
                                    </button>                           
                                </form>
                            </div>

                            <div class="modal-footer">
                                <ul id="relations-errors" class="centered">
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- ========== E N D // N O T E =========================== -->
                
            </div> 
                        
        </div>

    </div>

</div>

<?php
    if($data['treeLoaded'])
    {
        $exercise           = $data['exercise'];
        $treeTags           = $data['treeTags'];
?>
        <script>
            var json = 
            {
              nodes         : <?php echo json_encode($exercise->jsonNodes) ?>,
              relations     : <?php echo json_encode($exercise->jsonRelations) ?>,
              notes         : <?php echo json_encode($exercise->jsonNotes) ?>,
              drawnObjects  : <?php echo json_encode($exercise->jsonDrawnObjects) ?>,
              treeTags      : <?php echo json_encode($treeTags) ?>
            };
            arborescence.init(json);
        </script>
<?php
    } else
    {
?>
        <script>
            arborescence.init();
        </script>
<?php
    }
?>

<script>
    activateMenuItem('adminExercise', 'adminItems');
    window.onbeforeunload = function()
    {
        if (!arborescence.isSaved)
            return 'Voulez vous quitter cette page ?';
    };
</script>