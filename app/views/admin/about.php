<?php
use \core\error;
?>

<div class="bodyWrapper">

	<div class="bodyMargin">
		
		<div class="bodyContent">

			<div id="slide_account" class="slide">
				<h4>Informations</h4>
				<hr/>
				<div> 
					<label class="label label-info label-title">Annonce</label>
					<div class="container-sm">
						<i class="fa fa-caret-right"></i> Une annonce administrateur a besoin d'un <b>titre</b> 
						(max. 20 caractères)
						<br/>
						<i class="fa fa-caret-right"></i> Ainsi que du <b>corps de l'annonce</b> (max. 128 caractères)
					</div>
					
					<label class="label label-info label-title">Recherche</label>
					<div class="container-sm">
						<i class="fa fa-caret-right"></i> Recherche d'utilisateur/arbre connaissant (partiellement) le nom
						<br/>
						<i class="fa fa-caret-right"></i> Recherche d'utilisateur/arbre connaissant (partiellement) l'e-mail de l'utilisateur
						<br/>
						<i class="fa fa-caret-right"></i> Recherche d'utilisateur/arbre connaissant un tag lié à l'utilisateur/arbre
					</div>

					<label class="label label-info label-title">Exercice</label>
					<div class="container-sm">
						<i class="fa fa-caret-right"></i> Créer l'arbre (la solution) normalement
						<br/>
						<i class="fa fa-caret-right"></i> Rajouter les options de visibilité sur les noeuds
						<br/>
						<i class="fa fa-caret-right"></i> Sauvegarder
					</div>
				</div>
			</div>

		</div>

	</div>

</div>

<script>
	activateMenuItem('adminAbout', 'adminItems');
</script>
