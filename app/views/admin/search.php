<?php
use \core\error;
?>

<div class="bodyWrapper">

	<div class="bodyMargin">
		
		<div class="bodyContent">

			<div id="slide_account" class="slide">
				<h4>Outil de recherche administrateur</h4>
				<hr/>
				<div> 
					<ul class="nav nav-tabs" role="tablist" id="searchTab">
					  	<li role="presentation">
					  		<a href="#user" aria-controls="user" role="tab" data-toggle="tab">Rechercher un utilisateur</a>
					  	</li>
			  			<li role="presentation">
			  				<a href="#tree" aria-controls="tree" role="tab" data-toggle="tab">Rechercher un arbre</a>
			  			</li>
					</ul>

					<div class="tab-content">
				  		<div role="tabpanel" class="tab-pane" id="user">
				  			<form role="form" method="POST" class="form-default-lg">
								<h4>Rechercher un utilisateur</h4>

								<!-- ----- NOM ----------------------------- -->
						        <input type="text" name="userUsername" 
						               value="" id="userUsername"
						               class="form-control" placeholder="Nom de l'utilisateur"
						               autofocus>
						        <!-- ----- EMAIL --------------------------- -->
						        <input type="text" name="userUserEmail" 
						               value=""
						               class="form-control" placeholder="Adresse de courriel de l'utilisateur">
						        <!-- ----- TAGS ---------------------------- -->
						        <input type="text" name="userUserTag" 
						               value=""
						               class="form-control" placeholder="Mot clé">

						        <br />

						        <!-- ----- SUBMIT -------------------------- -->
						        <span>
						        	<a href="javascript:adminSearchUser();" class="btn btn-lg btn-success">
						        		<i class="fa fa-search"></i> Rechercher 
						        	</a>
							        <button type="reset" name="submit" value="searchUser" class="btn btn-lg btn-danger">
							            <i class="fa fa-refresh"></i> Réinitialiser
							        </button>
						        </span>
						    </form>
				  		</div>
				  		<div role="tabpanel" class="tab-pane" id="tree">
				  			<form role="form" method="POST" class="form-default-lg">
								<h4>Rechercher un arbre</h4>

								<!-- ----- NOM ----------------------------- -->
						        <input type="text" name="treeTreeName" 
						               value=""
						               class="form-control" placeholder="Nom de l'arbre"
						               autofocus>
						        <!-- ----- EMAIL --------------------------- -->
						        <input type="text" name="treeUserEmail" 
						               value=""
						               class="form-control" placeholder="Adresse de courriel de l'utilisateur">
						        <!-- ----- TAGS ---------------------------- -->
						        <input type="text" name="treeTreeTag" 
						               value=""
						               class="form-control" placeholder="Mot clé"
						               >

						        <br />

						        <!-- ----- SUBMIT -------------------------- -->
						        <span>
						        	<a href="javascript:adminSearchTree();" class="btn btn-lg btn-success">
						        		<i class="fa fa-search"></i> Rechercher 
						        	</a>
							        <button type="reset" name="submit" value="searchTree" class="btn btn-lg btn-danger">
							            <i class="fa fa-refresh"></i> Réinitialiser
							        </button>
							    </span>
						    </form>
				  		</div>
					</div>
					
				</div>
			</div>

		</div>

	</div>

</div>

<script>
	 $(function () {
	    $('#searchTab a:last').tab('show')
	  })
	activateMenuItem('adminSearch', 'adminItems');
</script>