<div class="container center">
	<img src="<?php echo helpers\url::template_path()?>images/logo_160.png">
	<h1 style="letter-spacing:8px;">ARBORESCENCE</h1>
	<h2>404</h2>

	<hr />

	<h3>The page you were looking for could not be found</h3>
	<p>This could be the result of the page being removed, the name being changed or the page being temporarily unavailable</p>
	<h3>Troubleshooting</h3>

	<ul>
	  <li>If you spelled the URL manually, double check the spelling</li>
	  <li>Go to our website's home page, and navigate to the content in question</li>
	</ul>  
</div>
