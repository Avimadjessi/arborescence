<?php
use \core\error;
?>
<div class="bodyWrapper">

	<div class="bodyMargin">

		<div class="bodyContent">
			<!-- ---------- L O G I N . F O R M ------------------------------------ -->
			<div id="slide_edit" class="slide">
				<h4>Modification de votre image de profil</h4>
				<hr/>
				<?php
					$errors = $data['failure'];
					if($errors)
					{
				?>
						<div class="alert alert-danger center">
							<?php
								foreach ($errors as $key => $value) 
								{
								 	echo $errors[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<?php
					$success = $data['success'];
					if($success)
					{
				?>
						<div class="alert alert-success center">
							<?php
								foreach ($success as $key => $value) 
								{
								 	echo $success[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<div class="center">
					<form role="form" class="form-default-lg"
						method="POST" enctype="multipart/form-data"
						action="<?php echo DIR.'profile/edit-image' ?>">
						(Taille minimum recommandée : 80x80px // Maximum : 5Mb)
						<br/>
						<br/>
						<input id="profile-image" type="file" accept="image/*" name="profile-image">
					</form>
				</div>
			</div>

		</div>

	</div>

</div>

<script>
	activateMenuItem('editProfileImage', 'accountItems');

	/* Initialize your widget via javascript as follows */
	$("#profile-image").fileinput({	  
		showCaption		: false,
		showUpload		: true,
	    overwriteInitial: true,
		previewFileType	: "image",
		browseClass		: "btn btn-primary",
		browseLabel		: "Choisir une nouvelle image",
		browseIcon		: '<i class="fa fa-file-picture-o"></i>',
		removeClass		: "btn btn-danger",
		removeLabel		: "Supprimer",
		removeIcon		: '<i class="fa fa-trash"></i>',
		uploadClass		: "btn btn-info",
		uploadLabel		: "Uploader",
		uploadIcon		: '<i class="fa fa-upload"></i>',
		maxFileSize		: 5000,
		slugCallback	: function(filename) {
            return filename.replace('(', '_').replace(']', '_');
        }
	});
</script>
