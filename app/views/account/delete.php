<?php
use \core\error;
?>
<div class="bodyWrapper">

	<div class="bodyMargin">

		<div class="bodyContent">

			<!-- ---------- L O G I N . F O R M ------------------------------------ -->
			<div id="slide_account" class="slide">
				<h4 class="color-red">Suppression de votre compte</h4>
				<hr/>
				<div class="center"> 
					<form role="form" method="POST" class="form-default-lg">
						Suppression <small><small>(presque)</small></small> <b class="color-red">définitive</b> de votre compte!
						<br/>
						<br/>
						<!-- SUBMIT -->
				        <button type="submit" name="submit" value="edit" class="btn btn-lg btn-danger">
				            <span class="fa fa-times"></span> Supprimer
				        </button>
					</form>
				</div>
			</div>

		</div>

	</div>
	
</div>

<script>
	activateMenuItem('deleteProfile', 'accountItems');
</script>