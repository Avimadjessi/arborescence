<?php
use \core\error,
	\helpers\date;
?>
<div class="bodyWrapper">

	<div class="bodyMargin">

		<div class="bodyContent">

			<div id="slide_trees" class="slide">

				<h4>Liste de vos arbres</h4>
				<hr/>

				<a href="<?php echo DIR?>trees/canvas" class="btn btn-primary">
					<i class="fa fa-plus"></i>
					Créer un arbre
				</a>
				<br/>
				<br/>
				<?php
					$errors = $data['failure'];
					if($errors)
					{
				?>
						<div class="alert alert-danger center">
							<?php
								foreach ($errors as $key => $value) 
								{
								 	echo $errors[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<?php
					$success = $data['success'];
					if($success)
					{
				?>
						<div class="alert alert-success center">
							<?php
								foreach ($success as $key => $value) 
								{
								 	echo $success[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<div class="treeList">
				<?php
				$treeList = $data['treeList'];
				if ($treeList)
				{
				?>
					<table id="user-trees-table" class="table">
						<thead>
							<tr>
								<th class="no_sorting"></th>
								<th>Nom</th>
								<th>Auteur</th>
								<th>Dernière Modification</th>
								<th class="center no_sorting">Partager</th>
								<th class="center no_sorting color-red danger"><i class="fa fa-exclamation-triangle"></i></th>
							</tr>
						</thead>
						<tbody>
						<?php
							foreach ($treeList as $tree) 
							{
								$popoverContent = '<b>Création</b> : '.Date::verbose($tree->created_at).'<br/>'
													. '<b>Auteur</b> : '.$tree->fullname ;
								$row_color 		= '';

								if($tree->fullname != $tree->lastUserFullname)
								{
									$popoverContent .= '<br/><span class=\'color-node\'><b>Envoyé par</b> : '.$tree->lastUserFullname.'</span>'
										.'<br/>'.htmlentities($tree->message);
									$row_color = "info";
								}
								if($tree->exercise)
								{
									$row_color = "warning";
								}
						?>
								<tr class="<?php echo $row_color?>">
									<td>
										<a href="#" tabindex="0" role="button" data-container="body" 
											data-toggle="popover" data-trigger="focus" data-placement="left" 
											title="<?php echo $tree->name ?>"
											data-content="<?php echo $popoverContent ?>">
											<i class="fa fa-question-circle">
											</i>
										</a> 
									</td>
									<td class="text-left"><a href="<?php echo DIR.'trees/'.$tree->slug.'/canvas';?>"><?php echo $tree->name; ?></a></td>
									<td class="text-left"><?php echo $tree->lastUserFullname ?></td>
									<td class="text-left"><?php echo Date::verbose($tree->updated_at) ?></td>
									<td>
										<div class="modal fade" id="share-tree-<?php echo $tree->id; ?>" role="dialog" 
								            aria-labelledby="relationModalLabel" aria-hidden="true">
									        <div class="modal-dialog">
									            <div class="modal-content">
									                <div class="modal-header">
									                    <button type="button" class="close" data-dismiss="modal">
									                        <span aria-hidden="true">&times;</span>
									                        <span class="sr-only">Close</span>
									                    </button>
									                    <h4 class="modal-title color-default" id="relationModalLabel">
									                        P A R T A G E . A R B R E
									                    </h4>
									                </div>

									                <div class="modal-body">
									                	<form role="form" action="javascript:shareTree(<?php echo $tree->id; ?>);" class="form-default">

						                                    <label id="share-message-name" class="label label-default share-message"><?php echo $tree->name; ?></label>
						                                    <label id="share-message-success" class="label label-success share-message disabled">
						                                    	<i class="fa fa-check-circle"></i> Arbre envoyé. 
						                                    </label>
						                                    <label id="share-message-danger-user" class="label label-danger share-message disabled">
						                                    	<i class="fa fa-times-circle"></i> Aucun utilisateur ne s'est enregistré avec cette adresse. 
						                                    </label>
						                                    <label id="share-message-danger-tree" class="label label-danger share-message disabled">
						                                    	<i class="fa fa-times-circle"></i> 
						                                    	L'utilisateur possède déjà cet arbre.
						                                    	Changez le nom de l'arbre si vous désirez tout de le même l'envoyer.
						                                    </label>
						                                    <br/><br/>

						                                    <input id="share-email-<?php echo $tree->id; ?>" class="form-control form-input disabled" 
							                                       type="email" name="share-email"
							                                       placeholder="Adresse de courriel de l'utilisateur">            
							                                </input>

							                                <textarea id="share-message-<?php echo $tree->id; ?>" class="form-control last-input"
							                                		type="text" value="" rows="2" maxlength="148" style="resize:none" 
							                                		placeholder="Message" required></textarea>
							                                <input id="share-slug-<?php echo $tree->id; ?>" type="hidden" value="<?php echo $tree->slug; ?>"></input>
							                                
							                                <br/>
							                                <span class="center block">
								                                <button type="submit" class="btn btn-success btn-block"> 
								                                    <i class="fa fa-send"></i> Partager 
								                                </button>
								                           	</span>
                       
						                                </form>
									                </div>

									                <div class="modal-footer"></div>
						            			</div>
						        			</div>
										</div>
										<a href="javascript:showModal('share-tree-<?php echo $tree->id; ?>');"><i class="fa fa-send"></i></a> 
									</td>
									<td class="danger">
										<div class="modal fade" id="delete-tree-<?php echo $tree->id; ?>" role="dialog" 
								            aria-labelledby="relationModalLabel" aria-hidden="true">
									        <div class="modal-dialog">
									            <div class="modal-content">
									                <div class="modal-header">
									                    <button type="button" class="close" data-dismiss="modal">
									                        <span aria-hidden="true">&times;</span>
									                        <span class="sr-only">Close</span>
									                    </button>
									                    <h4 class="modal-title color-default" id="relationModalLabel">
									                        A R B R E . O P T I O N S
									                    </h4>
									                </div>

									                <div class="modal-body">
								                        Vous êtes sur le point de <b>supprimer <b/>
								                        <br/>
								                        <h3 class="color-green"><?php echo $tree->name; ?></h3>
								                        <br/>

								                        <a href="<?php echo DIR.'trees/'.$tree->slug.'/delete';?>" class="btn btn-danger btn-block"> 
								                            <i class="fa fa-check"></i> Supprimer 
								                        </a>
									                </div>

									                <div class="modal-footer"></div>
						            			</div>
						        			</div>
										</div>
										<a href="javascript:showModal('delete-tree-<?php echo $tree->id; ?>');">
											<i class="fa fa-remove color-red"></i>
										</a> 
									</td>
								</tr>
						<?php
							}
						?>
						</tbody>
						<tfoot>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td class="color-red"></td>
							</tr>
						</tfoot>
					</table>
				<?php
				} else
				{
				?>
					<p>Aucun arbre trouvé. 
						<small>
							<a href="<?php echo DIR?>trees/canvas">
								Créer un arbre maintenant.
							</a>
						</small>
					</p>
				<?php
				} 
				?>
				</div>

				<br/>
				<h4>Liste des exercices</h4>
				<hr/>
				<div class="exerciseList">
				<?php
				$exerciseList = $data['exerciseList'];
				if ($exerciseList)
				{
				?>
					<table class="table">
						<thead>
							<tr>
								<th>Nom</th>
								<th>Dernière Modification</th>
								<?php if($data['user']->level == 9)
								{ ?>
								<th class="center">Partager</th>
								<?php } ?>
								<th class="center">Démarrer</th>
								<th class="center color-red danger"><i class="fa fa-exclamation-triangle"></i></th>
							</tr>
						</thead>
						<tbody>
						<?php
							foreach ($exerciseList as $exercise) 
							{
						?>
								<tr>
									<td class="text-left">
										<?php 
										if($data['user']->level == 9) 
											echo "<a href='".DIR."admin/exercise/".$exercise->slug."/canvas'>".$exercise->name."</a>";
										else
											echo $exercise->name;
											?>
									</td>
									<td class="text-left"><?php echo Date::verbose($exercise->updated_at) ?></td>
									<?php 
									if($data['user']->level >= 9) 
									{ ?>
									<td>
										<div class="modal fade" id="share-exercise-<?php echo $exercise->id; ?>" role="dialog" 
								            aria-labelledby="relationModalLabel" aria-hidden="true">
									        <div class="modal-dialog">
									            <div class="modal-content">
									                <div class="modal-header">
									                    <button type="button" class="close" data-dismiss="modal">
									                        <span aria-hidden="true">&times;</span>
									                        <span class="sr-only">Close</span>
									                    </button>
									                    <h4 class="modal-title color-default" id="relationModalLabel">
									                        P A R T A G E . E X E R C I C E
									                    </h4>
									                </div>

									                <div class="modal-body">
					                                    <div id="share-exercise-message"></div>

											  			<form role="form" action="javascript:shareExercise(<?php echo $exercise->id; ?>);">
															<?php
																$userList = $data['userList'];
																if ($userList)
																{
															?>
																	<table id="share-users-table" class="table">
																		<thead>
																			<tr>
																				<th class="center"><input type="checkbox" name="th-user-select" value="all-user"></th>
																				<th>Nom</th>
																				<th>Email</th>
																			</tr>
																		</thead>
																		<tbody>
																		<?php
																		foreach ($userList as $user) 
																		{
																		?>
																			<tr class="">
																				<td><input type="checkbox" name="user-select" value="<?php echo $user->id; ?>"></td>
																				<td class="text-left"><?php echo $user->fullname; ?></a></td>
																				<td class="text-left"><?php echo $user->email ?></td>
																			</tr>
																		<?php
																		}	
																		?>
																		</tbody>
																		<tfoot>
																		</tfoot>
																	</table>
															<?php
																} else
																{
																	echo 'Erreur lors du chargement des utilisateurs.';
																}
															?>
															<br/>
							                                <span class="center block">
								                                <button type="submit" class="btn btn-success"> 
								                                    <i class="fa fa-send"></i> Partager 
								                                </button>
								                           	</span>
													    </form>
													</div>
						            			</div>
						        			</div>
										</div>
										<a href="javascript:showModal('share-exercise-<?php echo $exercise->id; ?>');"><i class="fa fa-send"></i></a> 
									</td>
									<?php } ?>
									<td>
										<!-- ========== M O D A L // B E G I N / E X E R C I S E / S A V E  ================ -->
							            <div class="modal fade" id="begin-exercise-<?php echo $exercise->id; ?>" role="dialog">
							                <div class="modal-dialog">
							                    <div class="modal-content">

							                        <div class="modal-header">
							                            <button type="button" class="close" data-dismiss="modal">
							                                <span aria-hidden="true">&times;</span>
							                                <span class="sr-only">Close</span>
							                            </button>
							                            <h4 class="modal-title  color-default" id="relationModalLabel">
							                                E X E R C I C E . O P T I O N S
							                            </h4>
							                        </div>

							                        <div class="modal-body">
							                            <form action="<?php echo DIR.'trees/exercise/start';?>" method="post" role="form" class="form-default">

							                                <h2 class="form-default-heading color-default">Entrez le nom de la sauvegarde</h2>
							                                <input class="form-control form-input" id="solutionName" name="solutionName" 
							                                    value="<?php echo $exercise->name ?>"
							                                    type="text" placeholder="Nom de l'arbre" required autofocus>
							                                <input id="exercise-id" name="exerciseId" value="<?php echo $exercise->id ?>" type="hidden">
							                                <br/>
							                                <button type="submit" class="btn btn-success"> 
							                                    <i class="fa fa-save"></i> Démarrer
							                                </button>
							                                <button class="btn btn-danger" data-dismiss="modal"> 
							                                    <i class="fa fa-times"></i> Annuler
							                                </button>
							                            </form>  
							                        </div>
							                    </div>
							                </div>
							            </div>
							            <!-- ========== E N D // B E G I N // E X E R C I S E ========== -->
										<!-- <a href="<?php echo DIR.'trees/exercise/'. $exercise->slug .'/canvas'?>"> -->
										<a href="javascript:showModal('begin-exercise-<?php echo $exercise->id ?>');">
											<i class="fa fa-play color-green"></i>
										</a> 
									</td>
									<td class="danger">
										<div class="modal fade" id="delete-exercise-<?php echo $exercise->id; ?>" role="dialog" 
								            aria-labelledby="relationModalLabel" aria-hidden="true">
									        <div class="modal-dialog">
									            <div class="modal-content">
									                <div class="modal-header">
									                    <button type="button" class="close" data-dismiss="modal">
									                        <span aria-hidden="true">&times;</span>
									                        <span class="sr-only">Close</span>
									                    </button>
									                    <h4 class="modal-title color-default" id="relationModalLabel">
									                        E X E R C I C E . O P T I O N S
									                    </h4>
									                </div>

									                <div class="modal-body">
								                        Vous êtes sur le point de <b>supprimer <b/>
								                        <br/>
								                        <h3 class="color-green"><?php echo $exercise->name; ?></h3>
								                        <br/>

								                        <a href="<?php echo DIR.'admin/exercise/'.$exercise->slug.'/delete';?>" class="btn btn-danger btn-block" disabled> 
								                            <i class="fa fa-check"></i> Supprimer 
								                        </a>
									                </div>

									                <div class="modal-footer"></div>
						            			</div>
						        			</div>
										</div>
										<!-- ----- E N D . M O D A L ----------- -->
										<a href="javascript:showModal('delete-exercise-<?php echo $exercise->id; ?>');">
											<i class="fa fa-remove color-red"></i>
										</a> 
									</td>
								</tr>
						<?php
							}
						?>
						</tbody>
						<tfoot>
							<tr>
								<td></td>
								<td></td>
								<?php 
								if($data['user']->level == 9) 
								{ ?>
								<td></td>
								<?php } ?>
								<td></td>
								<td class="color-red"></td>
							</tr>
						</tfoot>
					</table>
				<?php
				} else
				{
				?>
					<p>Aucun exercice trouvé.</p>
				<?php
				} 
				?>
				</div>
			</div>

		</div>

	</div>

</div>

<script>
	$(function () {
		// --- Init DataTables
	    var userTreesTable = $('#user-trees-table').DataTable({
	    	"lengthMenu": [[5, 10, 25, -1], [5, 10, 25, "Tout"]],
	    	"language": 
	    	{
	    		processing:     "Traitement en cours...",
		        search:         "Rechercher&nbsp;:",
		        lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
		        info:           "Page _PAGE_ sur _PAGES_",
		        infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
		        infoFiltered:   "(_END_ filtr&eacute; sur _MAX_ &eacute;l&eacute;ments au total)",
		        infoPostFix:    "",
		        loadingRecords: "Chargement en cours...",
		        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
		        emptyTable:     "Aucune donnée disponible dans le tableau",
		        paginate: {
		            first:      "Premier",
		            previous:   "Pr&eacute;c&eacute;dent",
		            next:       "Suivant",
		            last:       "Dernier"
		        },
		        aria: {
		            sortAscending:  ": activer pour trier la colonne par ordre croissant",
		            sortDescending: ": activer pour trier la colonne par ordre décroissant"
		        }
	        }
		});
	    // $('#share-users-table').DataTable();

	    // --- Disable unwanted column ordering
    	disableTableSorting();
	    $('#user-trees-table').on( 'draw.dt', function () {
    		// $('#myInput').val( json.lastInput );
    		disableTableSorting();
		});

		// --- Check box
		$('input[name="th-user-select"]').change(function() 
		{
		    if($(this).is(":checked") )
		    {
		    	$('input[name="user-select"]').each(function()
		    	{
				 	$(this).prop('checked', true);
				});
		    } else
		    {
		    	$('input[name="user-select"]').each(function()
		    	{
				 	$(this).prop('checked', false);
				});
		    }
		});

	    // --- Popover ---------------------------------------------------------
		$('[data-toggle="popover"]').popover({html : true});

		// --- Side menu activation
		activateMenuItem('trees', 'treesItems');
	});
</script> 