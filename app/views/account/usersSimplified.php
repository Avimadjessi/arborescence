<?php
use \core\error,
	\helpers\date;
?>
<div class="bodyContentModal">

	<div>

		<label><?php echo $data['searchCriteria']; ?></label>
		<p>
			<div>
			<?php
				$userList = $data['userList'];
				if ($userList)
				{
			?>
					<table class="table">
						<thead>
							<tr>
								<th>Nom</th>
								<th>Email</th>
								<th>Date d'inscription</th>
								<!-- <th>Arbres</th> -->
							</tr>
						</thead>
						<tbody>
						<?php
							foreach ($userList as $user) 
							{
						?>
								<tr class="">
									<td class="text-left"><?php echo $user->fullname; ?></a></td>
									<td class="text-left"><?php echo $user->email ?></td>
									<td class="text-left"><?php echo Date::verbose($user->created_at) ?></td>
									<!-- <td class="right"><?php echo '0'?></td> -->
								</tr>
						<?php
							}
						?>
						</tbody>
						<tfoot>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<!-- <td></td> -->
							</tr>
						</tfoot>
					</table>
			<?php
				} else
				{
			?>
					<p>Aucun utilisateur trouvé avec ces critères de recherche
					</p>
			<?php
				} 
			?>
			</div>
		</p>
	</div>

</div>