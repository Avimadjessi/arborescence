<?php
use \core\error,
	\helpers\date;
?>
<div class="bodyWrapper">

	<div class="bodyMargin">

		<div class="bodyContent">

			<div id="slide_trees" class="slide">

				<h4>Liste de vos arbres // <?php echo $data['sub-title']; ?></h4>
				<hr/>

				<a href="<?php echo DIR?>trees/canvas" class="btn btn-primary">
					<i class="fa fa-plus"></i>
					Créer un arbre
				</a>
				<br/>
				<br/>
				<?php
					$errors = $data['failure'];
					if($errors)
					{
				?>
						<div class="alert alert-danger center">
							<?php
								foreach ($errors as $key => $value) 
								{
								 	echo $errors[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<?php
					$success = $data['success'];
					if($success)
					{
				?>
						<div class="alert alert-success center">
							<?php
								foreach ($success as $key => $value) 
								{
								 	echo $success[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<p>
					<div>
					<?php
						$treeList = $data['treeList'];
						if ($treeList)
						{
					?>
							<table class="table">
								<thead>
									<tr>
										<th></th>
										<th>Nom</th>
										<th>Auteur</th>
										<th>Dernière Modification</th>
										<th class="center">Partager</th>
										<th class="center color-red danger"><i class="fa fa-exclamation-triangle"></i></th>
									</tr>
								</thead>
								<tbody>
								<?php
									foreach ($treeList as $tree) 
									{
										$popoverContent = '<b>Création</b> : '.Date::verbose($tree->created_at).'<br/>'
															. '<b>Auteur</b> : '.$tree->fullname ;
										$row_color 		= '';

										if($tree->fullname != $tree->lastUserFullname)
										{
											$popoverContent .= '<br/><span class=\'color-node\'><b>Envoyé par</b> : '.$tree->lastUserFullname.'</span>';
											$row_color = "info";
										}
								?>
										<tr class="<?php echo $row_color?>">
											<td>
												<a href="#" tabindex="0" role="button" data-container="body" 
													data-toggle="popover" data-trigger="focus" data-placement="left" 
													title="<?php echo $tree->name ?>"
													data-content="<?php echo $popoverContent ?>">
													<i class="fa fa-question-circle">
													</i>
												</a> </td>
											<td class="text-left"><a href="<?php echo DIR.'trees/'.$tree->slug.'/canvas';?>"><?php echo $tree->name; ?></a></td>
											<td class="text-left"><?php echo $tree->lastUserFullname ?></td>
											<td class="text-left"><?php echo Date::verbose($tree->updated_at) ?></td>
											<td>
												<div class="modal fade" id="share-tree-<?php echo $tree->id; ?>" role="dialog" 
										            aria-labelledby="relationModalLabel" aria-hidden="true">
											        <div class="modal-dialog">
											            <div class="modal-content">
											                <div class="modal-header">
											                    <button type="button" class="close" data-dismiss="modal">
											                        <span aria-hidden="true">&times;</span>
											                        <span class="sr-only">Close</span>
											                    </button>
											                    <h4 class="modal-title color-default" id="relationModalLabel">
											                        A R B R E . O P T I O N S
											                    </h4>
											                </div>

											                <div class="modal-body">
											                	<form role="form" action="javascript:shareTree(<?php echo $tree->id; ?>);" class="form-default">

								                                    <h3 class="form-default-heading color-default">Partager un arbre</h3>

								                                    <label id="share-message-name" class="label label-default share-message"><?php echo $tree->name; ?></label>
								                                    <label id="share-message-success" class="label label-success share-message disabled">
								                                    	<i class="fa fa-check-circle"></i> Arbre envoyé. 
								                                    </label>
								                                    <label id="share-message-danger-user" class="label label-danger share-message disabled">
								                                    	<i class="fa fa-times-circle"></i> Aucun utilisateur ne s'est enregistré avec cette adresse. 
								                                    </label>
								                                    <label id="share-message-danger-tree" class="label label-danger share-message disabled">
								                                    	<i class="fa fa-times-circle"></i> 
								                                    	L'utilisateur possède déjà cet arbre.
								                                    	Changez le nom de l'arbre si vous désirez tout de le même l'envoyer.
								                                    </label>
								                                    <br/><br/>

								                                    <input id="share-email-<?php echo $tree->id; ?>" class="form-control form-input disabled" 
									                                       type="email" name="share-email"
									                                       placeholder="Adresse de courriel de l'utilisateur">            
									                                </input>
									                                <input id="share-slug-<?php echo $tree->id; ?>" type="hidden" value="<?php echo $tree->slug; ?>"></input>
									                                
									                                <br/>
									                                <span class="center block">
										                                <button type="submit" class="btn btn-success btn-block"> 
										                                    <i class="fa fa-send"></i> Partager 
										                                </button>
										                           	</span>
		                       
								                                </form>
											                </div>

											                <div class="modal-footer"></div>
								            			</div>
								        			</div>
												</div>
												<a href="javascript:showModal('share-tree-<?php echo $tree->id; ?>');"><i class="fa fa-send"></i></a> </td>
											<td class="danger">
												<div class="modal fade" id="delete-tree-<?php echo $tree->id; ?>" role="dialog" 
										            aria-labelledby="relationModalLabel" aria-hidden="true">
											        <div class="modal-dialog">
											            <div class="modal-content">
											                <div class="modal-header">
											                    <button type="button" class="close" data-dismiss="modal">
											                        <span aria-hidden="true">&times;</span>
											                        <span class="sr-only">Close</span>
											                    </button>
											                    <h4 class="modal-title color-default" id="relationModalLabel">
											                        A R B R E . O P T I O N S
											                    </h4>
											                </div>

											                <div class="modal-body">
										                        Vous êtes sur le point de <b>supprimer <b/>
										                        <br/>
										                        <h3 class="color-green"><?php echo $tree->name; ?></h3>
										                        <br/>

										                        <a href="<?php echo DIR.'trees/'.$tree->slug.'/delete';?>" class="btn btn-danger btn-block"> 
										                            <i class="fa fa-check"></i> Supprimer 
										                        </a>
											                </div>

											                <div class="modal-footer"></div>
								            			</div>
								        			</div>
												</div>
												<a href="javascript:showModal('delete-tree-<?php echo $tree->id; ?>');">
													<i class="fa fa-remove color-red"></i>
												</a> </td>
										</tr>
								<?php
									}
								?>
								</tbody>
								<tfoot>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td class="color-red"></td>
									</tr>
								</tfoot>
							</table>
					<?php
						} else
						{
					?>
							<p>Aucun arbre trouvé. 
								<small>
									<a href="<?php echo DIR?>trees/canvas">
										Créer un arbre maintenant.
									</a>
								</small>
							</p>
					<?php
						} 
					?>
					</div>
				</p>
			</div>

		</div>

	</div>

</div>

<script>
	$('[data-toggle="popover"]').popover({html : true});
	activateMenuItem('trees', 'accountItems');
</script> 