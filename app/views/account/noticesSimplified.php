<?php
use \core\error,
	\helpers\date;
?>
<?php 
if(Session::get('loggedin'))
{
	$userNotices 	= Session::get('allNotices');
	if ($userNotices)
	{
?>
		<div id="user-notice-list">
			<br/>
			<label>Notifications</label> 
			<a href="javascript:arborescence.drawing.displayAllNotices();" class="float-right">Tout afficher</a>
			<hr class="small"/>
			<?php
				foreach ($userNotices as $key => $notice) 
				{
					if ($notice->private == true)
					{
						switch ($notice->type) 
						{
							case 'TREE_CREATED':
								$color = 'success';
								break;
							case 'TREE_DELETED':
								$color = 'success';
								break;
							case 'TREE_SENT':
								$color = 'info';
								break;
							case 'TREE_RECEIVED':
								$color = 'info';
								break;
							default:
								$color = 'success';
								break;
						}
					} else
						$color = 'warning';
			?>
					<label class="label label-<?php echo $color; ?> notice-header">
						<?php echo Date::verbose($notice->created_at) ?>
					</label> 
					<br/>
					<b class="notice-title"><?php echo $notice->title; ?></b> <br/> 
					<span class="notice-message"><?php echo $notice->body; ?></span>
					<br/>
			<?php
				} 
			?>
	    </div>
<?php
	}
}
?>