<?php
use \core\error,
	\helpers\date;
?>
<div class="bodyContentModal">

	<div>

		<label><?php echo $data['searchCriteria']; ?></label>

		<p>
			<div>
			<?php
				$treeList = $data['treeList'];
				if ($treeList)
				{
			?>
					<table class="table table-tree">
						<thead>
							<tr>
								<th>Nom</th>
								<th>Auteur</th>
								<th>Dernière Modification</th>
							</tr>
						</thead>
						<tbody>
						<?php
							foreach ($treeList as $tree) 
							{
								$row_color 		= '';

								if($tree->fullname != $tree->lastUserFullname)
								{
									$row_color = "info";
								}
						?>
								<tr class="<?php echo $row_color?>">
									<td class="text-left"><a href="<?php echo DIR.'trees/'.$tree->slug.'/'.$tree->user.'/canvas';?>"><?php echo $tree->name; ?></a></td>
									<td class="text-left"><?php echo $tree->fullname ?></td>
									<td class="text-left"><?php echo Date::verbose($tree->updated_at) ?></td>
								</tr>
						<?php
							}
						?>
						</tbody>
						<tfoot>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tfoot>
					</table>
			<?php
				} else
				{
			?>
					<p>Aucun arbre trouvé. 
						<small>
							<a href="<?php echo DIR?>trees/canvas">
								Créer un arbre maintenant.
							</a>
						</small>
					</p>
			<?php
				} 
			?>
			</div>
		</p>
	</div>

</div>