<?php
use \core\error;
?>

<div class="bodyWrapper">

	<div class="bodyMargin">
		
		<div class="bodyContent">

			<!-- ---------- L O G I N . F O R M ------------------------------------ -->
			<div id="slide_account" class="slide">
				<h4>Profil</h4>
				<hr/>
				<div class="slide-content"> 
					<label>Nom complet</label><?php echo $data['user']->fullname; ?>
					<!-- <small class="font-seventy"> (Il est recommandé d'utiliser votre vrai nom)</small> -->
					<br/>
					<label>E-mail</label><?php echo $data['user']->email; ?>
					<br/>
					<label>Activité</label><?php echo $data['user']->occupation; ?>
					<br/>
					<label>Date d'inscription</label><?php echo $data['user']->created_at; ?>
					<br/>
					<label>Type de compte</label>
					<?php 
					if($data['user']->level == 9) 
						echo "Administrateur"; 
					 else if ($data['user']->level == 3) 
					 	echo "Utilisateur";
					 else 
					 	echo "Compte par défaut"; 
					?>
					<?php 
					if($data['user']->level == 9 || $data['user']->level == 3)
					{
					?>
					<br/>
					<br/>
					<a href="<?php echo DIR?>profile/edit" class="btn btn-primary">
						<i class="fa fa-pencil-square-o"></i> Modifier les informations
					</a>
					<a href="<?php echo DIR?>profile/delete" class="btn btn-danger">
						<i class="fa fa-exclamation-triangle"></i> Supprimer le compte
					</a>
					<?php
					}
					?>
				</div>
			</div>

		</div>

	</div>

</div>

<script>
	activateMenuItem('profile', 'accountItems');
</script>
