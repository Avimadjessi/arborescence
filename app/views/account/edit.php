<?php
use \core\error;
?>
<div class="bodyWrapper">

	<div class="bodyMargin">

		<div class="bodyContent">
			<!-- ---------- L O G I N . F O R M ---------------------------- -->
			<div id="slide_account" class="slide">
				<h4>Modification de votre profil</h4>
				<hr/>
				<?php
					$errors = $data['failure'];
					if($errors)
					{
				?>
						<div class="alert alert-danger center">
							<?php
								foreach ($errors as $key => $value) 
								{
								 	echo $errors[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<?php
					$success = $data['success'];
					if($success)
					{
				?>
						<div class="alert alert-success center">
							<?php
								foreach ($success as $key => $value) 
								{
								 	echo $success[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<div> 
					<form role="form" method="POST" class="form-default-lg">
						<label>Nom complet</label>
						<input type="text" name="userFullname" class="form-input" 
							value="<?php echo $data['user']->fullname; ?>"/>
						<br/>
						<label>Courriel</label>
							<?php echo $data['user']->email; ?>
						<br/>
						<label>Activité</label>
						<input type="text" name="userOccupation" class="form-input" 
							value="<?php echo $data['user']->occupation; ?>"/>
						<br/>
						<label>Date d'inscription</label>
							<?php echo $data['user']->created_at; ?>
						<br/>
						<label>Type de compte</label>
							<?php if($data['user']->level == 9) 
									echo "Administrateur"; 
								 else if ($data['user']->level == 3) 
								 	echo "Utilisateur"; 
								 else 
								 	echo "Compte par défaut"; 
							?>
						<br/>
						<br/>
						
						<label class="label label-danger">// Modifier votre mot de passe</label>
						<br/>
						<small>Ne remplissez pas ces champs si vous ne souhaitez pas modifier votre mot de passe</small>
						<br/>
						<span id="editPassword">
							<label>Mot de passe actuel</label>
							<input type="password" name="userPassword" class="form-input" 
								value=""/>
							<br/>
							<label>Nouveau mot de passe</label>
							<input type="password" name="userNewPassword" class="form-input" 
								value=""/>
							<br/>
							<label>Nouveau mot de passe</label>
							<input type="password" name="userNewPasswordConfirm" class="form-input" 
								value=""/>
							<br/>
						</span>
						<br/>
						<br/>
						<!-- SUBMIT -->
						<label></label>
				        <button type="submit" name="submit" value="edit" class="btn btn-lg btn-success">
				            <span class="fa fa-check"></span> Confirmer les modifications
				        </button>
					</form>
				</div>
			</div>

		</div>

	</div>

</div>

<script>
	activateMenuItem('editProfile', 'accountItems');
</script>
