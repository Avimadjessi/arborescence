<?php
use \core\error;
?>
<div class="bodyWrapper">

	<div class="bodyMargin">
		
		<div class="bodyContent">
			<!-- ---------- L O G I N . F O R M ------------------------------------ -->
			<div id="slide_home" class="slide">
				<ul>
					<b>Utilisateur par défaut</b>
					<li>E-mail : arborescence@gramm-r.ulb.ac.be</li>
					<li>Mot de passe : arborescence</li>
				</ul>
				<?php
					$errors = $data['failure'];
					if($errors)
					{
				?>
						<div class="alert alert-danger center">
							<?php
								foreach ($errors as $key => $value) 
								{
								 	echo $errors[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<?php
					$success = $data['success'];
					if($success)
					{
				?>
						<div class="alert alert-success center">
							<?php
								foreach ($success as $key => $value) 
								{
								 	echo $success[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
			    <form role="form" method="POST" class="form-default-lg">
			        <!-- ----- LOGIN ----- -->
			        <input type="email" name="userEmail" 
			               value="<?php echo $data['email']; ?>"
			               class="form-control" placeholder="Adresse de courriel"
			               required autofocus>

			        <!-- ----- PASSWORD ----- -->
			        <input type="password" name="password" 
			               value="" min="8" 
			               class="form-control" placeholder="Mot de passe" required>
			        <br />

			        <!-- ----- SUBMIT ----- -->
			        <button type="submit" name="submit" value="login" class="btn btn-lg btn-success">
			            <span class="fa fa-sign-in"></span> Connexion
			        </button>
			        <a href="<?php echo DIR?>" class="btn btn-lg btn-danger">
			            <span class="fa fa-close"></span> Annuler
			        </a>
			    </form>
			    <span>Nouvel utilisateur ? </span>
			    <a href="<?php echo DIR?>register" style="font-size: 0.8em"> Créer un compte ici </a>
			    <br/>
			    <span>Compte supprimé ? </span>
			    <a href="<?php echo DIR?>recover" style="font-size: 0.8em"> Récupérer le compte ici </a>
			</div>

		</div>

	</div>

</div>

<script>
	activateMenuItem('login', 'baseAccountItems');
</script>