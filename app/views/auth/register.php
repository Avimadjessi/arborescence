<?php
use \core\error;
?>

<div class="bodyWrapper">

	<div class="bodyMargin">
		
		<div class="bodyContent">
			<!-- ---------- R E G I S T E R . F O R M ------------------------------ -->
			<div id="slide_home" class="slide">
				<?php
					$errors = $data['failure'];
					if($errors)
					{
				?>
						<div class="alert alert-danger center">
							<?php
								foreach ($errors as $key => $value) 
								{
								 	echo $errors[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<?php
					$success = $data['success'];
					if($success)
					{
				?>
						<div class="alert alert-success center">
							<?php
								foreach ($success as $key => $value) 
								{
								 	echo $success[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<div id="home_register_form">
					<h4>Créer un compte utilisateur</h4>
				    <form role="form" method="POST" class="form-default-lg" >

				        <!-- ---------- U S E R N A M E -------------------- -->
				        <input type="text" name="userFullname" 
				               value="<?php echo $data['fullname']; ?>"
				               class="form-control" placeholder="Nom complet"
				               required autofocus>

				        <!-- ---------- E M A I L -------------------------- -->
				        <input type="email" name="userEmail" 
				               value="<?php echo $data['email']; ?>"
				               class="form-control" placeholder="Adresse de courriel"
				               required>

				        <!-- ---------- O C C U P A T I O N ---------------- -->
				        <input type="text" name="userOccupation" 
				               value="<?php echo $data['occupation']; ?>"
				               class="form-control" placeholder="Activité / Profession">

				        <!-- ---------- P A S S W O R D -------------------- -->
				        <input type="password" name="password" 
				               value=""
				               class="form-control" placeholder="Mot de passe" 
				               required>

				        <!-- ---------- P A S S W O R D -------------------- -->
				        <input type="password" name="passwordConfirm" 
				               value=""
				               class="form-control" placeholder="Confirmation mot de passe" 
				               required>

				        <br />

				        <!-- ---------- S U B M I T -------------------------------- -->
				        <button type="submit" name="submit" value="register" class="btn btn-lg btn-success">
				            <span class="fa fa-check"></span> Créer le compte
				        </button>
				        <a href="<?php echo DIR?>" class="btn btn-lg btn-danger">
				            <span class="fa fa-close"></span> Annuler
				        </a>
				    </form>
				    <span>Déjà utilisateur ? </span>
				    <a href="<?php echo DIR?>login" style="font-size: 0.8em"> Se connecter ici </a>
				    <br/>
				    <span>Compte supprimé ? </span>
				    <a href="<?php echo DIR?>recover" style="font-size: 0.8em"> Récupérer le compte ici </a>
				</div>
			</div> 
			 
		</div>

	</div>

</div>       

<script>
	activateMenuItem('register', 'baseAccountItems');
</script>