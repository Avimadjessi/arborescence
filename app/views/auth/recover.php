<?php
use \core\error;
?>
<div class="bodyWrapper">

	<div class="bodyMargin">
		
		<div class="bodyContent">
			<!-- ---------- R E C O V E R . F O R M -------------------------------- -->
			<div id="slide_home" class="slide">
				<?php
					$errors = $data['failure'];
					if($errors)
					{
				?>
						<div class="alert alert-danger center">
							<?php
								foreach ($errors as $key => $value) 
								{
								 	echo $errors[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<?php
					$success = $data['success'];
					if($success)
					{
				?>
						<div class="alert alert-success center">
							<?php
								foreach ($success as $key => $value) 
								{
								 	echo $success[$key]."<br/>";
								} 
							?>
						</div>
				<?php
					}
				?>
				<div id="home_recover_form">
					<h4>Récupérer un compte supprimé</h4>
				    <form role="form" method="POST" class="form-default-lg">
				        <!-- ----- LOGIN ----- -->
				        <input type="email" name="userEmail" 
				               value="<?php echo $data['email']; ?>"
				               class="form-control" placeholder="Adresse de courriel"
				               required autofocus>

				        <!-- ----- PASSWORD ----- -->
				        <input type="password" name="password" 
				               value=""
				               class="form-control" placeholder="Mot de passe" required>
				        <br />

				        <!-- ----- SUBMIT ----- -->
				        <button type="submit" name="submit" value="recover" class="btn btn-lg btn-success">
				            <span class="fa fa-sign-in"></span> Valider
				        </button>
				        <a href="<?php echo DIR?>" class="btn btn-lg btn-danger">
				            <span class="fa fa-close"></span> Annuler
				        </a>
				    </form>
				    <span>Déjà utilisateur ? </span>
				    <a href="<?php echo DIR?>login" style="font-size: 0.8em"> Se connecter ici </a>
				    <br/>
				    <span>Nouvel utilisateur ? </span>
				    <a href="<?php echo DIR?>register" style="font-size: 0.8em"> Créer un compte ici </a>
				</div>
			</div>

		</div>

	</div>

</div> 
<script>
	activateMenuItem('recover', 'baseAccountItems');
</script>