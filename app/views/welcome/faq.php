<div class="bodyWrapper">

	<div class="bodyMargin">
		
		<div class="bodyContent">
			<div id="slide_about" class="slide">
				<h4>Foire aux questions</h4>
				<hr/>
				<div> 
					<label>Poser une question</label>
					<br/>
					<?php
						$errors = $data['failure'];
						if($errors)
						{
					?>
							<div class="alert alert-danger center">
								<?php
									foreach ($errors as $key => $value) 
									{
									 	echo $errors[$key]."<br/>";
									} 
								?>
							</div>
					<?php
						}
					?>
					<?php
						$success = $data['success'];
						if($success)
						{
					?>
							<div class="alert alert-success center">
								<?php
									foreach ($success as $key => $value) 
									{
									 	echo $success[$key]."<br/>";
									} 
								?>
							</div>
					<?php
						}
					?>
					<div id="ask_form">
					    <form role="form" method="POST" class="form-default-lg" >

					        <!-- ---------- U S E R N A M E ---------------- -->
					        <input type="text" name="askFullname" 
					               value="<?php echo ($data['user']) ? $data['user']->fullname : '' ; ?>"
					               class="form-control" placeholder="Nom complet"
					               required autofocus>

					        <!-- ---------- E M A I L ---------------------- -->
					        <input type="email" name="askEmail" 
					               value="<?php echo ($data['user']) ? $data['user']->email : '' ; ?>"
					               class="form-control" placeholder="Adresse de courriel"
					               required>

					        <!-- ---------- M E S S A G E ------------------ -->
					        <textarea type="text" name="askMessage" style="resize:none;"
					               value=""
					               rows="4" maxlength="1024" 
					               class="form-control last-input" placeholder="Message à envoyer"
					               required></textarea>


					        <!-- ---------- S U B M I T -------------------- -->
					        <button type="submit" name="submit" value="faq" class="btn btn-lg btn-success">
					            <span class="fa fa-envelope"></span> Envoyer la question
					        </button>
					    </form>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

<script>
	activateMenuItem('faq');
</script> 
