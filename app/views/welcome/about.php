<div class="bodyWrapper">

	<div class="bodyMargin">
		
		<div class="bodyContent">
			<div class="slide">
				<h4>À propos du projet</h4>
				<hr/>
				<div id="resp-projet" class="team-member">
					<img class="user-picture"
						src="<?php echo helpers\url::template_path() . 'images/profile/_profile_lmeinert@gmail.com.png' ?>"></img>
					<br/>
					<br/>
					<span class="label label-warning team-member-name">Lionel Meinertzhagen</span>
					<br/>
					<br/>
					<span class="team-member-role">Responsable du projet</span>
					<br/>
					<span class="team-member-email">lmeinert(at)ulb.ac.be</span>
				</div>
				
				<div id="resp-scientifique" class="team-member">
					<img class="user-picture"
						src="<?php echo helpers\url::template_path() . 'images/profile/_profile_user@gramm-r.be.png' ?>"></img>
					<br/>
					<br/>
					<span class="label label-success team-member-name">Dan Van Raemdonck</span>
					<br/>
					<br/>
					<span class="team-member-role">Responsable scientifique</span>
					<br/>
					<span class="team-member-email">john.doe(at)mail.be</span>
				</div>

				<div id="resp-developpement" class="team-member">
					<img class="user-picture"
						src="<?php echo helpers\url::template_path() . 'images/profile/_profile_carlos.avim@gmail.com.png' ?>"></img>
					<br/>
					<br/>
					<span class="label label-info team-member-name">Carlos Avimadjessi</span>
					<br/>
					<br/>
					<span class="team-member-role">Développeur</span>
					<br/>
					<span class="team-member-email">carlos.avim(at)gmail.com</span>
				</div>

			</div>
		</div>

	</div>

</div>

<script>
	activateMenuItem('about');
</script> 
