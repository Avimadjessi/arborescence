<?php
use \helpers\session;
?>
<div id="body-home" class="body">
    
    <!-- ========== S L I D E ============================================== -->
    <div id="slide_1" class="slide center block">
        <div class="container">
            <h1 id="home_title" class="text">A R B O R E S C E N C E</h1>
            <hr class="dark"/>
            <div class="logo centered floating"></div>
            <br/>

        	<?php
        	if(Session::get('loggedin'))
        	{
        	?>
            	<h2 id="username_home">
            		<a class="blacklink" href="<?php echo DIR?>profile">
            			<?php echo Session::get('loggedUser')['fullname']; ?>
            		</a>
                </h2>
                <h6>
                	<a class="blacklink" href="<?php echo DIR?>logout">Déconnexion</a>
                </h6>
                <br/>
            <?php
            }else 
            {
            ?>
                <div id="intro">
                    <h3>Qu'est ce que c'est ?</h3>
                    <p>
                        Arborescence est un outil en ligne qui permet de dessiner 
                        des schémas de phrases en quelques clicks. 
                        Il accompagne l'utilisateur en anticipant un maximum 
                        d'erreurs et fournissant des rétroactions automatiques ciblées.
                        Plusieurs phrases y sont préencodées pour que chacun puisse s'entrainer.
                        Enfin tout utilisateur pourra sauvegarder les arbres 
                        qu’il a produits pour ensuite y revenir, les partager ou 
                        poser des questions théoriques à l'équipe de recherche.
                    </p>
                </div>
            <?php
        	}
            ?>
            <a id="home-enter" class="btn btn-lg btn-success"
                    href="<?php echo DIR?>trees"> 
                Entrer 
            </a>
        </div>
    </div>
    
    <!-- ========== S L I D E ============================================== -->
    <div id="slide_6" class="slide">
        &copy; Gramm-R | Arborescence 2014-2015
    </div>
    
</div>